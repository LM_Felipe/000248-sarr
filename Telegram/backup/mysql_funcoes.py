import mysql.connector
import requests
import json

#dadosDeslocamento = (km, cnpj, cc, data, placa, descricao, nome, caminho_imagem, status, responsavel_dif, tipo, reembolso,chat_id, msg_id)
#dadosDespesa = (valor, cnpj, cc, data, cartao, descricao, nome, caminho_imagem, status, responsavel_dif, tipo_gasto,chat_id,msg_id)
#dadosAbastecimento = (valor, cnpj, cc, data, cartao, litros, km, nome, caminho_imagem, status, responsavel_dif, placa,chat_id,msg_id)


def insereDeslocamento(dadosDeslocamento):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "INSERT INTO deslocamentos ( inicial, cnpj, cc, data, placa, descricao, nome, caminho_imagem, status, responsavel_dif, tipo, reembolso,chat_id, msg_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosDeslocamento)
	mycursor.close()
	mydb.close()


def insereDespesa(dadosDespesa):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "INSERT INTO notinhas ( valor, cnpj, cc, data, cartao, descricao, nome, caminho_imagem, status, responsavel_dif, tipo_gasto,chat_id,msg_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosDespesa)
	mycursor.close()
	mydb.close()


def insereAbastecimento(dadosAbastecimento):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "INSERT INTO abastecimento ( valor, cnpj, cc, data, cartao, litros, km, nome, caminho_imagem, status, responsavel_dif, placa,chat_id,msg_id, descricao) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosAbastecimento)
	mycursor.close()
	mydb.close()

def apagarRegistro(tipo, id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "UPDATE "+tipo+" set status = 'Apagado' where id="+str(id)+" AND status != 'Lancada'"
	mycursor.execute(statement)
	mycursor.close()
	mydb.close()

def listarDeslocamentosAbertos(telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT id, tipo, cc, km, placa, data, descricao from deslocamentos where chat_id="+str(telegram_id)+" AND (status = 'Aberto' OR status = 'Erro detectado') order by id desc"
	mycursor.execute(statement)

	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:
			registros.append(['ID: '+str(i[0])+' | CC: '+str(i[2])+' | Data: '+str(i[5])+' | Tipo: '+str(i[1])+' | KM: '+str(i[3])+' | Descricao: '+str(i[6])])

		return registros
	else:
		return false

def listarDespesasAbertas(telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT id, valor, cc, descricao, data, cnpj,cartao from notinhas where chat_id="+str(telegram_id)+" AND (status = 'Aberto' OR status = 'Erro detectado') order by id desc"
	mycursor.execute(statement)
	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:
			registros.append(['ID: '+str(i[0])+' | Valor:: '+str(i[1])+' | CC: '+str(i[2])+' | Descrição: '+str(i[3])+' | Data: '+str(i[4])+' | CNPJ: '+str(i[5])+' | Pagamento: '+str(i[6])])

		return registros
	else:
		return false

def listarAbastecimentosAbertos(telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT id, valor, cc, descricao, data, cnpj, cartao, placa from abastecimento where chat_id="+str(telegram_id)+" AND (status = 'Aberto' OR status = 'Erro detectado') order by id desc"
	mycursor.execute(statement)
	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:
			registros.append(['ID: '+str(i[0])+' | Valor:: '+str(i[1])+' | CC: '+str(i[2])+' | Placa: '+str(i[7])+' | Descrição: '+str(i[3])+' | Data: '+str(i[4])+' | CNPJ: '+str(i[5])+' | Pagamento: '+str(i[6])])

		return registros
	else:
		return false

def verificaCadastro(telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT * from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	if resultados:
		return True
	else:
		return False


def salvaID(email, telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT * from dados_usuario where id_telegram is not null AND email='"+email+"'"
	mycursor.execute(statement)
	resultados = mycursor.fetchall()

	if resultados:
		mycursor.close()
		mydb.close()
		return 3
	else:
		statement = "SELECT * from dados_usuario where email='"+email+"'"
		mycursor.execute(statement)
		resultados = mycursor.fetchall()
		if resultados:
			statement = "UPDATE dados_usuario set id_telegram="+str(telegram_id)+" where email='"+email+"'"
			mycursor.execute(statement)
			count = mycursor.rowcount
			if count:
				mycursor.close()
				mydb.close()
				return 1

		else:
			mycursor.close()
			mydb.close()
			return 2

	mycursor.close()
	mydb.close()

def validaApagarRegistro(tipo, telegram_id, id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT * FROM "+tipo+" WHERE chat_id="+str(telegram_id)+" AND id="+str(id)+" AND (status = 'Aberto' OR status = 'Erro detectado')"

	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	if resultados:
		return True
	else:
		return False

def insereCompra(dadosCompra):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "INSERT INTO comprasteste (id,solicitante,material,especificacoes,quantidade,marca,cc,status,justificativa,fornecedores,observacoes_comprador,observacoes_fornecedores,area,prazo_entrega,unidades, aprovacao_com, aprovacao_tec) values (id,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, 'Pendente','Pendente')"
	mycursor.execute(statement, dadosCompra)
	mycursor.close()
	mydb.close()

def listaDeslocamentosIncompletos(telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = '''
	SELECT D.id, inicial, cc, final, CONCAT(D.placa, " | ", V.modelo) as carro, data, descricao from deslocamentos D 
left join veiculos V on replace(V.placa,"-","") = D.placa
where chat_id='''+str(telegram_id)+''' AND final is NULL AND status='Aberto' order by id desc;
	'''
	mycursor.execute(statement)

	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:
			registros.append(['ID: '+str(i[0])+' | CC: '+str(i[2])+' | Data: '+str(i[5])+' | Inicial: '+str(i[1])+' | Descricao: '+str(i[6])+' | Carro: '+str(i[4])])

		return registros
	else:
		return false

def completaDeslocamento(id,km,path,descricao):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "UPDATE deslocamentos SET final ="+str(km)+", caminho_imagem2='"+path+"',descricao='"+descricao+"' where id="+str(id)
	mycursor.execute(statement)
	mycursor.close()
	mydb.close()

def confereExisteDeslocamento(id,id_telegram):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT * FROM deslocamentos WHERE id="+str(id)+" AND chat_id="+str(id_telegram)
	mycursor.execute(statement)

	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		return resultados[0][17]

	else:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		return False

def getDescricaoDeslocamento(id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT descricao FROM deslocamentos WHERE id="+str(id)
	mycursor.execute(statement)

	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	return resultados[0][0]

def insereCNPJ(CNPJ):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT cnpj from cnpj where cnpj=%s"
	mycursor.execute(statement, [CNPJ])
	teste = mycursor.fetchall()


	if not(teste):
		print('entrou')

		request = requests.get('https://www.receitaws.com.br/v1/cnpj/'+str(CNPJ))
		content = json.loads(request.content)

		try:
			cnae = content['atividade_principal'][0]['code']+' | '+content['atividade_principal'][0]['text']
			razao_social = content['nome']
			municipio = content['municipio']
			uf = content['uf']

			mycursor = mydb.cursor()
			statement = "INSERT INTO cnpj values (%s,%s,%s,%s,%s)"
			mycursor.execute(statement, [CNPJ,razao_social,municipio,uf,cnae])


		except:
			mycursor.close()
			mydb.close()

			return None

	mycursor.close()
	mydb.close()