#!/usr/bin/env python
# pylint: disable=W0613, C0116
# type: ignore[union-attr]
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging
import re
import datetime
import math

import telegramcalendar
import mysql_funcoes as my_sql
import get_CAR_CC as get_info

import json

from uuid import uuid4

from validate_docbr import CNPJ
from validate_docbr import CPF

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update, InlineQueryResultArticle, ParseMode, InputTextMessageContent
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
    CallbackQueryHandler,
    InlineQueryHandler,
)
from telegram.utils.helpers import escape_markdown




cnpj_verify = CNPJ()
cpf_verify = CPF()

date_format = '%d/%m/%y'

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

mensagem_saida = 'Tchau, até um próximo dia !\nPara iniciar um novo registro aperte: /start'

logger = logging.getLogger(__name__)

TIPO_REGISTRO, DATA_REG, CC, QUILOMETRAGEM, TIPO_QM, DESCRICAO, FOTOGRAFIA, NF, CNPJ, ABASTECIMENTO, VALOR, VEICULO, CARTAO, SELECT_CARTAO, DIGITOS_CARTAO, QUILOMETRO_ABASTECIDO, APAGAR_REGISTRO, CONFIRMACAO_APAGAR, AGORA_APAGOU, REGISTRO_EMAIL,SELECT_REGISTRO,NUM_PEDAGIOS,INCREMENTAR_DESCRICAO = range(23)

def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper

def inline_handler(update: Update, context: CallbackContext) -> int:
    selected,date = telegramcalendar.process_calendar_selection(update,context)
    if selected:
        context.user_data['data'] = date.strftime("%d/%m/%y")

        try:
          date_obj = datetime.datetime.strptime(date.strftime("%d/%m/%y"), date_format)
        except ValueError:
            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="O formato da data está errada, tente novamente",
                        reply_markup=telegramcalendar.create_calendar(),)

            # Próxima pergunta
            return DATA_REG       

        dif_datas = datetime.datetime.today() - datetime.datetime.strptime(context.user_data['data'], "%d/%m/%y")
        if dif_datas.days<0:
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você não pode lançar uma data futura, tente novamente:",
                        reply_markup=telegramcalendar.create_calendar(),)

            # Próxima pergunta
            return DATA_REG


        # Próxima pergunta
        if  context.user_data['choice'] == "Quilometragem":
             # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você selecionou: %s\n\nQual a quilometragem marcada no hodômetro?" % (date.strftime("%d/%m/%Y")),
                        reply_markup=ReplyKeyboardRemove(),)

            # Próxima pergunta
            return QUILOMETRAGEM



            '''
            # Teclado a ser exibido para o usuário
            reply_keyboard = [['Inicial', 'Final']]
            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você selecionou: %s\n\nQual o tipo de quilometragem ?" % (date.strftime("%d/%m/%Y")),
                        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            return TIPO_QM
            #return QUILOMETRAGEM
            '''



        if context.user_data['choice'] == 'Nota Fiscal' or context.user_data['choice'] == 'Pedágio':
            reply_keyboard = get_info.cnpjList(context.user_data['cc'])
            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
            text="Você selecionou: %s\n\nQual o CNPJ da nota fiscal?" % (date.strftime("%d/%m/%Y")),
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            #return NF
            return CNPJ

        if context.user_data['choice'] == 'Abastecimento':
            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
            text="Você selecionou: %s\n\nQual a quilometragem marcando quando abastecido ?" % (date.strftime("%d/%m/%Y")),
            reply_markup=ReplyKeyboardRemove(),)
            return QUILOMETRO_ABASTECIDO


# Qual pergunta será feita para o usuário
def start(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Quilometragem', 'Nota Fiscal', 'Abastecimento'],['Apagar registro','Pedágio']]

    user = update.message.from_user
    telegram_id = user.id
    context.user_data['telegram_id'] = user.id

    if my_sql.verificaCadastro(telegram_id):
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Olá, seja bem-vindo ao sistema SARR LMLogix\n'
            'Para cancelar o registro, envie /cancelar\n\n'
            'O que você deseja registrar ?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return TIPO_REGISTRO

    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Olá, seja bem-vindo ao sistema SARR LMLogix\n\n'
            'Verifiquei que seu número não está cadatrado\n'
            'Caso você possua cadastro no SARR, insira seu e-mail abaixo, para registrar o seu número de telefone:',
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return REGISTRO_EMAIL

def registro_email(update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text

    user = update.message.from_user
    telegram_id = user.id

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user

        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    tentativa_gravacao = str(my_sql.salvaID(text, telegram_id))

    if  tentativa_gravacao == '1':
        update.message.reply_text(
        'Cadastro realizado com sucesso !\nPara iniciar um novo registro aperte: /start' , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if  tentativa_gravacao == '3':
        update.message.reply_text(
        'Esse e-mail já está associado com um número !\nEntre em contato com a LMLogix para veriicar' , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if  tentativa_gravacao == '2':
        update.message.reply_text(
        'Esse e-mail não está cadastrado na LMLogix !\nEntre em contato com a LMLogix para veriicar' , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END


# Qual pergunta será feita para o usuário
def tipo_registro(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.ccList() #[['251-GM-Conversão CPU', '245-LM-Sistema WEB', '248-LM-SARR']]

    # Resposta recebida do usuário
    text = update.message.text
    # Informação gravada no session do usuário
    context.user_data['choice'] = text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    user = update.message.from_user
    telegram_id = user.id

    if text == 'Apagar registro':    
        reply_keyboard = [['Quilometragem', 'Nota Fiscal', 'Abastecimento']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Selecione qual tipo de registro você deseja apagar:',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return APAGAR_REGISTRO

    if not(text == 'Quilometragem' or text == 'Nota Fiscal' or text == 'Abastecimento' or text == 'Apagar registro' or text == 'Pedágio'):
        reply_keyboard = [['Quilometragem', 'Nota Fiscal', 'Abastecimento'],['Apagar registro', 'Pedágio']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Tipo de registro inválido, tente novamente',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return TIPO_REGISTRO


    if  context.user_data['choice'] == "Quilometragem":
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Inicial', 'Final']]
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Qual o tipo de quilometragem ?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_QM
        #return QUILOMETRAGEM

    if context.user_data['choice'] == "Pedágio":
        update.message.reply_text(
            'Quantos lançamentos iguais você deseja registrar?' , reply_markup=ReplyKeyboardRemove()
        )
        return NUM_PEDAGIOS
    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Qual é o centro de custos do registro ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    # Próxima pergunta
    return CC


def num_pedagios(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if text.isnumeric():
        context.user_data['numPedagios']=text
    else:
        update.message.reply_text(
            'Você precisa informar um número!\nQuantos lançamentos iguais você deseja registrar?' , reply_markup=ReplyKeyboardRemove()
        )
        return NUM_PEDAGIOS




    reply_keyboard = get_info.ccList() #[['251-GM-Conversão CPU', '245-LM-Sistema WEB', '248-LM-SARR']]

    update.message.reply_text(
        'Qual é o centro de custos dos registros?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    # Próxima pergunta
    return CC

# Qual pergunta será feita para o usuário
def apagar_registro(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if not(text == 'Quilometragem' or text == 'Nota Fiscal' or text == 'Abastecimento'):
        reply_keyboard = [['Quilometragem', 'Nota Fiscal', 'Abastecimento']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Tipo de registro inválido, tente novamente',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return APAGAR_REGISTRO

    # Informação gravada no session do usuário
    context.user_data['tipo_apagar'] = text

    user = update.message.from_user
    telegram_id = user.id

    if context.user_data['tipo_apagar'] == 'Quilometragem':
        reply_keyboard = my_sql.listarDeslocamentosAbertos(telegram_id)
    if context.user_data['tipo_apagar'] == 'Nota Fiscal':
        reply_keyboard = my_sql.listarDespesasAbertas(telegram_id)
    if context.user_data['tipo_apagar'] == 'Abastecimento':
        reply_keyboard = my_sql.listarAbastecimentosAbertos(telegram_id)

    if not(reply_keyboard):
        update.message.reply_text(
        'Não há registros para cancelar!  \n\nCaso tenha detectado um erro em algum lançamento que não está mais disponível para ser apagado, favor entrar em contato com o setor administrativo.\n\n'
        'Até a próxima vez !\n'
        'Para enviar um novo registro favor apertar /start'
        )

        return ConversationHandler.END

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Selecione qual registro você deseja apagar:',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    # Próxima pergunta
    return CONFIRMACAO_APAGAR

# Qual pergunta será feita para o usuário
def confirmacao_apagar(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if context.user_data['tipo_apagar'] == 'Quilometragem':
        tipo_para_apagar = "deslocamentos"
    if context.user_data['tipo_apagar'] == 'Nota Fiscal':
        tipo_para_apagar = "notinhas"
    if context.user_data['tipo_apagar'] == 'Abastecimento':
        tipo_para_apagar = "abastecimento"

    user = update.message.from_user
    telegram_id = user.id

    if context.user_data['tipo_apagar'] == 'Quilometragem':
        reply_keyboard = my_sql.listarDeslocamentosAbertos(telegram_id)
    if context.user_data['tipo_apagar'] == 'Nota Fiscal':
        reply_keyboard = my_sql.listarDespesasAbertas(telegram_id)
    if context.user_data['tipo_apagar'] == 'Abastecimento':
        reply_keyboard = my_sql.listarAbastecimentosAbertos(telegram_id)

    if text.find("|") != -1:
        id_apagar = text.split("|")    
        text_id = re.sub("[^0-9]", "", id_apagar[0])
        context.user_data['registro_apagar'] = re.sub("[^0-9]", "", id_apagar[0])
    else:
        text_id = re.sub("[^0-9]", "", text)
        context.user_data['registro_apagar'] = text_id


    if not text_id.isnumeric():
        update.message.reply_text(
            "Precisa ser um número, tente novamente" , 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        return CONFIRMACAO_APAGAR

    confere_se_pode_apagar = my_sql.validaApagarRegistro(tipo_para_apagar,telegram_id,context.user_data['registro_apagar'])

    if not(confere_se_pode_apagar):
        update.message.reply_text(
            "Você não pode apagar esse ID, tente novamente." , 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        return CONFIRMACAO_APAGAR

    reply_keyboard = [['Sim', 'Não']]

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Tem certeza que quer apagar o registro abaixo ? \n\n' + text,
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    # Próxima pergunta
    return AGORA_APAGOU

# Qual pergunta será feita para o usuário
def agora_apagou(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)

        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if not(text == 'Sim' or text == 'Não'):
        reply_keyboard = [['Sim', 'Não']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Opção inválida, tente novamente',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return AGORA_APAGOU

    if context.user_data['tipo_apagar'] == 'Quilometragem':
        tipo_para_apagar = "deslocamentos"
    if context.user_data['tipo_apagar'] == 'Nota Fiscal':
        tipo_para_apagar = "notinhas"
    if context.user_data['tipo_apagar'] == 'Abastecimento':
        tipo_para_apagar = "abastecimento"

    if text == 'Sim':   
        my_sql.apagarRegistro(tipo_para_apagar, context.user_data['registro_apagar'])

    if text == 'Não':   
        update.message.reply_text(
        'Operação cancelada !\n'
        'Até a próxima vez !\n\n'
        'Para enviar um novo registro favor apertar /start'
        )

        return ConversationHandler.END

    update.message.reply_text(
        'Registro apagado com sucesso !\n'
        'Até a próxima vez !\n\n'
        'Para enviar um novo registro favor apertar /start'
    )

    return ConversationHandler.END

# Qual pergunta será feita para o usuário
def centro_custos(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if text.find("|") != -1:
        text = text.split("|")    
        # Informação gravada no session do usuário
        context.user_data['cc'] = re.sub("[^0-9]", "", text[0])
    else:
        context.user_data['cc'] = re.sub("[^0-9]", "", text)

    if context.user_data['cc'].isnumeric():
        cc_existe = get_info.checkCC(context.user_data['cc'])
    else:
        reply_keyboard = get_info.ccList() #[['251-GM-Conversão CPU', '245-LM-Sistema WEB', '248-LM-SARR']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Este centro de custos não existe, tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return CC

    if cc_existe:

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual é a data do registro ?',
            reply_markup=telegramcalendar.create_calendar(),
            #reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return DATA_REG
    else:
        reply_keyboard = get_info.ccList() #[['251-GM-Conversão CPU', '245-LM-Sistema WEB', '248-LM-SARR']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Este centro de custos não existe, tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return CC


# Qual pergunta será feita para o usuário
def dia_reg (update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    try:
        date = context.user_data['data']
    except KeyError:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
                'O formato da data está errada, tente novamente',
                reply_markup=telegramcalendar.create_calendar(),
            )

        # Próxima pergunta
        return DATA_REG

    # Próxima pergunta
    if  context.user_data['choice'] == "Quilometragem":
        # Teclado a ser exibido para o usuário
        reply_keyboard = get_info.carList() #[['AAA-2359 -> Corsa Sedan', 'AAA-2359 -> Gol', 'AAA-2359 -> Montana']]
        # Resposta recebida do usuário
        text = update.message.text

        # Finaliza a conversa
        if text == '/cancelar':
            user = update.message.from_user
            logger.info("Usuário %s cancelou a conversa.", user.first_name)
            update.message.reply_text(
                mensagem_saida , reply_markup=ReplyKeyboardRemove()
            )
            return ConversationHandler.END

        # Informação gravada no session do usuário
        context.user_data['distancia_percorrida'] = text

        if text.isnumeric():
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual o veículo utilizado ?',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )

            # Próxima pergunta
            return VEICULO
        else:
            update.message.reply_text(
                'Deve ser um número, tente novamente',
                reply_markup=ReplyKeyboardRemove(),
            )

            # Próxima pergunta
            return QUILOMETRAGEM

    if context.user_data['choice'] == 'Nota Fiscal' or context.user_data['choice'] == 'Pedágio':
        # Resposta recebida do usuário
        text = update.message.text

        # Finaliza a conversa
        if text == '/cancelar':
            user = update.message.from_user
            logger.info("Usuário %s cancelou a conversa.", user.first_name)
            update.message.reply_text(
                mensagem_saida , reply_markup=ReplyKeyboardRemove()
            )
            return ConversationHandler.END

        if text.find("|") != -1:
            text = text.split("|")    
            # Informação gravada no session do usuário
            context.user_data['cnpj'] = re.sub("[^0-9]", "", text[1])
        else:
            context.user_data['cnpj'] = re.sub("[^0-9]", "", text)

        if not (cnpj_verify.validate(context.user_data['cnpj'])or cpf_verify.validate(context.user_data['cnpj'])):
            # Teclado a ser exibido para o usuário
            reply_keyboard = get_info.cnpjList(context.user_data['cc']) #[['LM-Logix-04.580.206/0001-99', 'Braun-04.580.206/0001-99', 'Darolt-04.580.206/0001-99', 'Delupo-04.580.206/0001-99']]
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'CNPJ inválido, tente novamente.',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )
            return CNPJ

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o valor gasto ?',
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return VALOR

    if context.user_data['choice'] == 'Abastecimento':
        context.user_data['quilometro_abastecido'] = text

        if re.search("^[0-9]{1,}([,.][0-9]{1,3})?$", text):
            if text.find(",") != -1:
                context.user_data['abastecimento'] = text.replace(",",".") 

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Quantos litros foram abastecidos ?',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return ABASTECIMENTO
        else:
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'O valor inserido tem que ser um número, tente novamente.',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return QUILOMETRO_ABASTECIDO

# Qual pergunta será feita para o usuário
def tipo_deslocamento(update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['tipo_deslocamento'] = text


    if text == 'Inicial':
        reply_keyboard = get_info.ccList()
        update.message.reply_text(
        'Qual é o centro de custos do registro ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

        # Próxima pergunta
        return CC

    elif text == 'Final':
        # Questionamento a ser feito para o usuário
        reply_keyboard = my_sql.listaDeslocamentosIncompletos(context.user_data['telegram_id'])

        if reply_keyboard:
            update.message.reply_text(
            'Qual registro você está completando?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            return SELECT_REGISTRO
        else:
            user = update.message.from_user
            logger.info("Usuário %s cancelou a conversa.", user.first_name)
            update.message.reply_text(
                'Você não possui deslocamentos em aberto!\n\nPara enviar um novo registro favor apertar /start' , reply_markup=ReplyKeyboardRemove()
            )
            return ConversationHandler.END


def select_registro(update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if text.find("|") != -1:
        id_apagar = text.split("|")    
        text_id = re.sub("[^0-9]", "", id_apagar[0])
        context.user_data['id_deslocamento'] = re.sub("[^0-9]", "", id_apagar[0])

        context.user_data['distancia_inicial'] = my_sql.confereExisteDeslocamento(context.user_data['id_deslocamento'],context.user_data['telegram_id'])
        if context.user_data['distancia_inicial']:
            update.message.reply_text(
                'Qual a quilometragem marcada no hodômetro?',
                reply_markup=ReplyKeyboardRemove(),
            )

            # Próxima pergunta
            return QUILOMETRAGEM

    else:
        # Questionamento a ser feito para o usuário
        reply_keyboard = [['Inicial', 'Final']]
        update.message.reply_text(
            'Opção inválida, tente novamente',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return TIPO_QM

# Qual pergunta será feita para o usuário
def nota_fiscal(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.cnpjList(context.user_data['cc']) #[['LM-Logix-04.580.206/0001-99', 'Braun-04.580.206/0001-99', 'Darolt-04.580.206/0001-99', 'Delupo-04.580.206/0001-99']]

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['nota_fiscal'] = text

    if text.isnumeric():
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o CNPJ da nota ?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return CNPJ   
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'O valor inserido tem que ser um número, tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return NF

# Qual pergunta será feita para o usuário
def quilometro_abastecido(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['quilometro_abastecido'] = text

    if re.search("^[0-9]{1,}([,.][0-9]{1,3})?$", text):
        if text.find(",") != -1:
            context.user_data['abastecimento'] = text.replace(",",".") 

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Quantos litros foram abastecidos ?',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return ABASTECIMENTO
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'O valor inserido tem que ser um número, tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return QUILOMETRO_ABASTECIDO

# Qual pergunta será feita para o usuário
def abastecimento(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.carList() #[['AAA-2359 -> Corsa Sedan', 'AAA-2359 -> Gol', 'AAA-2359 -> Montana']]
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['abastecimento'] = text

    if re.search("^[0-9]{1,}([,.][0-9]{1,3})?$", text):
        if text.find(",") != -1:
            context.user_data['abastecimento'] = text.replace(",",".") 

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o veículo utilizado ?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return VEICULO
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'O valor inserido tem que ser um número, tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return ABASTECIMENTO

# Qual pergunta será feita para o usuário
def veiculo(update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if text.find("-") != -1:
        text = text.replace("-","")  

    placa = text.split("|")
    context.user_data['veiculo'] = placa[0].strip()

    if re.search("[a-zA-Z]{3}[0-9][0-9A-Z][0-9]{2}", text):
        
        if context.user_data['choice'] == 'Quilometragem':
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual a descrição do deslocamento ?',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return DESCRICAO
    else:
        reply_keyboard = get_info.carList()

        update.message.reply_text(
            'Placa não encontrada, tente novamente',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        return VEICULO    

    if context.user_data['choice'] == 'Abastecimento':
        # Teclado a ser exibido para o usuário
        reply_keyboard = get_info.cnpjPOSTOS() #[['LM-Logix-04.580.206/0001-99', 'Braun-04.580.206/0001-99', 'Darolt-04.580.206/0001-99', 'Delupo-04.580.206/0001-99']]
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o CNPJ do posto ?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return CNPJ

# Qual pergunta será feita para o usuário
def cnpj(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if text.find("|") != -1:
        text = text.split("|")    
        # Informação gravada no session do usuário
        context.user_data['cnpj'] = re.sub("[^0-9]", "", text[1])
    else:
        context.user_data['cnpj'] = re.sub("[^0-9]", "", text)

    if not (cnpj_verify.validate(context.user_data['cnpj'])or cpf_verify.validate(context.user_data['cnpj'])):
        # Teclado a ser exibido para o usuário
        reply_keyboard = get_info.cnpjList(context.user_data['cc']) #[['LM-Logix-04.580.206/0001-99', 'Braun-04.580.206/0001-99', 'Darolt-04.580.206/0001-99', 'Delupo-04.580.206/0001-99']]
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'CNPJ inválido, tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        return CNPJ

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Qual o valor gasto ?',
        reply_markup=ReplyKeyboardRemove(),
    )

    # Próxima pergunta
    return VALOR

# Qual pergunta será feita para o usuário
def valor(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Cartão da empresa', 'Caixa'],['Faturado']]
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['valor_gasto'] = text

    if re.search("^[0-9]{1,}([,.][0-9]{1,3})?$", text):
        if text.find(",") != -1:
            context.user_data['valor_gasto'] = text.replace(",",".") 
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o cartão utilizado ?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return CARTAO
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Deve ser um número, tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return VALOR


# Qual pergunta será feita para o usuário
def deslocamento(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.carList() #[['AAA-2359 -> Corsa Sedan', 'AAA-2359 -> Gol', 'AAA-2359 -> Montana']]
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['distancia_percorrida'] = text

    if text.isnumeric():

        if context.user_data['tipo_deslocamento']=='Final':
            if context.user_data['distancia_inicial']>int(context.user_data['distancia_percorrida']):
                update.message.reply_text(
                    'A quilometragem final não pode ser menor que a inicial! Informe a quilometragem final:',
                    reply_markup=ReplyKeyboardRemove(),
                )

                # Próxima pergunta
                return QUILOMETRAGEM

            else:
                update.message.reply_text(
                    "Caso queira adicionar alguma observação à descrição inicial, digite aqui.\n\nCaso queira mantê-la inalterada, envie /pular.",
                    reply_markup=ReplyKeyboardRemove(),
                )

                # Próxima pergunta
                return INCREMENTAR_DESCRICAO
            
        else:

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual o veículo utilizado ?',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )

            # Próxima pergunta
            return VEICULO
    else:
        update.message.reply_text(
            'Deve ser um número, tente novamente',
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return QUILOMETRAGEM

def incrementar_descricao(update: Update, context: CallbackContext) -> int:

    text = update.message.text
    context.user_data['descricao'] = my_sql.getDescricaoDeslocamento(context.user_data['id_deslocamento'])

    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if text == '/pular':
        update.message.reply_text(
            'Favor enviar uma foto do registro',
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return FOTOGRAFIA
    else:
        context.user_data['descricao'] = context.user_data['descricao']+' | OBS final: '+text
        update.message.reply_text(
            'Favor enviar uma foto do registro',
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return FOTOGRAFIA


    

# Qual pergunta será feita para o usuário
def cartao (update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['cartao'] = text



    if text == 'Cartão da empresa':
        cartoes = get_info.cartao(context.user_data['telegram_id'])

        if cartoes is None:
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Quais os 4 últimos dígitos do cartão ?',
                reply_markup=ReplyKeyboardRemove(),
            )
            
            # Próxima pergunta
            return DIGITOS_CARTAO         

        else:
            cartoes = json.loads(cartoes)
            # Teclado a ser exibido para o usuário
            reply_keyboard = cartoes       

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Confirme os últimos 4 dígitos do cartão',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )

            # Próxima pergunta
            return SELECT_CARTAO

    else:
        if text == 'Caixa' or text == 'Faturado':    

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual a descrição ?',
                reply_markup=ReplyKeyboardRemove(),
            )

            # Próxima pergunta
            return DESCRICAO    
        else:
            reply_keyboard = [['Cartão da empresa', 'Caixa'],['Faturado']]

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Opção inválida, tente novamente.',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )

            return CARTAO

# Qual pergunta será feita para o usuário
def select_cartao (update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['cartao'] = text
    cartoes = json.loads(get_info.cartao(context.user_data['telegram_id']))
    cartao_select = 0

    logger.info("text = %s.", text)
    logger.info("cartoes[0] = %s.", cartoes)

    for i in cartoes:
        i = re.sub('[^0-9]', '', str(i))
        logger.info("i = %s.", i)   
        if text == i:
            cartao_select = i
        
    if text == cartao_select:
        context.user_data['cartao'] = cartao_select
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual a descrição ?',
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return DESCRICAO        
    else:
        reply_keyboard = cartoes

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Opção inválida, tente novamente. ',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        return SELECT_CARTAO

# Qual pergunta será feita para o usuário
def digitos_cartao (update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['cartao'] = text

    text = re.sub('[^0-9]', '', text)
    logger.info("Card: %s.", text)

    if len(text) == 4:
        context.user_data['cartao'] = text
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual a descrição ?',
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return DESCRICAO
    else:
        reply_keyboard = [['Cartão da empresa', 'Caixa'],['Faturado']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Opção inválida, tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        return DIGITOS_CARTAO


# Qual pergunta será feita para o usuário
def descricao (update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['descricao'] = text


    if context.user_data['choice']=='Pedágio':
        context.user_data['photoIndex']=1
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Favor enviar foto 1/'+str(context.user_data['numPedagios']),
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return FOTOGRAFIA


    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Favor enviar uma foto do registro',
        reply_markup=ReplyKeyboardRemove(),
    )

    # Próxima pergunta
    return FOTOGRAFIA



# Tira a foto
def imagem(update: Update, context: CallbackContext) -> int:

    user = update.message.from_user
    now = datetime.datetime.now() # current date and time

    # Resposta recebida do usuário
    text = update.message.text
    message_id = update.message.message_id
    user_id = user.id
    date_time = now.strftime("%m-%d-%Y_%H-%M-%S-%f")

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    file_name = "Uid_" + str(user_id) + "_time_" + date_time + ".jpg"

    if  context.user_data['choice'] == "Quilometragem":
        local_imagem = 'C:/Ponto/public/img/deslocamento/'
        context.user_data['caminho_imagem'] = '/img/deslocamento/' + file_name

    if context.user_data['choice'] == 'Nota Fiscal' or context.user_data['choice'] == 'Pedágio':
        local_imagem = 'C:/Ponto/public/img/notas/'
        context.user_data['caminho_imagem'] = '/img/notas/' + file_name

    if context.user_data['choice'] == 'Abastecimento':
        local_imagem = 'C:/Ponto/public/img/abastecimento/'
        context.user_data['caminho_imagem'] = '/img/abastecimento/' + file_name

    user_name = user.first_name #+ " " + user.last_name

    user = update.message.from_user
    
    try:
        photo_file = update.message.photo[-1].get_file()
        photo_file.download(local_imagem+file_name)
    except IndexError:
            update.message.reply_text(
            'Você precisa enviar uma foto, tente novamente',
            reply_markup=ReplyKeyboardRemove(),
            )

            # Próxima pergunta
            return FOTOGRAFIA
    
    # Próxima pergunta
    if  context.user_data['choice'] == "Quilometragem":
        if context.user_data['tipo_deslocamento']=='Final':

            my_sql.completaDeslocamento(context.user_data['id_deslocamento'],context.user_data['distancia_percorrida'],context.user_data['caminho_imagem'],context.user_data['descricao'])
            informacoes_registro = 'Modalidade: Deslocamento (final)\nID lançamento Inicial: '+str(context.user_data['id_deslocamento'])+ '\nHodômetro: ' + context.user_data['distancia_percorrida'] + '\nDeslocamento total: '+str(int(context.user_data['distancia_percorrida'])-int(context.user_data['distancia_inicial']))+'km\nDescrição: '+context.user_data['descricao']+'\n\n'
        else:

            dadosDeslocamento = (context.user_data['distancia_percorrida'], '04.580.206/0001-99', context.user_data['cc'], context.user_data['data'], context.user_data['veiculo'], context.user_data['descricao'], user_name, context.user_data['caminho_imagem'], "Aberto", "0", context.user_data['tipo_deslocamento'], "reembolso",user_id, message_id)
            my_sql.insereDeslocamento(dadosDeslocamento)
            informacoes_registro = 'Modalidade: Deslocamento\nCC: ' + str(context.user_data['cc']) + '\nData: ' + context.user_data['data'] + "\nVeículo: " + context.user_data['veiculo'] + '\nTipo: ' + context.user_data['tipo_deslocamento'] + '\nHodômetro: ' + context.user_data['distancia_percorrida'] + '\nDescrição: ' + context.user_data['descricao'] + '\n\n'


 
    if context.user_data['choice'] == 'Nota Fiscal' or context.user_data['choice'] == 'Pedágio':
        dadosDespesa = (round(float(context.user_data['valor_gasto']),2), context.user_data['cnpj'], context.user_data['cc'], context.user_data['data'], context.user_data['cartao'], context.user_data['descricao'], user_name, context.user_data['caminho_imagem'], "Aberto", "0", "tipo_gasto",user_id,message_id)
        my_sql.insereDespesa(dadosDespesa)
        informacoes_registro = 'Modalidade: Nota Fiscal\nCC: ' + str(context.user_data['cc']) + '\nData: ' + context.user_data['data'] + '\nCartão: ' + str(context.user_data['cartao']) + '\nCNPJ estabelecimento: ' + str(get_info.format(context.user_data['cnpj'])) + '\nValor gasto: R$ ' + str(context.user_data['valor_gasto']) + '\nDescrição: ' + context.user_data['descricao'] + '\n\n'

        if len(context.user_data['cnpj'])==14:
            my_sql.insereCNPJ(context.user_data['cnpj'])


    if context.user_data['choice'] == 'Abastecimento':
        dadosAbastecimento = (round(float(context.user_data['valor_gasto']),2), context.user_data['cnpj'], context.user_data['cc'], context.user_data['data'], context.user_data['cartao'], round(float(context.user_data['abastecimento']),2), context.user_data['quilometro_abastecido'], user_name, context.user_data['caminho_imagem'], "Aberto", "0", context.user_data['veiculo'],user_id,message_id, context.user_data['descricao'])  
        my_sql.insereAbastecimento(dadosAbastecimento)
        informacoes_registro = 'Modalidade: Abastecimento\nCC: ' + str(context.user_data['cc']) + '\nData: ' + context.user_data['data'] + '\nCartão: ' + str(context.user_data['cartao']) + '\nCNPJ estabelecimento: ' + str(get_info.format(context.user_data['cnpj'])) + '\nQuilometro abastecido: ' + str(context.user_data['quilometro_abastecido']) + '\nLitros abastecidos: ' + str(context.user_data['abastecimento']) + '\nValor gasto: R$ ' + str(context.user_data['valor_gasto']) + '\nPlaca: ' + str(context.user_data['veiculo']) + '\nDescrição: ' + context.user_data['descricao'] + '\n\n'    



    nome_cabloco = get_info.nomeCadastro(user_id)


    # context.bot.send_message(chat_id=-426613638,
    #                     text='Dados obtidos: \n\n' + informacoes_registro)
    if not(context.user_data['choice'] == 'Pedágio'):
        update.message.reply_text(
            'Dados obtidos: \n\n' + informacoes_registro +
            'Registro efetuado com sucesso !\n'
            'Até a próxima vez !\n\n'
            'Para enviar um novo registro favor apertar /start'
        )

        context.bot.send_photo(chat_id=-571036896, photo=open(local_imagem+file_name, 'rb'), 
                       caption='Usuário: ' + nome_cabloco + ' mandou um novo registro.\n\nDados obtidos: \n\n' + informacoes_registro)

        return ConversationHandler.END
    else:
        if str(context.user_data['photoIndex'])==str(context.user_data['numPedagios']):
            update.message.reply_text(
                'Dados obtidos: \n\n' + 'Registro '+str(context.user_data['photoIndex'])+'\n'+informacoes_registro +
                'Registro efetuado com sucesso !\n'
                'Até a próxima vez !\n\n'
                'Para enviar um novo registro favor apertar /start'
            )

            context.bot.send_photo(chat_id=-571036896, photo=open(local_imagem+file_name, 'rb'), 
                        caption='Usuário: ' + nome_cabloco + ' mandou um novo registro.\n\nDados obtidos: \n\n' + informacoes_registro)
            return ConversationHandler.END
        else:
            context.user_data['photoIndex']+=1
            update.message.reply_text(
                'Dados obtidos: \n\n' + 'Registro '+str(context.user_data['photoIndex']-1)+'\n'+informacoes_registro +
                'Registro efetuado com sucesso !\n\n'+
                'Favor enviar foto '+str(context.user_data['photoIndex'])+'/'+str(context.user_data['numPedagios']),
                reply_markup=ReplyKeyboardRemove(),
            )

            context.bot.send_photo(chat_id=-571036896, photo=open(local_imagem+file_name, 'rb'), 
                        caption='Usuário: ' + nome_cabloco + ' mandou um novo registro.\n\nDados obtidos: \n\n' + informacoes_registro)

            # Próxima pergunta
            return FOTOGRAFIA


# Termina a conversa
def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        'Tchau, até um próximo dia !\nPara iniciar um novo registro aperte: /start' , reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END

# Termina a conversa
def ajuda(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        'Comando inválido !\n\nPara iniciar um novo registro aperte: /start\nPara cancelar um registro em progresso aperte: /cancelar' , reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END

def inlinequery(update: Update, context: CallbackContext) -> None:
    user_input = update.inline_query.query

    info_cnpj = get_info.searchCNPJ(str(user_input))

    try:
        results_list=[]    

        for i in info_cnpj:
            results_list.append(InlineQueryResultArticle(id=uuid4(), title=str(i[0]), input_message_content=InputTextMessageContent(str(i[0]+' | '+i[1]))))
                    
        update.inline_query.answer(results_list)

    except Exception as e:
        print(e)


def main() -> None:
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    #updater = Updater("1673212380:AAFsp2yNBFWaxSAllk-j90ZGjSlMYWv6a9k", use_context=True)
    updater = Updater("1664029734:AAGLO1anpVlfAQ4_mTcesN0r6ZD6pwcWACk", use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={ 
            TIPO_REGISTRO: [MessageHandler(Filters.all, tipo_registro), CommandHandler('cancelar', cancel)],       
            CC: [MessageHandler(Filters.all, centro_custos), CommandHandler('cancelar', cancel)],
            DATA_REG: [MessageHandler(Filters.all, dia_reg), CommandHandler('cancelar', cancel)],
            TIPO_QM: [MessageHandler(Filters.all, tipo_deslocamento), CommandHandler('cancelar', cancel)],
            QUILOMETRAGEM: [MessageHandler(Filters.all, deslocamento), CommandHandler('cancelar', cancel)],
            NF: [MessageHandler(Filters.all, nota_fiscal), CommandHandler('cancelar', cancel)],
            CNPJ: [MessageHandler(Filters.all, cnpj), CommandHandler('cancelar', cancel)],
            VALOR: [MessageHandler(Filters.all, valor), CommandHandler('cancelar', cancel)],
            CARTAO: [MessageHandler(Filters.all, cartao), CommandHandler('cancelar', cancel)],
            SELECT_CARTAO: [MessageHandler(Filters.all, select_cartao), CommandHandler('cancelar', cancel)],
            DIGITOS_CARTAO: [MessageHandler(Filters.all, digitos_cartao), CommandHandler('cancelar', cancel)],
            VEICULO: [MessageHandler(Filters.all, veiculo), CommandHandler('cancelar', cancel)],
            ABASTECIMENTO: [MessageHandler(Filters.all, abastecimento), CommandHandler('cancelar', cancel)],
            DESCRICAO: [MessageHandler(Filters.all, descricao), CommandHandler('cancelar', cancel)],
            FOTOGRAFIA: [MessageHandler(Filters.all, imagem), CommandHandler('cancelar', cancel)],
            QUILOMETRO_ABASTECIDO: [MessageHandler(Filters.all, quilometro_abastecido), CommandHandler('cancelar', cancel)],
            APAGAR_REGISTRO: [MessageHandler(Filters.all, apagar_registro), CommandHandler('cancelar', cancel)],
            CONFIRMACAO_APAGAR: [MessageHandler(Filters.all, confirmacao_apagar), CommandHandler('cancelar', cancel)],
            AGORA_APAGOU: [MessageHandler(Filters.all, agora_apagou), CommandHandler('cancelar', cancel)],
            REGISTRO_EMAIL: [MessageHandler(Filters.all, registro_email), CommandHandler('cancelar', cancel)],
            SELECT_REGISTRO: [MessageHandler(Filters.all, select_registro), CommandHandler('cancelar', cancel)],
            NUM_PEDAGIOS: [MessageHandler(Filters.all, num_pedagios), CommandHandler('cancelar', cancel)],
            INCREMENTAR_DESCRICAO: [MessageHandler(Filters.all, incrementar_descricao), CommandHandler('cancelar', cancel)],
        },
        fallbacks=[CommandHandler('cancelar', cancel)],
    )

    dispatcher.add_handler(CallbackQueryHandler(inline_handler))
    dispatcher.add_handler(conv_handler)
    dispatcher.add_handler(MessageHandler(Filters.regex('^(?!.*(start|cancelar))'), ajuda))

    dispatcher.add_handler(InlineQueryHandler(inlinequery))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
