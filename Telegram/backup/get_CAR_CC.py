import mysql.connector
import json

def carList():
	veiculos=[[]]

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT placa, modelo from veiculos"
	mycursor.execute(statement)
	listaVeiculos = mycursor.fetchall()
	linha=0
	
	for i in listaVeiculos:
		veiculos[linha].append(i[0]+' | '+i[1])
		
		if len(veiculos[linha])==3:
			veiculos.append([])
			linha= linha+1

	mycursor.close()
	mydb.close()

	return veiculos


def ccList():
	cc=[]

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "SELECT cc, nome_obra from projetos"
	mycursor.execute(statement)
	listaCC = mycursor.fetchall()
	
	for i in listaCC:
		cc.append([i[0]+' | '+i[1]])


	mycursor.close()
	mydb.close()

	return cc

def cnpjList(cc):
	cnpj=[]

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()

	statement = "SELECT cidade from projetos where cc="+str(cc)
	mycursor.execute(statement)
	cidade = mycursor.fetchall()
	
	statement = 'SELECT razao_social, cnpj from cnpj order by FIELD(municipio, "'+cidade[0][0]+'") desc, razao_social asc'
	mycursor.execute(statement)
	listaCNPJ = mycursor.fetchall()
	
	for i in listaCNPJ:
		cnpj.append([i[0]+' | '+i[1]])

	mycursor.close()
	mydb.close()
	
	return cnpj

def checkCC(cc):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "SELECT EXISTS(SELECT * FROM projetos WHERE cc = " + cc +" )"
	mycursor.execute(statement)
	response_cc = mycursor.fetchall()
	
	mycursor.close()
	mydb.close()

	return response_cc[0][0]

def searchCNPJ(input):
	cnpj=[]

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "SELECT razao_social, cnpj from cnpj where razao_social like '%"+input+"%' order by razao_social asc"
	mycursor.execute(statement)
	listaCNPJ = mycursor.fetchall()
	
	# for i in listaCNPJ:
	# 	cnpj.append([i[0]+' | '+i[1]])


	mycursor.close()
	mydb.close()

	return listaCNPJ

def cnpjPOSTOS():
	cnpj=[]

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "SELECT razao_social, cnpj from cnpj where cnae like '47.31-8-00%' or razao_social like '%posto%' order by razao_social asc"
	mycursor.execute(statement)
	listaCNPJ = mycursor.fetchall()
	
	for i in listaCNPJ:
		cnpj.append([i[0]+' | '+i[1]])


	mycursor.close()
	mydb.close()

	return cnpj

def format( cnpj ):
		"""
		Method to format cnpj numbers.
		Tests:
		
		>>> print Cnpj().format('53612734000198')
		53.612.734/0001-98
		"""
		if len(cnpj)==14:
			return "%s.%s.%s/%s-%s" % ( cnpj[0:2], cnpj[2:5], cnpj[5:8], cnpj[8:12], cnpj[12:14] )
		else:
			return "%s.%s.%s-%s" % ( cnpj[0:3], cnpj[3:6], cnpj[6:9], cnpj[9:11])

def nomeCadastro(telegram_id):
	cnpj=[]

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "SELECT nome from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	nome = mycursor.fetchall()
	
	return nome[0][0]

def username(telegram_id):
	cnpj=[]

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "SELECT username from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	nome = mycursor.fetchall()
	
	return nome[0][0]

def cartao(telegram_id):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "SELECT cartao from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	nome = mycursor.fetchall()
	
	return nome[0][0]

def aaaa():
    cartoes = json.loads(cartao(1174495728))
    cartao_select = 0

    text = cartoes[0]

    for i in cartoes[0]:
            cartao_select = i
            print(i)