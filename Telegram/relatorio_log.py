'''
Arquivo para geração de relatório em PDF
da tabela de log do controle de dispositivos
da Base pelo SARR
'''

import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import mysql.connector
from datetime import datetime
from fpdf import FPDF
import conexao_banco

# Função para geração do relatório
def relatorio_log(telegram_id,nome,ini,fim,acao,titulo):
    try:
        # Conexão com o banco de dados
        mydb = conexao_banco.conexao_banco()
        
        # Verifica se há filtro de nome
        if nome != '':
            where = 'where nome = "'+nome+'"'
        else:
            where = ''

        # Verifica se há filtro de data
        if ini != '' and fim != '':
            data_ini = datetime.strptime(ini, "%d/%m/%Y").strftime('%Y-%m-%d')
            data_fim = datetime.strptime(fim, "%d/%m/%Y").strftime('%Y-%m-%d')

            if where == '':
                where = 'where date(tempo) between date("'+data_ini+'") and date("'+data_fim+'")'
            else:
                where = where + ' and date(tempo) between date("'+data_ini+'") and date("'+data_fim+'")'

        # Verifica se há filtro de ação
        if acao != '':
            if where == '':
                where = 'where alteracao = "'+acao+'"'
            else:
                where = where + 'and alteracao = "'+acao+'"'

        mycursor = mydb.cursor()
        # Seleciona os dados da tabela conforme os filtros
        statement = '''SELECT nome, alteracao, tempo from controle_log_bot A left join 
                    dados_usuario B on chat_id = id_telegram '''+where+''' order by tempo desc'''
        mycursor.execute(statement)
        resultados = mycursor.fetchall()
        
        n = 0
        usuario = []
        acao_vetor = []
        data_hora = []

        # Cria um vetor para cada coluna (usuário, ação e data)
        for i in resultados:
            usuario.append(resultados[n][0])
            acao_vetor.append(resultados[n][1])
            data_hora.append(resultados[n][2])
            n = n + 1

        # Estrutura a tabela do relatório
        df = pd.DataFrame()
        df['Usuário'] = usuario
        df['Ação'] = acao_vetor
        df['Data/hora'] = data_hora
        
        k = 1
        c = []
        for i in resultados:
            c.append(k*2)
            k=k+1

        m = [x - 0.5 for x in c]

        plt.xticks(c, df['Usuário'])
        
        # Data e hora da geração do relatório
        t = datetime.now()

        s = str(format(t.second, '02d'))
        m = str(format(t.minute, '02d'))
        h = str(format(t.hour, '02d'))

        d = str(format(t.day, '02d'))
        mon = str(format(t.month, '02d'))
        y = str(t.year)

        x = '/'
        z = ':'
        p = '-'

        data_hora_titulo = y + p + mon + p + d + '_' + h + p + m + p + s
        data_hora_footer = d + x + mon + x + y + ' ' + h + z + m + z + s

        # Adiciona cabeçalho e rodapé ao arquivo
        class PDF(FPDF):

            # Page header
            def header(this):
                # Logo
                this.image('C:/Telegram/Logo.png', 10, 0, 30, 30, 'PNG', 'Logo.png')
                # Arial bold 15
                this.set_font('Arial', '', 9)
                # Move to the right
                this.cell(120)
                # Title
                this.cell(60, 10, 'CONECTANDO PESSOAS A UMA NOVA EXPERIÊNCIA', 0, 2, 'C')
                # Line break
                this.ln(20)

            # Page footer
            def footer(this):
                # Position at 1.5 cm from bottom
                this.set_y(-18)                        
                # Arial italic 8
                this.set_font('Arial','I',9)
                # Data e hora
                this.cell(0,10,str(data_hora_footer),0,0,'L')
                # Page number
                this.cell(0,10,'Página: '+str(this.page_no())+'/{nb}',0,0,'R')
        
        # Instanciation of inherited class
        pdf = PDF()
        pdf.alias_nb_pages()
        pdf.add_page()

        # Sem filtro de usuário
        if nome == '':
            nome = 'todos'

        # Sem filtro de data
        if ini == '':
            ini = 'todas'
            fim = ''
        else:
            ini = ini+' - '

        # Sem filtro de ação
        if acao == '':
            acao = 'todas'

        # Posição e conteúdo dos textos
        pdf.set_xy(0, 30)
        pdf.set_font('arial', 'B', 12)
        pdf.cell(60)
        pdf.cell(90, 10, "Relatório de log do bot SARR", 0, 2, 'C') # Título central
        pdf.cell(90, 10, " ", 0, 2, 'C')
        pdf.cell(-55)
        pdf.cell(90, 8, "Usuário selecionado: "+nome+".", 0, 2, 'L') # Indica filtro de usuário
        pdf.cell(90, 8, "Data selecionada: "+ini+fim+".", 0, 2, 'L') # Indica filtro de data
        pdf.cell(90, 8, "Ação selecionada: "+acao+".", 0, 2, 'L') # Indica filtro de ação
        pdf.cell(90, 8, " ", 0, 2, 'C')
        pdf.cell(80, 10, 'Usuário', 1, 0, 'C')
        pdf.cell(70, 10, 'Ação', 1, 0, 'C')
        pdf.cell(50, 10, 'Data/hora', 1, 2, 'C')
        pdf.cell(-150)
        pdf.set_font('arial', '', 12)
        # Organização da tabela
        for i in range(0, len(df)):
            pdf.cell(80, 10, '%s' % (df['Usuário'].loc[i]), 1, 0, 'C')
            pdf.cell(70, 10, '%s' % (str(df['Ação'].loc[i])), 1, 0, 'C')
            pdf.cell(50, 10, '%s' % (str(df['Data/hora'].loc[i])), 1, 2, 'C')
            pdf.cell(-150)
        pdf.cell(90, 10, " ", 0, 2, 'C')
        pdf.cell(-30)
        
        # Geração do arquivo em PDF
        pdf.output('../../../../../../../Telegram/'+data_hora_titulo+'_'+titulo+'_'+str(telegram_id)+'.pdf', 'F')
        # Para o bot no servidor:
        # pdf.output('../../../../../../../Telegram/'+data_hora_titulo+'_'+'Relatorio_'+str(telegram_id)+'.pdf', 'F')

        # Para relatório gerado retorna 1 e a data e horário que vai no nome do arquivo
        return [1,data_hora_titulo]
    except Exception as err:
        return '\n\nErro: '+str(err)+'\n\n'

# Para testes:
# print(relatorio_log(1174495728,'','',''))