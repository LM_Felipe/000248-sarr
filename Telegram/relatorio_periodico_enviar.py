'''
Arquivo para enviar os relatórios periódicos
É necessário um serviço sendo executado
no servidor para o envio dos relatórios
nos dias e horários corretos
Nome do serviço: "relatorio_periodico"
'''

import mysql.connector
import relatorio_log
import time
from datetime import datetime, timedelta
import mysql_funcoes
import sendEmail
import os
import conexao_banco

# Envia um relatório periódico por e-mail
def enviarRelatorioPeriodico():
	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	# Lista os relatórios criados pelo usuário que estão ativos (não foram apagados)
	statement = "SELECT * from relatorio_periodico where status = 'ativo'"
	mycursor.execute(statement)
	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

	# Para cada relatório listado
	for i in resultados:
		id = i[0]
		usuario = i[1]
		periodicidade = i[2]
		horario = i[3]

		destinatarios = i[4]
		destinatarios = destinatarios.replace('[','')
		destinatarios = destinatarios.replace(']','')
		destinatarios = destinatarios.replace("'","")

		if destinatarios.find(",")!=-1:
			destinatarios = destinatarios.split(', ')

		# Filtro de usuário
		f_usuario = i[5]
		# Filtro de ação
		f_acao = i[6]
		# Filtro de data
		data = i[7]
		# Status do relatório
		status = i[8]

		data_relatorio = ''
		primeiro_relatorio = True

		# Não é o primeiro envio do relatório
		if data != None:
			data_relatorio = datetime.strptime(str(data), '%d/%m/%Y')
			data_relatorio = data_relatorio.date()
			data_relatorio = data_relatorio + timedelta(days=periodicidade)
			data_relatorio = str(data_relatorio).split("-")
			data_relatorio = data_relatorio[2]+"/"+data_relatorio[1]+"/"+data_relatorio[0]
			primeiro_relatorio = False

		dia_atual = datetime.now().strftime('%d/%m/%Y')

		# Confirma se é dia de enviar o relatório
		if dia_atual == data_relatorio or primeiro_relatorio == True:
			hora_atual = datetime.now().strftime('%H:%M')

			# Confirma se é o horário de enviar relatório
			if hora_atual == horario:
				# Conexão com o banco de dados
				mydb = conexao_banco.conexao_banco()

				mycursor = mydb.cursor()
				# Seleciona a id do telegram do usuário
				statement = "SELECT id_telegram from dados_usuario where nome='"+usuario+"'"
				mycursor.execute(statement)
				telegram_id = mycursor.fetchall()
				telegram_id = telegram_id[0][0]

				# Calcula data inicial do próximo envio
				data_ini = datetime.now().strftime('%d/%m/%Y')
				data_ini = datetime.strptime(str(data_ini), '%d/%m/%Y')
				data_ini = data_ini.date()
				data_ini = data_ini + timedelta(days=(-1)*periodicidade)
				data_ini = str(data_ini).split("-")
				data_ini = data_ini[2]+"/"+data_ini[1]+"/"+data_ini[0]

				# Gera o relatório
				relatorio = relatorio_log.relatorio_log(telegram_id,f_usuario,data_ini,dia_atual,
					f_acao,'Relatorio')

				# Nome do arquivo de relatório
				nome_arquivo = relatorio[1]+'_Relatorio_'+str(telegram_id)+'.pdf'
				titulo = 'Relatório Log Bot SARR'

				# Para mais de um destinatário
				if isinstance(destinatarios, list):
					for i in destinatarios:
						email = mysql_funcoes.email(str(i))[0]
						sendEmail.sendEmailRelatorio(usuario, i, email, titulo, nome_arquivo)
				# Para apenas um destinatário
				else:
					email = mysql_funcoes.email(destinatarios)[0]
					sendEmail.sendEmailRelatorio(usuario, destinatarios, email, titulo, nome_arquivo)

				# Conexão com o banco de dados
				mydb = conexao_banco.conexao_banco()

				mycursor = mydb.cursor()

				# Altera o campo data para o dia do envio do último relatório
				statement = "UPDATE relatorio_periodico set data ='"+dia_atual+"' where id='"+str(id)+"'"
				mycursor.execute(statement)
				count = mycursor.rowcount
				mycursor.close()
				mydb.close()

				# Remove o arquivo de relatório
				# No servidor utilizar: "os.remove('../../../../../../../Telegram/'+nome_arquivo)"
				os.remove('../../../../../../../Telegram/'+nome_arquivo)

			# Não precisa enviar relatório neste minuto
			else:
				print('não enviar e-mail')

	return 'Finalizado'

# Busca a cada minuto relatórios que precisam ser enviados
while True:
	print(enviarRelatorioPeriodico())
	time.sleep(60)