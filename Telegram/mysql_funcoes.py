'''
Arquivo com funções para realizar ações no banco de dados, como: insert, select e update
'''

import mysql.connector
import requests
import json
import conexao_banco

# Insere nova linha na tabela "deslocamentos"
def insereDeslocamento(dadosDeslocamento):

	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "INSERT INTO deslocamentos ( inicial, cnpj, cc, data, placa, descricao, nome, caminho_imagem, status, responsavel_dif, tipo, reembolso,chat_id, msg_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosDeslocamento)
	id = mycursor.lastrowid
	mycursor.close()
	mydb.close()

	return id

# Insere nova linha na tabela "notinhas"
def insereDespesa(dadosDespesa):

	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "INSERT INTO notinhas ( valor, cnpj, cc, data, cartao, descricao, nome, caminho_imagem, status, responsavel_dif, tipo_gasto,chat_id,msg_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosDespesa)
	id = mycursor.lastrowid
	mycursor.close()
	mydb.close()

	return id

# Insere nova linha na tabela "abastecimento"
def insereAbastecimento(dadosAbastecimento):

	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "INSERT INTO abastecimento ( valor, cnpj, cc, data, cartao, litros, km, nome, caminho_imagem, status, responsavel_dif, placa,chat_id,msg_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosAbastecimento)
	id = mycursor.lastrowid
	mycursor.close()
	mydb.close()

	return id

# Apaga um registro (nota, deslocamento, abastecimento) mudando seu status para "Apagado"
def apagarRegistro(tipo, id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "UPDATE "+tipo+" set status = 'Apagado' where id="+str(id)+" AND status != 'Lancada'"
	mycursor.execute(statement)
	mycursor.close()
	mydb.close()

# Lista os deslocamentos que estão em aberto ou com erro para o usuário conforme sua ID do Telegram
def listarDeslocamentosAbertos(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT id, tipo, cc, inicial, final, placa, data, descricao from deslocamentos where chat_id="+str(telegram_id)+" AND (status = 'Aberto' OR status = 'Erro detectado') order by id desc"
	mycursor.execute(statement)

	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:

			inicial = str(i[3])
			final = str(i[4])

			if final == 'None':
				final = inicial

			registros.append(['ID: '+str(i[0])+' | CC: '+str(i[2])+' | Data: '+str(i[6])+' | Tipo: '+str(i[1])+' | KM: '+final+' | Descricao: '+str(i[7])])

		return registros
	else:
		return false

# Lista as despesas que estão em aberto ou com erro para o usuário conforme sua ID do Telegram
def listarDespesasAbertas(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT id, valor, cc, descricao, data, cnpj,cartao from notinhas where chat_id="+str(telegram_id)+" AND (status = 'Aberto' OR status = 'Erro detectado') order by id desc"
	mycursor.execute(statement)
	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:
			registros.append(['ID: '+str(i[0])+' | Valor: '+str(i[1])+' | CC: '+str(i[2])+' | Descrição: '+str(i[3])+' | Data: '+str(i[4])+' | CNPJ: '+str(i[5])+' | Pagamento: '+str(i[6])])

		return registros
	else:
		return false

# Lista os abastecimentos que estão em aberto ou com erro para o usuário conforme sua ID do Telegram
def listarAbastecimentosAbertos(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT id, valor, cc, descricao, data, cnpj, cartao, placa from abastecimento where chat_id="+str(telegram_id)+" AND (status = 'Aberto' OR status = 'Erro detectado') order by id desc"
	mycursor.execute(statement)
	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:
			registros.append(['ID: '+str(i[0])+' | Valor: '+str(i[1])+' | CC: '+str(i[2])+' | Placa: '+str(i[7])+' | Descrição: '+str(i[3])+' | Data: '+str(i[4])+' | CNPJ: '+str(i[5])+' | Pagamento: '+str(i[6])])

		return registros
	else:
		return false

# Verifica cadastro de usuário conforme ID do Telegram
def verificaCadastro(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT * from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	if resultados:
		return True
	else:
		return False

# Guarda ID do Telegram para novo usuário no bot
def salvaID(email, telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT * from dados_usuario where id_telegram is not null AND email='"+email+"'"
	mycursor.execute(statement)
	resultados = mycursor.fetchall()

	# Caso haja resultado
	if resultados:
		mycursor.close()
		mydb.close()
		return 3
	# Para usuário com e-mail válido, porém sem ID do Telegram
	else:
		statement = "SELECT * from dados_usuario where email='"+email+"'"
		mycursor.execute(statement)
		resultados = mycursor.fetchall()
		if resultados:
			# Adiciona ID do Telegram ao usuário
			statement = "UPDATE dados_usuario set id_telegram="+str(telegram_id)+" where email='"+email+"'"
			mycursor.execute(statement)
			count = mycursor.rowcount
			if count:
				mycursor.close()
				mydb.close()
				return 1

		else:
			mycursor.close()
			mydb.close()
			return 2

	mycursor.close()
	mydb.close()

# Confirmação para apagar um registro (nota, deslocamento, abastecimento)
def validaApagarRegistro(tipo, telegram_id, id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT * FROM "+tipo+" WHERE chat_id="+str(telegram_id)+" AND id="+str(id)+" AND (status = 'Aberto' OR status = 'Erro detectado')"

	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	if resultados:
		return True
	else:
		return False	

# Insere nova linha de compra na tabela "comprasteste"
def insereCompra(dadosCompra):

	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "INSERT INTO comprasteste (id,solicitante,material,especificacoes,quantidade,marca,cc,status,justificativa,fornecedores,observacoes_comprador,observacoes_fornecedores,area,prazo_entrega,unidades, aprovacao_com, aprovacao_tec) values (id,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, 'Pendente','Pendente')"
	mycursor.execute(statement, dadosCompra)
	idCompra=mycursor.lastrowid
	mycursor.close()
	mydb.close()
	return idCompra

# Alterar status de linha na tabela "compras" para "Aprovado"
def aprovaCompra(dadosCompra):

	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor(id)
	statement = "update table comprasteste set status='Aprovado' where id=%s"
	mycursor.execute(statement, id)
	mycursor.close()
	mydb.close()

# Insere novo relatório de horas
def insereRelatorio(dadosRelatorio):

	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	print('teste '+dadosRelatorio[6])
	statement = "INSERT INTO relatorio (colaborador, Data, entrada, saida, CC, tipo, cidade, motorista, veiculo, km_Inicial, km_Final, km_Abst, litros, valor, funcao, solicitante, pernoite, relatorio,id_telegram,aprovacao) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosRelatorio)
	mycursor.close()
	mydb.close()

# Lista relatórios de horas que estão com aprovação pendente
def listarRelatoriosAbertos(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT id, Data, entrada, saida, CC, tipo, cidade, solicitante, pernoite, relatorio from relatorio where id_telegram="+str(telegram_id)+" AND aprovacao = 'Pendente' order by id desc"
	mycursor.execute(statement)
	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:
			registros.append(['ID: '+str(i[0])+' | Data: '+str(i[1])+' | Entrada: '+str(i[2])+' | Saída: '+str(i[3])+' | CC: '+str(i[4])+' | Modalidade: '+str(i[5])+' | Cidade: '+str(i[6])+' | Solicitante: '+str(i[7])+' | Pernoite: '+str(i[8])+' | Relatório: '+str(i[9])])

		return registros
	else:
		return false

# Lista os deslocamentos que estão incompletos
def listaDeslocamentosIncompletos(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''
	SELECT D.id, inicial, cc, final, CONCAT(D.placa, " | ", V.modelo) as carro, data, descricao from deslocamentos D 
left join veiculos V on replace(V.placa,"-","") = D.placa
where chat_id='''+str(telegram_id)+''' AND final is NULL AND status='Aberto' order by id desc;
	'''
	mycursor.execute(statement)

	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:
			registros.append(['ID: '+str(i[0])+' | CC: '+str(i[2])+' | Data: '+str(i[5])+' | Inicial: '+str(i[1])+' | Descricao: '+str(i[6])+' | Carro: '+str(i[4])])

		return registros
	else:
		return false

# Acrescenta informações adicionais a um registro de deslocamento
def completaDeslocamento(id,km,path,descricao):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "UPDATE deslocamentos SET final ="+str(km)+", caminho_imagem2='"+path+"',descricao='"+descricao+"' where id="+str(id)
	mycursor.execute(statement)
	mycursor.close()
	mydb.close()

# Confere se existe um determinado deslocamento conforme ID e usuário
def confereExisteDeslocamento(id,id_telegram):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT * FROM deslocamentos WHERE id="+str(id)+" AND chat_id="+str(id_telegram)
	mycursor.execute(statement)

	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		return resultados[0][17]

	else:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		return False

# Pega a descrição de um deslocamento conforme sua ID
def getDescricaoDeslocamento(id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT descricao FROM deslocamentos WHERE id="+str(id)
	mycursor.execute(statement)

	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	return resultados[0][0]

# Insere novo CNPJ na tabela "cnpj"
def insereCNPJ(CNPJ):

	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT cnpj from cnpj where cnpj=%s"
	mycursor.execute(statement, [CNPJ])
	teste = mycursor.fetchall()


	if not(teste):
		print('entrou')

		request = requests.get('https://www.receitaws.com.br/v1/cnpj/'+str(CNPJ))
		content = json.loads(request.content)

		try:
			cnae = content['atividade_principal'][0]['code']+' | '+content['atividade_principal'][0]['text']
			razao_social = content['nome']
			municipio = content['municipio']
			uf = content['uf']

			mycursor = mydb.cursor()
			statement = "INSERT INTO cnpj values (%s,%s,%s,%s,%s)"
			mycursor.execute(statement, [CNPJ,razao_social,municipio,uf,cnae])


		except:
			mycursor.close()
			mydb.close()

			return None

	mycursor.close()
	mydb.close()

# Insere nova linha de log de ações de controle realizadas via bot
def controle_log(chat_id,alteracao):

	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "INSERT INTO controle_log_bot (chat_id, alteracao) values ("+str(chat_id)+",'"+str(alteracao)+"')"
	mycursor.execute(statement)
	mycursor.close()
	mydb.close()

# Lista os nomes dos colaboradores cadastrados
def listarNomes():
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT nome from dados_usuario"
	mycursor.execute(statement)
	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		return resultados
	else:
		return false

# Permissão de acesso para cada usuário
def permissao(nome,permissao):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''SELECT nome, chat_id, permissao, id_telegram from permissao_telegram A, dados_usuario B 
	where nome = "'''+nome+'''" and chat_id = id_telegram '''
	mycursor.execute(statement)
	resultados = mycursor.fetchall()

	# Tabela não está vazia
	if resultados:
		# Usuário já existe
		if resultados[0][1] == resultados[0][3]:
			statement = '''UPDATE permissao_telegram set permissao = "'''+str(permissao)+'''" 
			where convert(chat_id, char) = '''+str(resultados[0][3])+''' '''
			mycursor.execute(statement)
			mycursor.close()
			mydb.close()
		# Usuário é novo		
		else:
			statement = '''INSERT INTO permissao_telegram (chat_id,permissao) 
			VALUES('''+str(resultados[0][3])+''',"'''+str(permissao)+'''")'''
			mycursor.execute(statement)
			mycursor.close()
			mydb.close()
	# Tabela vazia		
	else:
		statement = '''SELECT id_telegram from dados_usuario where nome = "'''+nome+'''" '''
		mycursor.execute(statement)
		resultados = mycursor.fetchall()

		statement = '''INSERT INTO permissao_telegram (chat_id,permissao) 
		VALUES('''+str(resultados[0][0])+''',"'''+str(permissao)+'''")'''
		mycursor.execute(statement)
		mycursor.close()
		mydb.close()

	return str(resultados[0][0])

# Confere a permissão do usuário do bot
def confere_permissao(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''SELECT permissao from permissao_telegram	where chat_id = '''+str(telegram_id)+''' '''
	mycursor.execute(statement)
	resultados = mycursor.fetchall()

	if resultados:
		return resultados[0][0]
	else:
		return False

# Pega email do destinatario do relatório conforme nome do usuário
def email(nome):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT email, id_telegram from dados_usuario where nome='"+nome+"'"
	mycursor.execute(statement)
	select = mycursor.fetchall()
	
	return [select[0][0],select[0][1]]

# Pega nome do remetente pela id do telegram
def remetente(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT nome from dados_usuario where id_telegram='"+str(telegram_id)+"'"
	mycursor.execute(statement)
	nome = mycursor.fetchall()
	
	return nome[0][0]

# Log de relatório: guarda no banco os filtros do último relatório criado
def log_relatorio(telegram_id,filtro_data,filtro_usuario,filtro_acao):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''INSERT INTO log_relatorio (chat_id, filtro_data, filtro_usuario, filtro_acao) values 
	("'''+str(telegram_id)+'''","'''+filtro_data+'''","'''+filtro_usuario+'''","'''+filtro_acao+'''")'''
	mycursor.execute(statement)
	id = mycursor.lastrowid
	mycursor.close()
	mydb.close()

	return id