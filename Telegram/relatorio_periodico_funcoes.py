'''
Arquivo com as funções do 
sistema de relatório periódico
'''

import mysql.connector
import mysql_funcoes
import conexao_banco

# Lista todos os relatórios periódicos ativos do usuário que solicita
def listaRelatorios(usuario):
	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT * from relatorio_periodico where usuario='"+usuario+"' and status = 'ativo'"
	mycursor.execute(statement)
	if mycursor.rowcount:
		resultados = mycursor.fetchall()
		mycursor.close()
		mydb.close()

		registros=[]
		for i in resultados:
			registros.append(['ID: '+str(i[0])+' | A cada '+str(i[2])+' dia(s) | horário: '+str(i[3])+''])

		if registros != []:
			return registros
		else:
			return False
	else:
		return False

# Exclui relatório periódico (muda status)
def excluirRelatorioPeriodico(id):
	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	try:
		mycursor = mydb.cursor()

		statement = "UPDATE relatorio_periodico set status = 'desativado' where id='"+id+"'"
		mycursor.execute(statement)
		count = mycursor.rowcount
		mycursor.close()
		mydb.close()

		return True
	except Exception as err:
		return '\nErro: '+str(err)+'\n'

# insere um novo relatório periódico no banco
def insereRelatorioPeriodico(usuario,periodicidade,horario,destinatarios,f_usuario,f_acao,status):
	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	try:
		mycursor = mydb.cursor()
		statement = '''INSERT INTO relatorio_periodico (usuario, periodicidade, horario, destinatarios, 
		f_usuario, f_acao, status) values ("'''+usuario+'''",'''+periodicidade+''',"'''+horario+'''",
		"'''+destinatarios+'''","'''+f_usuario+'''","'''+f_acao+'''",
		"'''+status+'''")'''
		mycursor.execute(statement)
		id = mycursor.lastrowid
		mycursor.close()
		mydb.close()

		return True
	except Exception as err:
		return '\nErro: '+str(err)+'\n'

# Altera alguma informação em um relatório periódico
def mudarRelatorioPeriodico(id,parametro,valor):
	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()

	statement = "UPDATE relatorio_periodico set "+parametro+"='"+valor+"' where id='"+id+"'"
	mycursor.execute(statement)
	count = mycursor.rowcount
	mycursor.close()
	mydb.close()

# Para testes:
# print(insereRelatorioPeriodico('Vitalli Streliaev Centeno Kyburz','2','15:45:00',"['Vitalli Streliaev Centeno Kyburz','Vitalli Streliaev Centeno Kyburz']",'01/10/2021','05/10/2021','Vitalli Streliaev Centeno Kyburz','','ativo'))

# Para testes:
# mudarRelatorioPeriodico('1','horario','08:14:00')