import mysql.connector


#dadosDeslocamento = (km, cnpj, cc, data, placa, descricao, nome, caminho_imagem, status, responsavel_dif, tipo, reembolso,chat_id, msg_id)
#dadosDespesa = (valor, cnpj, cc, data, cartao, descricao, nome, caminho_imagem, status, responsavel_dif, tipo_gasto,chat_id,msg_id)
#dadosAbastecimento = (valor, cnpj, cc, data, cartao, litros, km, nome, caminho_imagem, status, responsavel_dif, placa,chat_id,msg_id)


def insereDeslocamento(dadosDeslocamento):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "INSERT INTO deslocamentosteste ( km, cnpj, cc, data, placa, descricao, nome, caminho_imagem, status, responsavel_dif, tipo, reembolso,chat_id, msg_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosDeslocamento)
	mycursor.close()
	mydb.close()


def insereDespesa(dadosDespesa):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "INSERT INTO notinhasteste ( valor, cnpj, cc, data, cartao, descricao, nome, caminho_imagem, status, responsavel_dif, tipo_gasto,chat_id,msg_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosDespesa)
	mycursor.close()
	mydb.close()


def insereAbastecimento(dadosAbastecimento):

	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)
	
	mycursor = mydb.cursor()
	statement = "INSERT INTO abastecimentoteste ( valor, cnpj, cc, data, cartao, litros, km, nome, caminho_imagem, status, responsavel_dif, placa,chat_id,msg_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
	mycursor.execute(statement, dadosAbastecimento)
	mycursor.close()
	mydb.close()

def agaparRegistro(tipo, id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "UPDATE "+tipo+"teste set status = 'Apagado' where id="+str(id)+" AND status != 'Lancada'"
	mycursor.execute(statement)
	mycursor.close()
	mydb.close()

def validaApagarRegistro(tipo, telegram_id, id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT * FROM "+tipo+"teste WHERE chat_id="+str(telegram_id)+" AND id="+str(id)+" AND status = 'Aberto'"

	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	if resultados:
		return True
	else:
		return False


def listarDeslocamentosAbertos(telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT id, tipo, cc, km, placa, data, descricao from deslocamentosteste where chat_id="+str(telegram_id)+" AND status = 'Aberto' order by id desc"
	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	registros=[]
	for i in resultados:
		registros.append(['ID: '+str(i[0])+' | CC: '+str(i[2])+' | Data: '+str(i[5])+' | Tipo: '+str(i[1])+' | KM: '+str(i[3])+' | Descricao: '+str(i[6])])


	return registros

def listarDespesasAbertas(telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT id, valor, cc, descricao, data, cnpj,cartao from notinhasteste where chat_id="+str(telegram_id)+" AND status = 'Aberto' order by id desc"
	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	registros=[]
	for i in resultados:
		registros.append(['ID: '+str(i[0])+' | Valor:: '+str(i[1])+' | CC: '+str(i[2])+' | Descrição: '+str(i[3])+' | Data: '+str(i[4])+' | CNPJ: '+str(i[5])+' | Pagamento: '+str(i[6])])


	return registros

def listarAbastecimentosAbertos(telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT id, valor, cc, descricao, data, cnpj, cartao, placa from abastecimentoteste where chat_id="+str(telegram_id)+" AND status = 'Aberto' order by id desc"
	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	registros=[]
	for i in resultados:
		registros.append(['ID: '+str(i[0])+' | Valor:: '+str(i[1])+' | CC: '+str(i[2])+' | Placa: '+str(i[7])+' | Descrição: '+str(i[3])+' | Data: '+str(i[4])+' | CNPJ: '+str(i[5])+' | Pagamento: '+str(i[6])])


	return registros

def verificaCadastro(telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT * from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	if resultados:
		return True
	else:
		return False


def salvaID(email, telegram_id):
	mydb = mysql.connector.connect(
	  host="45.7.171.172",
	  port=1940,
	  autocommit=True,
	  user="admin",
	  password="m%Le$3xA",
	  database="my_schema"
	)

	mycursor = mydb.cursor()
	statement = "SELECT * from dados_usuario where id_telegram="+str(telegram_id)+" AND email='"+email+"'"
	mycursor.execute(statement)
	resultados = mycursor.fetchall()

	if resultados:
		mycursor.close()
		mydb.close()
		return 3
	else:
		statement = "SELECT * from dados_usuario where email='"+email+"'"
		mycursor.execute(statement)
		resultados = mycursor.fetchall()
		if resultados:
			statement = "UPDATE dados_usuario set id_telegram="+str(telegram_id)+" where email='"+email+"'"
			mycursor.execute(statement)
			count = mycursor.rowcount
			if count:
				mycursor.close()
				mydb.close()
				return 1

		else:
			mycursor.close()
			mydb.close()
			return 2

	mycursor.close()
	mydb.close()
