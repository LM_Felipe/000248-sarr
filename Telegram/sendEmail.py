'''
Arquivo para o envio de e-mails
'''

import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os.path
from datetime import datetime

# Envia e-mail com informações dos veículos para responsáveis
def sendEmailVeiculo(veiculo,placa,km,corpo_email,data,email,nome):
	# Define o e-mail que vai aparecer como remetente
	sender_email = "noreply@lmlogix.com.br"
	receiver_email = email
	password = 'tfqfwhcnvtjzcwbb'

	# Informações do e-mail
	message = MIMEMultipart("alternative")
	message["Subject"] = "E-mail informativo sobre irregularidades em veículo"
	message["From"] = sender_email
	message["To"] = receiver_email

	# Define o e-mail que vai enviar a mensagem
	username = "rafael.mattos@lmlogix.com.br"
	password = "tfqfwhcnvtjzcwbb"
	mail_from = "noreply@lmlogix.com.br"
	# Destinatário
	mail_to = email
	# Título do e-mail
	mail_subject = "E-mail informativo sobre irregularidades em veículo"

	# Pega o primeiro nome do destinatário
	nome = nome.split(' ')
	nome = nome[0]

	# Corpo do e-mail
	mail_body = '''Olá, '''+nome+'''!
	<br>Este é um e-mail automático com informações sobre o veículo '''+veiculo+''', de placa '''+placa+'''.
	<br><br>Quilometragem hodômetro: '''+str(km)+''' km.'''+corpo_email+'''
	<br><br>Quando isto ocorreu: <br>'''+str(data)+'''.'''

	# Assinatura do e-mail
	report_file = open('assinatura_sarr.html')
	mail_body = mail_body + report_file.read()
	html = mail_body

	# Turn these into plain/html MIMEText objects
	part2 = MIMEText(html, "html")

	# Add HTML/plain-text parts to MIMEMultipart message
	# The email client will try to render the last part first
	message.attach(part2)

	# Tenta enviar o e-mail
	try:
	    server = smtplib.SMTP('smtp.office365.com', 587)
	    server.ehlo()
	    server.starttls()
	    server.login(username, password)
	    text = message.as_string()
	    server.sendmail(sender_email, receiver_email, text)
	    print('email sent')
	    server.quit()
	# Falha no envio do e-mail	    
	except:
	    print("SMPT server connection error")
	return True

# Para testes:
# sendEmailVeiculo('prisma','MFJ-2623',13045,'','03/07/2021','vitalli.centeno@lmlogix.com.br','Vitalli')

# Envia e-mail com relatório gerado pelo bot
def sendEmailRelatorio(remetente, nome, email, titulo, nome_arquivo):
	# Define o e-mail que vai aparecer como remetente
	sender_email = "noreply@lmlogix.com.br"
	receiver_email = email
	password = 'tfqfwhcnvtjzcwbb'

	# Informações do e-mail
	message = MIMEMultipart("alternative")
	message["Subject"] = titulo
	message["From"] = sender_email
	message["To"] = receiver_email

	# Define o e-mail que vai enviar a mensagem
	username = "rafael.mattos@lmlogix.com.br"
	password = "tfqfwhcnvtjzcwbb"
	mail_from = "noreply@lmlogix.com.br"
	# Destinatário
	mail_to = email
	# Título do e-mail
	mail_subject = titulo

	# Pega o primeiro nome do destinatário
	nome = nome.split(' ')
	nome = nome[0]

	# Data e hora do momento
	data = datetime.today()

	# Corpo do e-mail
	mail_body = '''Olá, '''+nome+'''!
	<br><br>Este é um e-mail enviado por '''+remetente+''' via bot SARR.
	<br><br>Anexo o relatório de log do bot SARR.
	<br><br>Quando isto ocorreu: <br>'''+str(data)+'''.'''

	# Assinatura do e-mail
	# No servidor é: "report_file = open('../../../../../../../Telegram/assinatura_sarr.html')"
	report_file = open('../../../../../../../Telegram/assinatura_sarr.html')
	mail_body = mail_body + report_file.read()
	html = mail_body

	# Turn these into plain/html MIMEText objects
	part2 = MIMEText(html, "html")

	# Add HTML/plain-text parts to MIMEMultipart message
	# The email client will try to render the last part first
	message.attach(part2)

	# Arruma nome do arquivo
	nome_dividido = nome_arquivo.split('_')
	novo_nome = ''
	for i in nome_dividido:
	    if i != nome_dividido[len(nome_dividido)-1]:
	        if novo_nome != '':
	            novo_nome = novo_nome + '_' + i
	        else:
	            novo_nome = novo_nome + i

	# Novo nome do arquivo
	novo_nome = novo_nome + '.pdf'

	filename = novo_nome

	# Adiciona o relatório como anexo
	attachment = open('../../../../../../../Telegram/'+nome_arquivo,'rb')
	# No bot SARR do servidor:
	# attachment = open('../../../../../../../Telegram/'+nome_arquivo,'rb')

	part = MIMEBase('application', 'octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

	message.attach(part)

	attachment.close()

	# Tenta enviar o e-mail
	try:
	    server = smtplib.SMTP('smtp.office365.com', 587)
	    server.ehlo()
	    server.starttls()
	    server.login(username, password)
	    text = message.as_string()    
	    server.sendmail(sender_email, receiver_email, text)
	    print('e-mail enviado')
	    server.quit()

	    return True
	# Falha no envio do e-mail
	except Exception as err:
		return '\nErro: '+str(err)+'\n'