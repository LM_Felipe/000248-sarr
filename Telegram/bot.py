'''
Arquivo principal para executar o bot

Deve ser criado um serviço no servidor para executar este arquivo ("bot.py")
Para cada bot deve ser colocado seu token neste arquivo em "Updater"
Os arquivos importados precisam estar na mesma pasta
'''

#!/usr/bin/env python
# pylint: disable=W0613, C0116
# type: ignore[union-attr]
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

# Importa módulos e outros arquivos
import logging
import re
import datetime
import math
import os
from slugify import slugify

import telegramcalendar
import mysql_funcoes as my_sql
import get_CAR_CC as get_info
import controle_clp
import relatorio_log
import sendEmail
import relatorio_periodico_funcoes as rp_funcoes

import json

from uuid import uuid4

from validate_docbr import CNPJ
from validate_docbr import CPF

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update, InlineQueryResultArticle, ParseMode, InputTextMessageContent
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
    CallbackQueryHandler,
    InlineQueryHandler,
)
from telegram.utils.helpers import escape_markdown

import requests

# Valida CNPJ
cnpj_verify = CNPJ()
# Valida CPF
cpf_verify = CPF()

# Formato de data dia/mês/ano
date_format = '%d/%m/%y'

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

# Mensagem enviado pelo bot ao cancelar a conversa
mensagem_saida = 'Tchau, até a próxima!\nPara iniciar um novo registro aperte: /start'

logger = logging.getLogger(__name__)

# Funções utilizadas no arquivo
TIPO_REGISTRO, DATA_REG, CC, QUILOMETRAGEM, TIPO_QM, DESCRICAO, FOTOGRAFIA, NF, CNPJ, ABASTECIMENTO, VALOR, VEICULO, CARTAO, SELECT_CARTAO, DIGITOS_CARTAO, QUILOMETRO_ABASTECIDO, APAGAR_REGISTRO, CONFIRMACAO_APAGAR, AGORA_APAGOU, REGISTRO_EMAIL,SELECT_REGISTRO,NUM_PEDAGIOS,INCREMENTAR_DESCRICAO,RELATORIO,SAIDA,ENTRADA,PERNOITE,SOLICITANTE,LOCAL, MODALIDADE,KM_INI_REL,VEICULO_PLANILHA,KM_FIM_REL,MOTORISTA,BASE,ABRIR_PORTA,ABRIR_PORTAO,FECHAR_PORTAO,LIGAR_ILUMINACAO_INTERNA,DESLIGAR_ILUMINACAO_INTERNA,LIGAR_ILUMINACAO_EXTERNA,DESLIGAR_ILUMINACAO_EXTERNA,LIGAR_AR_CONDICIONADO,DESLIGAR_AR_CONDICIONADO,ACESSO_USUARIOS,ACESSO_PERMISSAO,CONFIRMAR_PERMISSAO,LIFE_COUNTER,GERAR_RELATORIO,FILTRO_RELATORIO,FILTRO_USUARIO,TITULO_RELATORIO,ENVIAR_RELATORIO,AGORA_ENVIA,FILTRO_ACAO,TIPO_DATA,ULTIMOS_DIAS,RELATORIO_PERIODICO,EXCLUIR_RELATORIO,AGORA_EXCLUI_RELATORIO,RELATORIO_PERIODICIDADE,RELATORIO_HORARIO,RELATORIO_DESTINATARIO,RELATORIO_PERIODICO_FILTROS,RELATORIO_FILTRO_USUARIO,RELATORIO_FILTRO_ACAO,CRIAR_RELATORIO_PERIODICO,EDITAR_RELATORIO = range(68)

# Teste para abrir porta pelo deep link
PORTA = "abrir_porta_teste"
PORTAO = "abrir_portao_teste"

'''
Padrões de teclado
a serem mostrados
ao usuário:
'''

# Teste para abrir porta pelo deep link
def abrir_porta_teste(update: Update, context: CallbackContext) -> None:
    print('\nteste\n')
    
    if controle_clp.abre_porta():
        mensagem = 'Comando "abrir" enviado para a porta. O que mais você deseja controlar?'
    else:
        mensagem = 'Falha na comunicação. O que você deseja controlar?' 

    # Teclado a ser exibido ao usuário
    reply_keyboard = reply_keyboard_controles()

    """Reached through the USING_KEYBOARD payload"""
    payload = context.args

    # Questionamento a ser feito para o usuário
    context.bot.send_message(chat_id=update.message.from_user.id,
                text=mensagem,
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
    return BASE    

# Teclado menu principal
def reply_keyboard_principal(telegram_id):
    # Confere qual é a permissão do usuário
    permissao = my_sql.confere_permissao(telegram_id)

    # Teste de comunicação com CLP
    controles = controle_clp.teste_leitura_tag('SARR_L_Conta')

    # Teclado a ser exibido para o usuário
    if permissao == 'Administrador' or permissao == 'Programador':
        reply_keyboard = [['Quilometragem 📋', 'Nota Fiscal 📋'], 
        ['Abastecimento 📋', 'Apagar registro 📋'],['Pedágio 📋', 'Ponto 📋'], 
        ['Controles'+controles, 'Permissão usuário'], ['Atualiza 🔄', 'Sair ⤴️']]
    else:
        reply_keyboard = [['Quilometragem 📋', 'Nota Fiscal 📋'], ['Abastecimento 📋', 'Apagar registro 📋'],['Pedágio 📋', 'Ponto 📋']]
        
    return reply_keyboard

# Teclado para a função "Controles"
def reply_keyboard_controles():
    # Testa comunicação com CLP
    tag = controle_clp.teste_leitura_tags_clp()

    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Porta'+tag[0], 'Portão'+tag[1]],
                ['Iluminação interna'+tag[2], 'Iluminação externa'+tag[3]], 
                ['Relatório 📋', 'Lifecounter ('+str(tag[5])+')'], 
                ['Ajuda 🆘', 'Voltar ⬅️'], ['Atualiza 🔄', 'Sair ⤴️']]
    return reply_keyboard

# Teclado padrão para controle de dispositivo
def reply_keyboard_porta():
    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Sim ✅', 'Voltar ⬅️'], ['Atualiza 🔄', 'Sair ⤴️']]
    return reply_keyboard

# Teclado padrão para geração de relatórios
def reply_keyboard_relatorio():
    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Gerar relatório 📋', 'Adicionar filtro 🔎'], ['Ajuda 🆘', 'Voltar ⬅️'],
        ['Relatório periódico 🔄', 'Sair ⤴️']]
    return reply_keyboard

# Teclado padrão para filtro de relatórios
def reply_keyboard_filtro_relatorio():
    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Usuário 👤', 'Data 📆'], ['Ação 🤳', 'Voltar ⬅️'],
        ['Atualiza 🔄', 'Sair ⤴️']]
    return reply_keyboard

# Teclado padrão para filtro de ações
def reply_keyboard_filtro_acao():
    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Abrir porta'],['Abrir portão'],['Comando para portão'],
        ['Ligar iluminação interna'],['Desligar iluminação interna'],['Ligar iluminação externa'],
        ['Desligar iluminação externa'],['Ligar ar-condicionado'],['Desligar ar-condicionado']]
    return reply_keyboard

# Teclado padrão para relatórios periódicos
def reply_keyboard_relatorio_periodico():
    # Teclado a ser exibido para o usuário
    #reply_keyboard = [['Criar 🆕', 'Editar ✏️'],['Excluir 🗑', 'Voltar ⬅️'], ['Atualiza 🔄', 'Sair ⤴️']]
    reply_keyboard = [['Criar 🆕', 'Voltar ⬅️'], ['Excluir 🗑', 'Sair ⤴️']]
    return reply_keyboard

# Teclado padrão para selecionar número de dias
def reply_keyboard_dias():
    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Últimos 2 dias'],['Últimos 3 dias'],['Últimos 5 dias'],['Últimos 7 dias'],
        ['Últimos 30 dias']]
    return reply_keyboard

'''
Funções do bot
Argumentos: (update: Update, context: CallbackContext)
Precisam ser listadas no início e fim do programa
'''

# Pega a modalidade para o lançamento de horas
def modalidade(update: Update, context: CallbackContext):
    text = update.message.text

    # Guarda a modalidade em variável global
    context.user_data['modalidade'] = text

    for i in context.user_data['modalidades']:
        if text in i:

            # Teclado a ser exibido para o usuário
            reply_keyboard = get_info.cidades()

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual a cidade?',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )
            # Próxima pergunta
            return LOCAL

    # Teclado a ser exibido para o usuário
    reply_keyboard = context.user_data['modalidades']

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Opção inválida! Tente novamente.',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    # Próxima pergunta
    return MODALIDADE

# Pega a cidade para lançamento de horas
def local(update: Update, context: CallbackContext):
    text = update.message.text

    # Guarda a cidade em variável global
    context.user_data['cidade'] = text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Marcelo Martins de Macedo', 'Lourival Manoel Machado','Ricardo Mendonça'],['Diogo Matos Schneider','Miriam Soares Machado']]
    
    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Quem é o solicitante?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    # Próxima pergunta
    return SOLICITANTE

# Pega o solicitante para o lançamento de horas
def solicitante(update: Update, context: CallbackContext):
    text = update.message.text

    # Guarda o solicitante em variável global
    context.user_data['solicitante'] = text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Modalidade diferente de "Campo"
    if not(context.user_data['modalidade']=='Campo'):
        context.user_data['pernoite'] = ""

        # Finaliza a conversa
        update.message.reply_text(
            'Qual o horário de entrada? (utilizar formato hh:mm)\n\nPara considerar padrão, envie:\n/manha ou\n\n/tarde',
            reply_markup=ReplyKeyboardRemove()    )  
        # Próxima pergunta
        return ENTRADA

    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Sim', 'Não']]

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'É pernoite?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    # Próxima pergunta
    return PERNOITE

# Vê se é pernoite ou não: para atividade em campo
def pernoite(update: Update, context: CallbackContext):
    text = update.message.text
    
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Valida resposta para "Sim" ou "Não"
    if text == 'Sim' or text == 'Não':
        if text == 'Sim':
            context.user_data['pernoite'] = 'Pernoite'
        else:
            context.user_data['pernoite'] = ''

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o horário de entrada? (utilizar formato hh:mm)\n\nPara considerar padrão, envie:\n/manha ou\n\n/tarde',
            reply_markup=ReplyKeyboardRemove()    )  
        # Próxima pergunta
        return ENTRADA
    # Resposta inválida
    else:
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Sim', 'Não']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Resposta inválida! Tente novamente com "Sim" ou "Não":',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return PERNOITE

# Pega horário de entrada para lançamento de horas
def entrada(update: Update, context: CallbackContext):
    text = update.message.text

    # Guarda o horário de entrada em variável global
    context.user_data['entrada'] = text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Opção de horário padrão pela manhã (7:42 - 12:00)
    elif text == '/manha':
        user = update.message.from_user
        context.user_data['entrada']='7:42'
        context.user_data['saida']='12:00'

        # Modalidade selecionada: "Deslocamento"
        if context.user_data['modalidade']=='Deslocamento':

            # Teclado a ser exibido para o usuário
            reply_keyboard = get_info.carListPlanilha();

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual o veículo utilizado?',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            );
            # Próxima pergunta
            return VEICULO_PLANILHA

        else:
            # Repetir lançamento anterior
            if context.user_data['repetir']==1:

                # Questionamento a ser feito para o usuário
                update.message.reply_text(
                'Informe a descrição do lançamento:\n\nUse /repetir para registrar "'+context.user_data['lastReport'][0][18]+'"',
                reply_markup=ReplyKeyboardRemove()    ) 
                # Próxima pergunta
                return RELATORIO
            else:
                # Questionamento a ser feito para o usuário
                update.message.reply_text(
                    'Informe a descrição do lançamento:',
                    reply_markup=ReplyKeyboardRemove()    ) 
                # Próxima pergunta
                return RELATORIO
    # Opção de horário padrão pela tarde (13:00 - 18:00)
    elif text == '/tarde':
        user = update.message.from_user
        # Horário de entrada
        context.user_data['entrada']='13:30'
        # Horário de saída
        context.user_data['saida']='18:00'
        
        # Modalidade "Deslocamento"
        if context.user_data['modalidade']=='Deslocamento':

            # Teclado a ser exibido para o usuário
            reply_keyboard = get_info.carListPlanilha();

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual o veículo utilizado?',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            );
            # Próxima pergunta
            return VEICULO_PLANILHA
        # Modalidade não é "Deslocamento"
        else:
            # Repetir lançamento anterior
            if context.user_data['repetir']==1:

                # Questionamento a ser feito para o usuário
                update.message.reply_text(
                'Informe a descrição do lançamento:\n\nUse /repetir para registrar "'+context.user_data['lastReport'][0][18]+'"',
                reply_markup=ReplyKeyboardRemove()    ) 
                # Próxima pergunta
                return RELATORIO
            else:

                # Questionamento a ser feito para o usuário
                update.message.reply_text(
                    'Informe a descrição do lançamento:',
                    reply_markup=ReplyKeyboardRemove()    ) 
                # Próxima pergunta
                return RELATORIO

    # Horário digitado
    elif re.search("^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$",text):
        # Validação do horário
        if (datetime.datetime.today()-datetime.datetime.strptime(context.user_data['data']+' '+text, "%d/%m/%y %H:%M")).days<0:
            
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Você está tentando lançar um horário posterior à hora atual! Tente novamente!:',
                reply_markup=ReplyKeyboardRemove()    ) 
            # Próxima pergunta
            return ENTRADA
        
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o horário de saída? (utilizar formato hh:mm)',
            reply_markup=ReplyKeyboardRemove()    ) 
        # Próxima pergunta
        return SAIDA
    # Formato de horário inválido
    else:
        update.message.reply_text(
            'Formato inválido! Tente novamente utilizando o formato hh:mm:',
            reply_markup=ReplyKeyboardRemove()    ) 
        return ENTRADA

# Horário de saída para o lançamento de horas
def saida(update: Update, context: CallbackContext):
    text = update.message.text
    context.user_data['saida'] = text
    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Validação de horário
    if re.search("^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$",text):
        entrada = datetime.datetime.strptime(context.user_data['entrada'], "%H:%M")
        saida = datetime.datetime.strptime(context.user_data['saida'], "%H:%M")
        dif = saida - entrada

        # Entrada posterior à saída
        if dif.days <0:
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
            'Horários inconsistentes, a entrada não pode ser posterior à saída!\n\nQual o horário de entrada? (utilizar formato hh:mm)',
            reply_markup=ReplyKeyboardRemove())
            # Próxima pergunta
            return ENTRADA

        # Modalidade "Deslocamento"
        if context.user_data['modalidade']=='Deslocamento':
            reply_keyboard = get_info.carListPlanilha();

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual o veículo utilizado?',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            );
            # Próxima pergunta
            return VEICULO_PLANILHA 
        # Modalidade não é "Deslocamento"
        else:
            # Repetir lançamento anterior
            if context.user_data['repetir']==1:
                # Questionamento a ser feito para o usuário
                update.message.reply_text(
                'Informe a descrição do lançamento:\n\nUse /repetir para registrar "'+context.user_data['lastReport'][0][18]+'"',
                reply_markup=ReplyKeyboardRemove()) 
                # Próxima pergunta
                return RELATORIO
            else:
                # Questionamento a ser feito para o usuário
                update.message.reply_text(
                    'Informe a descrição do lançamento:',
                    reply_markup=ReplyKeyboardRemove()) 
                # Próxima pergunta
                return RELATORIO
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Formato inválido! Tente novamente utilizando o formato hh:mm:',
            reply_markup=ReplyKeyboardRemove())
        # Próxima pergunta
        return SAIDA

# Veículo utilizado (para lançamento de horas)
def veiculo_planilha(update: Update, context: CallbackContext):
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Lista veículos cadastrados
    for i in get_info.carListPlanilha():
        if text in i:
            update.message.reply_text(
                'Qual a quilometragem incial?', reply_markup=ReplyKeyboardRemove()
            )
            # Variável global para guardar veículo
            context.user_data['veiculo']=text
            return KM_INI_REL

    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.carListPlanilha();

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Veículo inválido! Tente novamente.',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    );
    # Próxima pergunta
    return VEICULO_PLANILHA

# Quilometragem inicial para lançamento de horas
def km_ini_rel(update: Update, context: CallbackContext):
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Valida se é um número
    if text.isnumeric():
        # Guarda o km inicial em variável global
        context.user_data['ini']=text

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'E qual a final?',
           reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return KM_FIM_REL   
    else: 
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'O valor inserido tem de ser um número! Tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return KM_INI_REL

# Quilometragem final para lançamento de horas
def km_fim_rel(update: Update, context: CallbackContext):
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Valida se é um número
    if text.isnumeric():
        # Guarda o km final em variável global
        context.user_data['fim']=text

        # km final menor do que a incial
        if int(text) < int(context.user_data['fim']):
            update.message.reply_text(
                'Quilometragens inconsistentes, a final não pode ser menor que a inicial!\n\nInforme o km inicial:',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return KM_INI_REL

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Sim', 'Não']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Você estava dirigindo?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return MOTORISTA   
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'O valor inserido tem de ser um número! Tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return KM_FIM_REL

# Pega o motorista para o lançamento de horas
def motorista(update: Update, context: CallbackContext):

    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Resposta diferente de "Sim" e "Não"
    if not(text=='Sim' or text=='Não'):
        reply_keyboard = [['Sim', 'Não']]
        update.message.reply_text(
            'Opção inválida! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return MOTORISTA

    else:
        # O usuário foi motorista
        if text == 'Sim':
            # Guarda opção motorista/carona na variável global
            context.user_data['motorista']='Motorista'

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Informe a descrição do lançamento:',
                reply_markup=ReplyKeyboardRemove()    ) 
            # Próxima pergunta
            return RELATORIO
        # O usuário foi carona
        else:
            # Guarda opção motorista/carona na variável global
            context.user_data['motorista']='Carona'

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Informe a descrição do lançamento:',
                reply_markup=ReplyKeyboardRemove()    ) 
            # Próxima pergunta
            return RELATORIO

# Registro de horas de colaborador
def relatorio(update: Update, context: CallbackContext):
    text = update.message.text

    # Utilizar último registro para descrição
    if text=='/repetir':
        context.user_data['descricao']=context.user_data['lastReport'][0][18]
    # Descrição digitada pelo usuário
    else:
        context.user_data['descricao'] = text
    
    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Pega nome e cargo do usuário
    dados=get_info.dados_relatorio(context.user_data['telegram_id'])
    if context.user_data['repetir']==0:
        context.user_data['data'] = datetime.datetime.strptime(context.user_data['data'], '%d/%m/%y')
    else:
        context.user_data['data'] = datetime.datetime.strptime(context.user_data['data'], '%d/%m/%Y')

    # Insere novo registro de horas
    dadosRelatorio=[dados[0],context.user_data['data'].strftime('%d/%m/%Y'),context.user_data['entrada'],context.user_data['saida'],context.user_data['cc'],context.user_data['modalidade'],context.user_data['cidade'],context.user_data['motorista'],context.user_data['veiculo'],context.user_data['ini'],context.user_data['fim'],"","","",dados[1],context.user_data['solicitante'],context.user_data['pernoite'],context.user_data['descricao'],context.user_data['telegram_id'],'Pendente',]
    my_sql.insereRelatorio(dadosRelatorio)

    # Mensagem de feedback com dados registrados
    feedback_msg= 'Dados registrados:\n\nColaborador: '+dados[0]
    feedback_msg += '\n Data: '+context.user_data['data'].strftime('%d/%m/%Y')
    feedback_msg += '\n CC: '+context.user_data['cc']
    feedback_msg += '\n Modalidade: '+context.user_data['modalidade']
    feedback_msg += '\n Cidade: '+context.user_data['cidade']
    feedback_msg += '\n Cargo: '+dados[1]
    feedback_msg += '\n Solicitante: '+context.user_data['solicitante']
    feedback_msg += '\n Pernoite: '+context.user_data['pernoite']
    feedback_msg += '\n Entrada: '+context.user_data['entrada']
    feedback_msg += '\n Saída: '+context.user_data['saida']
    if context.user_data['modalidade']=='Deslocamento':
        feedback_msg += '\n Motorista: '+context.user_data['motorista']
        feedback_msg += '\n Veículo: '+context.user_data['veiculo']
        feedback_msg += '\n Km Inicial: '+context.user_data['ini']
        feedback_msg += '\n Km Final: '+context.user_data['fim']

    feedback_msg += '\n Descrição: '+context.user_data['descricao']
    feedback_msg += '\n\nPara enviar um novo registro, envie /start'

    update.message.reply_text(
        feedback_msg, reply_markup=ReplyKeyboardRemove()) 
    return ConversationHandler.END

def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper

def inline_handler(update: Update, context: CallbackContext) -> int:
    selected,date = telegramcalendar.process_calendar_selection(update,context)
    if selected:
        context.user_data['data'] = date.strftime("%d/%m/%y")

        try:
          date_obj = datetime.datetime.strptime(date.strftime("%d/%m/%y"), date_format)
        except ValueError:
            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="O formato da data está errado! Tente novamente.",
                        reply_markup=telegramcalendar.create_calendar(),)

            # Próxima pergunta
            return DATA_REG       

        dif_datas = datetime.datetime.today() - datetime.datetime.strptime(context.user_data['data'], "%d/%m/%y")
        if dif_datas.days<0:
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você não pode lançar uma data futura! Tente novamente:",
                        reply_markup=telegramcalendar.create_calendar(),)

            # Próxima pergunta
            return DATA_REG

        # Opção escolhida: "Quilomentragem"
        if  context.user_data['choice'] == "Quilometragem 📋":
             # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você selecionou: %s\n\nQual a quilometragem marcada no hodômetro?" % 
                        (date.strftime("%d/%m/%Y")),
                        reply_markup=ReplyKeyboardRemove(),)
            # Próxima pergunta
            return QUILOMETRAGEM

        # Opção escolhida: "Nota Fiscal"
        if context.user_data['choice'] == 'Nota Fiscal 📋':
            # Teclado a ser exibido para o usuário
            reply_keyboard = get_info.cnpjList(context.user_data['cc'])

            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você selecionou: %s\n\nQual o CNPJ da nota fiscal?" % (date.strftime("%d/%m/%Y")),
                        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            # Próxima pergunta
            return CNPJ

        # Opção escolhida: "Pedágio"
        if context.user_data['choice'] == 'Pedágio 📋':
            # Teclado a ser exibido para o usuário
            reply_keyboard = get_info.cnpjListPedagio(context.user_data['cc'])

            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você selecionou: %s\n\nQual o CNPJ da nota fiscal?" % (date.strftime("%d/%m/%Y")),
                        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            # Próxima pergunta
            return CNPJ

        # Opção escolhida: "Abastecimento"
        if context.user_data['choice'] == 'Abastecimento 📋':
            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você selecionou: %s\n\nQual a quilometragem marcando quando abastecid?" 
                        % (date.strftime("%d/%m/%Y")),
                        reply_markup=ReplyKeyboardRemove(),)
            # Próxima pergunta
            return QUILOMETRO_ABASTECIDO

        # Opção escolhida: "Ponto" (lançamento de horas)
        if context.user_data['choice'] == 'Ponto 📋':
            # Teclado a ser exibido para o usuário
            reply_keyboard= context.user_data['modalidades']

            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você selecionou: %s\n\nQual a modalidade?" % (date.strftime("%d/%m/%Y")),
                        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            # Próxima pergunta
            return QUILOMETRO_ABASTECIDO

        # Caso tenha sido escolhida a data inicial do relatório
        if  context.user_data['choice'] == "Controles 🙂":
            # Data inicial recebida
            context.user_data['data_ini'] = date.strftime("%d/%m/%Y")

            # Muda para data final
            context.user_data['choice'] = "Data final"

             # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você selecionou: %s\n\nQual a data final?" % (date.strftime("%d/%m/%Y")),
                        reply_markup=telegramcalendar.create_calendar(),)
            # Próxima pergunta
            return GERAR_RELATORIO

        # Para a data final do relatório
        if  context.user_data['choice'] == "Data final":
            # Data final recebida
            context.user_data['data_fim'] = date.strftime("%d/%m/%Y")

            # Teclado a ser exibido ao usuário
            reply_keyboard = reply_keyboard_relatorio()

            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.callback_query.from_user.id,
                        text="Você selecionou: %s\n\nVocê deseja gerar o relatório ou adicionar algum filtro?" 
                        % (date.strftime("%d/%m/%Y")),
                        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            # Próxima pergunta
            return GERAR_RELATORIO            

# Função executada quando o usuário envia "/start" para começar a conversa
def start(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    telegram_id = user.id
    context.user_data['telegram_id'] = user.id

    # Seta os filtros do relatório de log dos controles feitos pelo bot
    context.user_data['usuario'] = ''
    context.user_data['data_ini'] = ''
    context.user_data['data_fim'] = ''
    context.user_data['acao'] = ''

    # Teclado a ser exibido ao usuário
    reply_keyboard = reply_keyboard_principal(context.user_data['telegram_id'])

    # Definição das modadlidades do lançamento de horas
    context.user_data['modalidades']=[['Campo','Base','Deslocamento'],['Home Office','Assistência Remota','Sobreaviso'],['Emergência Campo','Emergência Base']]

    # Verifica cadastro de usuário conforme ID do Telegram
    if my_sql.verificaCadastro(telegram_id):
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Olá, seja bem-vindo ao sistema SARR LMLogix\n'
            'Para cancelar o registro, envie /cancelar\n\n'
            'O que você deseja registrar?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return TIPO_REGISTRO
    # Caso o número do usuário ainda não esteja cadastrado no bot
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Olá, seja bem-vindo ao sistema SARR LMLogix\n\n'
            'Verifiquei que seu número não está cadatrado\n'
            'Caso você possua cadastro no SARR, insira seu e-mail abaixo, para registrar o seu número de telefone:',
            reply_markup=ReplyKeyboardRemove(),
        )

        # Próxima pergunta
        return REGISTRO_EMAIL

# Registrar número de telefone pelo e-mail
def registro_email(update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text

    user = update.message.from_user
    telegram_id = user.id

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user

        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    tentativa_gravacao = str(my_sql.salvaID(text, telegram_id))

    # Cadastro realizado
    if  tentativa_gravacao == '1':
        update.message.reply_text(
        'Cadastro realizado com sucesso !\nPara iniciar um novo registro aperte: /start' , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # E-mail já possui ID do Telegram
    if  tentativa_gravacao == '3':
        update.message.reply_text(
        'Esse e-mail já está associado com um número !\nEntre em contato com a LMLogix para verificar' , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # E-mail não cadastrado no banco
    if  tentativa_gravacao == '2':
        update.message.reply_text(
        'Esse e-mail não está cadastrado na LMLogix !\nEntre em contato com a LMLogix para verificar' , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

# Tipo de registro escolhido pelo usuário do menu apresentado após o "/start"
def tipo_registro(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.ccList() #[['251-GM-Conversão CPU', '245-LM-Sistema WEB', '248-LM-SARR']]

    # Resposta recebida do usuário
    text = update.message.text
    # Informação gravada no session do usuário
    context.user_data['choice'] = text

    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    user = update.message.from_user
    telegram_id = user.id

    # Para apagar um registro
    if text == 'Apagar registro 📋':    
        reply_keyboard = [['Quilometragem', 'Nota Fiscal', 'Abastecimento']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Selecione qual tipo de registro você deseja apagar:',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return APAGAR_REGISTRO

    # Valida opções apresentadas pelo menu
    if not(text == 'Quilometragem 📋' or text == 'Nota Fiscal 📋' or text == 'Abastecimento 📋' or text == 'Apagar registro 📋' or text == 'Pedágio 📋' or text == 'Ponto 📋' or text == 'Controles 🙂' or text == 'Controles 😐' or 'Atualiza 🔄'):

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_principal(context.user_data['telegram_id'])

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Tipo de registro inválido! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return TIPO_REGISTRO

    # Para lançar uma quilometragem
    if  context.user_data['choice'] == "Quilometragem 📋":
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Inicial', 'Final']]
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Qual o tipo de quilometragem?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_QM

    # Para lançar um pedágio
    if context.user_data['choice'] == "Pedágio 📋":
        update.message.reply_text(
            'Quantos lançamentos iguais você deseja registrar?' , reply_markup=ReplyKeyboardRemove()
        )
        return NUM_PEDAGIOS
    
    # Para controlar algum dispositivo da Base (comunicação OK)
    if  context.user_data['choice'] == "Controles 🙂":
        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles() 
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Para controlar algum dispositivo da Base (falha na comunicação)
    if  context.user_data['choice'] == "Controles 😐":
        # Faz o teste de comunicação com o CLP
        controles = controle_clp.teste_leitura_tag('SARR_L_Conta')

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_principal(context.user_data['telegram_id'])
        
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Problema de comunicação com os dispositivos. O que você deseja fazer?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_REGISTRO

    # Para controlar a permissão (diferentes menus) de cada usuário do bot
    if  context.user_data['choice'] == "Permissão usuário":
        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes() 
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Para qual usuário você deseja colocar uma permissão?"
                    "\nPara voltar, envie /voltar",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return ACESSO_USUARIOS

    # Para atualizar os dados apresntados
    if  context.user_data['choice'] == "Atualiza 🔄":

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_principal(context.user_data['telegram_id'])

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Dados atualizados. O que você deseja fazer?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_REGISTRO

    # Mensagem para o usuário
    mensagem = 'Qual é o centro de custos do registro?'    

    # Para fazer lançamento de horas
    if text == 'Ponto 📋':
        # Último registro de horas
        lastReport = get_info.ultimoPonto(context.user_data['telegram_id'])
        context.user_data['lastReport']=lastReport
        if lastReport:
            # Mensagem de feedback com dados registrados
            feedback_msg= 'Registro anterior:\n Data: '+lastReport[0][2]
            feedback_msg += '\n CC: '+lastReport[0][5]
            feedback_msg += '\n Modalidade: '+lastReport[0][6]
            feedback_msg += '\n Cidade: '+lastReport[0][7]
            feedback_msg += '\n Cargo: '+lastReport[0][15]
            feedback_msg += '\n Solicitante: '+lastReport[0][16]
            feedback_msg += '\n Pernoite: '+lastReport[0][17]
            feedback_msg += '\n Entrada: '+lastReport[0][3]
            feedback_msg += '\n Saída: '+lastReport[0][4]
            feedback_msg += '\n Descrição: '+lastReport[0][18]
            feedback_msg += '\n\n Se desejares repetir campos anteriores, informando apenas horarios e descrição, envie /repetir.'

            mensagem+= '\n\n'+feedback_msg

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        mensagem, reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    # Próxima pergunta
    return CC

# Pega o número de lançamentos de pedágio
def num_pedagios(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Valida se é número
    if text.isnumeric():
        context.user_data['numPedagios']=text
    else:
        update.message.reply_text(
            'Você precisa informar um número!\nQuantos lançamentos iguais você deseja registrar?' , reply_markup=ReplyKeyboardRemove()
        )
        # Próxima pergunta
        return NUM_PEDAGIOS

    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.ccList()

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Qual é o centro de custos dos registros?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    # Próxima pergunta
    return CC

# Controle dos dispositivos da base
def base(update: Update, context: CallbackContext) -> int:
    # Seta o filtro de usuários do relatório para todos os usuários
    context.user_data['usuario'] = ''

    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Separa texto dos emonjis
    resposta = text
    texto = text.split(' ')
    text = texto[0]

    # Valida opção selecionada
    if not(text == 'Porta' or text == 'Portão' or text == 'Lifecounter' or text == 'Iluminação' or 
        text == 'Relatório' or text == 'Atualiza' or text == 'Voltar' or text == 'Ajuda'):

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Opção inválida! tente novamente.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        # Volta para "BASE"
        return BASE

    # Se a porta estiver fechada
    if resposta == 'Porta (F)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'A porta está fechada. Deseja enviar comando "abrir"?', 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Função para abrir porta
        return ABRIR_PORTA

    # Se o estado da porta não for conhecido
    if resposta == 'Porta (-)':
        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'A porta não está fechada. Deseja enviar comando "abrir"?', 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Função para abrir porta
        return ABRIR_PORTA

    # Se houver problema de comunicação para abrir a porta
    if resposta == 'Porta 😐':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Falha na comunicação. O que você deseja controlar?', 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Volta para "BASE"
        return BASE   

    # Se o portão estiver fechado
    if resposta == 'Portão (F)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'O portão está fechado. Deseja enviar comando "abrir"?', 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Função para abrir portão
        return ABRIR_PORTAO

    # Se o estado do portão não for conhecido
    if resposta == 'Portão (-)':

        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()
        update.message.reply_text(
            'O portão não está fechado. Deseja enviar comando para o portão?', 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Função para comandar portão
        return FECHAR_PORTAO

    # Se houver problema de comunicação para comandar portão
    if resposta == 'Portão 😐':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles()

        # Teclado a ser exibido para o usuário
        update.message.reply_text(
            'Falha na comunicação. O que você deseja controlar?', 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Volta para "BASE"
        return BASE

    # Para mostrar lifecounter
    if text == 'Lifecounter':

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Ajuda 🆘', 'Voltar ⬅️'], ['Atualiza 🔄', 'Sair ⤴️']]

        # Leitura das tags do lifecounter
        leitura = controle_clp.life_counter()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Tag de leitura = "+str(leitura[0])+
                    "\nTag de escrita = "+str(leitura[0]),
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return LIFE_COUNTER

    # Para ligar iluminação interna
    if resposta == 'Iluminação interna (D)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta() 

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="A iluminação interna está desligada. Deseja ligar a iluminação interna?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return LIGAR_ILUMINACAO_INTERNA

    # Para desligar iluminação interna
    if resposta == 'Iluminação interna (L)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta() 

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="A iluminação interna está ligada. Deseja desligar a iluminação interna?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return DESLIGAR_ILUMINACAO_INTERNA        

    # Estado da iluminação interna desconhecido
    if resposta == 'Iluminação interna (-)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O estado da iluminação interna é desconhecido. O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        # Volta para "BASE"
        return BASE

    # Problema de comunicação para controlar iluminação interna
    if resposta == 'Iluminação interna 😐':

        reply_keyboard = reply_keyboard_controles()       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Falha na comunicação. O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Para ligar iluminação externa
    if resposta == 'Iluminação externa (D)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="A iluminação externa está desligada. Deseja ligar a iluminação externa?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return LIGAR_ILUMINACAO_EXTERNA

    # Para desligar iluminação externa
    if resposta == 'Iluminação externa (L)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="A iluminação externa está ligada. Deseja desligar a iluminação externa?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return DESLIGAR_ILUMINACAO_EXTERNA        

    # Estado da iluminação externa desconhecido
    if resposta == 'Iluminação externa (-)':

        reply_keyboard = reply_keyboard_controles()       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O estado da iluminação externa é desconhecido. O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Problema de comunicação para controlar iluminação externa
    if resposta == 'Iluminação externa 😐':

        reply_keyboard = reply_keyboard_controles()       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Falha na comunicação. O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Tenta gerar relatório em pdf da tabela de log
    if resposta == 'Relatório 📋':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você deseja gerar o relatório ou adicionar algum filtro?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return GERAR_RELATORIO

    # Para ligar ar-condicionado
    if resposta == 'Ar-condicionado (D)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O ar-condicionado está desligado. Deseja ligar o ar-condicionado?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return LIGAR_AR_CONDICIONADO

    # Para desligar ar_condicionado
    if resposta == 'Ar-condicionado (L)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O ar-condicionado está ligado. Deseja desligar o ar-condicionado?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return DESLIGAR_AR_CONDICIONADO    

    # Estado do ar-condicionado é desconhecido
    if resposta == 'Ar-condicionado (-)':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O estado do ar-condicionado é desconhecido. O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        # Volta para "BASE"
        return BASE

    # Problema na comunicação para controlar ar-condicionado
    if resposta == 'Ar-condicionado 😐':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Falha na comunicação. O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        # Volta para "BASE"
        return BASE

    # Para atualizar dados mostrados
    if text == "Atualiza":

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Dados atualizados. O que você deseja fazer?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        # Volta para "BASE"
        return BASE

    # Para voltar ao psso anterior
    if  text == "Voltar":
        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_principal(context.user_data['telegram_id'])

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja fazer?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_REGISTRO

    # Para mensagem de ajuda ao usuário
    if resposta == 'Ajuda 🆘':

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="🙂 = Comunicação OK"
                    "\n😐 = Estado do controlador inacessível"
                    "\n(F) = Fechado"
                    "\n(A) = Aberto"
                    "\n(-) = Estado do dispositivo desconhecido"
                    "\n(D) = Desligado"
                    "\n(L) = Ligado"
                    "\n(nn) = Indica o último valor analógico conhecido"
                    "\n\nO que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        # Volta para "BASE"
        return BASE                

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Abrir porta da recepção
def abrir_porta(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para abrir porta
    if text == 'Sim ✅':
        # Envia comando para abrir porta
        if controle_clp.abre_porta():
            # Mensagem enviada para usuário
            mensagem = 'Comando "abrir" enviado para a porta. Deseja enviar comando novamente?'
            
            # Retorno da função
            retorno = ABRIR_PORTA

            # Teclado a ser exibido ao usuário
            reply_keyboard = reply_keyboard_porta()
        else:
            # Mensagem enviada para usuário
            mensagem = 'Falha na comunicação. O que você deseja controlar?'
            
            # Retorno da função
            retorno = BASE

            # Teclado a ser exibido ao usuário
            reply_keyboard = reply_keyboard_controles()

        # Insere nova linha de log de ações de controle realizadas via bot
        my_sql.controle_log(str(context.user_data['telegram_id']), 'Abrir porta')

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return retorno

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Para atualizar dados apresentados
    if text == 'Atualiza 🔄':
        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Mensagem para cada estado da porta
        if tag[0] == ' (F)':
            mensagem = 'A porta está fechada. Deseja enviar comando "abrir"?'
        elif tag[0] == ' (-)':
            mensagem = 'A porta não está fechada. Deseja enviar comando "abrir"?'
        else:
            mensagem = 'Falha na comunicação!'

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        # Volta para "ABRIR_PORTA"
        return ABRIR_PORTA

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Abrir portão externo
def abrir_portao(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para abrir portão
    if text == 'Sim ✅':
        # Envia comando para abrir portão
        if controle_clp.abre_portao():
            # Mensagem enviada para usuário
            mensagem = 'Comando "abrir" enviado para o portão. Deseja enviar comando para o portão novamente?'
        else:
            # Mensagem enviada para usuário
            mensagem = 'Falha na comunicação. O que você deseja controlar?'

        # Insere nova linha de log de ações de controle realizadas via bot
        my_sql.controle_log(context.user_data['telegram_id'], 'Abrir portão')

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FECHAR_PORTAO

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Para atualizar dados apresentados
    if text == 'Atualiza 🔄':
        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Mensagem para cada estado do portão
        if tag[1] == ' (F)':
            mensagem = 'O portão está fechado. Deseja enviar comando "abrir"?'
        elif tag[1] == ' (-)':
            mensagem = 'O portão não está fechado. Deseja enviar comando para o portão?'
        else:
            mensagem = 'Falha na comunicação!'

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return ABRIR_PORTAO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Abrir, parar ou fechar portão externo
def fechar_portao(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para abrir, para ou fechar portão
    if text == 'Sim ✅':

        # Envia comando para abrir/parar/fechar portão
        if controle_clp.abre_portao():
            mensagem = 'Comando enviado para o portão. Deseja enviar comando para o portão novamente?'
        else:
            mensagem = 'Falha na comunicação. O que você deseja controlar?'

        # Insere nova linha de log de ações de controle realizadas via bot
        my_sql.controle_log(str(context.user_data['telegram_id']), 'Comando para portão')

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FECHAR_PORTAO

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Atualiza os dados apresentados
    if text == 'Atualiza 🔄':
        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Mensagem para cada estado do portão
        if tag[1] == ' (F)':
            mensagem = 'O portão está fechado. Deseja enviar comando "abrir"?'
        elif tag[1] == ' (-)':
            mensagem = 'O portão não está fechado. Deseja enviar comando para o portão?'
        else:
            mensagem = 'Falha na comunicação!'

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FECHAR_PORTAO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Ligar a iluminação interna da Base
def ligar_iluminacao_interna(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para ligar iluminação interna
    if text == 'Sim ✅':
        # Envia comando para ligar iluminação interna
        if controle_clp.liga_iluminacao_interna():
            mensagem = "Comando enviado para iluminação interna. O que mais você deseja controlar?"
        else:
            mensagem = 'Falha na comunicação. O que você deseja controlar?'        

        # Insere nova linha de log de ações de controle realizadas via bot
        my_sql.controle_log(str(context.user_data['telegram_id']), 'Ligar iluminação interna')

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Para atualizar os dados apresentados
    if text == 'Atualiza 🔄':
        
        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Mensagem para cada estado da iluminação interna
        if tag[2] == ' (D)':
            mensagem = 'A iluminação interna está desligada. Deseja ligar a iluminação interna?'
        elif tag[2] == ' (L)':
            mensagem = 'A iluminação interna está ligada. Deseja desligar a iluminação interna?'            
        elif tag[2] == ' (-)':
            mensagem = 'O estado da iluminação interna é desconhecido. Deseja enviar comando "ligar/desligar"?'
        else:
            mensagem = 'Falha na comunicação!'

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return LIGAR_ILUMINACAO_INTERNA

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Desligar a iluminação interna da Base
def desligar_iluminacao_interna(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para desligar iluminação interna
    if text == 'Sim ✅':
        # Envia comando para desligar iluminação interna
        if controle_clp.liga_iluminacao_interna():
            mensagem = "Comando enviado para iluminação interna. O que mais você deseja controlar?"
        else:
            mensagem = 'Falha na comunicação. O que você deseja controlar?'  

        # Insere nova linha de log de ações de controle realizadas via bot
        my_sql.controle_log(str(context.user_data['telegram_id']), 'Desligar iluminação interna')

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Para atualizar os dados apresentados
    if text == 'Atualiza 🔄':

        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Mensagem para cada estado da iluminação
        if tag[2] == ' (D)':
            mensagem = 'A iluminação interna está desligada. Deseja ligar a iluminação interna?'
        elif tag[2] == ' (L)':
            mensagem = 'A iluminação interna está ligada. Deseja desligar a iluminação interna?'            
        elif tag[2] == ' (-)':
            mensagem = 'O estado da iluminação interna é desconhecido. Deseja enviar comando "ligar/desligar"?'
        else:
            mensagem = 'Falha na comunicação!'

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return DESLIGAR_ILUMINACAO_INTERNA

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Ligar iluminação externa da Base
def ligar_iluminacao_externa(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para ligar iluminação externa
    if text == 'Sim ✅':

        # Envia comando para ligar iluminação externa
        if controle_clp.liga_iluminacao_externa():
            mensagem = "Comando enviado para iluminação externa. O que mais você deseja controlar?"
        else:
            mensagem = 'Falha na comunicação. O que você deseja controlar?' 

        # Insere nova linha de log de ações de controle realizadas via bot
        my_sql.controle_log(str(context.user_data['telegram_id']), 'Ligar iluminação externa')

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Para atualizar os dados apresentados
    if text == 'Atualiza 🔄':
        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Mensagem para cada estado da iluminação externa
        if tag[3] == ' (D)':
            mensagem = 'A iluminação externa está desligada. Deseja ligar a iluminação externa?'
        elif tag[3] == ' (L)':
            mensagem = 'A iluminação externa está ligada. Deseja desligar a iluminação externa?'            
        elif tag[3] == ' (-)':
            mensagem = 'O estado da iluminação externa é desconhecido. Deseja enviar comando "ligar/desligar"?'
        else:
            mensagem = 'Falha na comunicação!'

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return LIGAR_ILUMINACAO_EXTERNA

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Desligar iluminação externa da Base
def desligar_iluminacao_externa(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para desligar iluminação externa
    if text == 'Sim ✅':

        # Envia comando para desligar iluminação externa
        if controle_clp.liga_iluminacao_externa():
            mensagem = "Comando enviado para iluminação externa. O que mais você deseja controlar?"
        else:
            mensagem = 'Falha na comunicação. O que você deseja controlar?'

        # Insere nova linha de log de ações de controle realizadas via bot
        my_sql.controle_log(str(context.user_data['telegram_id']), 'Desligar iluminação externa')

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Para atualizar os dados apresentados
    if text == 'Atualiza 🔄':

        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Mensagem para cada estado da iluminação externa
        if tag[3] == ' (D)':
            mensagem = 'A iluminação externa está desligada. Deseja ligar a iluminação externa?'
        elif tag[3] == ' (L)':
            mensagem = 'A iluminação externa está ligada. Deseja desligar a iluminação externa?'            
        elif tag[3] == ' (-)':
            mensagem = 'O estado da iluminação externa é desconhecido. Deseja enviar comando "ligar/desligar"?'
        else:
            mensagem = 'Falha na comunicação!'

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return DESLIGAR_ILUMINACAO_EXTERNA

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END    

# Ligar circuito do ar-condicionado da Base
def ligar_ar_condicionado(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para ligar ar-condicionado
    if text == 'Sim ✅':

        # Envia comando para ligar ar-condicionado
        if controle_clp.liga_ar_condicionado():
            mensagem = "Comando enviado para o ar-condicionado. O que mais você deseja controlar?"
        else:
            mensagem = 'Falha na comunicação. O que você deseja controlar?'

        # Insere nova linha de log de ações de controle realizadas via bot
        my_sql.controle_log(str(context.user_data['telegram_id']), 'Ligar ar-condicionado')

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Para atualizar os dados apresentados
    if text == 'Atualiza 🔄':

        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Mensagem para cada estado do ar-condicionado
        if tag[4] == ' (D)':
            mensagem = 'O ar-condicionado está desligado. Deseja ligar o ar-condicionado?'
        elif tag[4] == ' (L)':
            mensagem = 'O ar-condicionado está ligado. Deseja desligar o ar-condicionado?'            
        elif tag[4] == ' (-)':
            mensagem = 'O estado do ar-condicionado é desconhecido. Deseja enviar comando "ligar/desligar"?'
        else:
            mensagem = 'Falha na comunicação!'

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return LIGAR_AR_CONDICIONADO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Desligar circuito do ar-condicionado da Base
def desligar_ar_condicionado(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para desligar o ar-condicionado
    if text == 'Sim ✅':

        # Envia comando para desligar ar-condicionado
        if controle_clp.liga_ar_condicionado():
            mensagem = "Comando enviado para o ar-condicionado. O que mais você deseja controlar?"
        else:
            mensagem = 'Falha na comunicação. O que você deseja controlar?'

        # Insere nova linha de log de ações de controle realizadas via bot
        my_sql.controle_log(str(context.user_data['telegram_id']), 'Desligar ar-condicionado')

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido ao usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Para atualizar os dados apresentados
    if text == 'Atualiza 🔄':

        # Testa comunicação com CLP
        tag = controle_clp.teste_leitura_tags_clp()

        # Mensagem para cada estado do ar-condicionado
        if tag[4] == ' (D)':
            mensagem = 'O ar-condicionado está desligado. Deseja ligar o ar-condicionado?'
        elif tag[4] == ' (L)':
            mensagem = 'O ar-condicionado está ligado. Deseja desligar o ar-condicionado?'            
        elif tag[4] == ' (-)':
            mensagem = 'O estado do ar-condicionado é desconhecido. Deseja enviar comando "ligar/desligar"?'
        else:
            mensagem = 'Falha na comunicação!'

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return DESLIGAR_AR_CONDICIONADO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Gerar o relatório ou filtrar dados
def gerar_relatorio(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para mensagem de ajuda ao usuário
    if text == 'Ajuda 🆘':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='A opção "Gerar relatório" cria um relatório de log contendo: '
                    'os usuários que controlaram algum dispositivo pelo bot, o que fizeram '
                    'e data/horário da ação.'
                    '\n\nA opção "Adicionar filtro" permite adicionar algum tipo de filtro ao relatório, como: '
                    'usuário e/ou data.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return GERAR_RELATORIO

    # Para gerar um relatório periódico
    if text == 'Relatório periódico 🔄':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio_periodico()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Você escolheu "Relatórios periódicos". O que você deseja fazer?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICO

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE

    # Para gerar um relatório
    if text == 'Gerar relatório 📋':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles()

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            "Digite o nome do relatório ou para nome padrão envie /padrao"
            "\nPara voltar envie /voltar", 
            reply_markup=ReplyKeyboardRemove(),
        )
        # Para digitar título e gerar relatório em PDF
        return TITULO_RELATORIO

    # Para adicionar filtros ao relatório
    if text == 'Adicionar filtro 🔎':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_relatorio()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha o tipo de filtro.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_RELATORIO

    # Teclado a ser exibido para o usuário
    reply_keyboard = reply_keyboard_relatorio()

    # Caso seja escolhida uma opção inválida
    context.bot.send_message(chat_id=update.message.from_user.id,
                text="Opção inválida! Tente novamente.",
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
    # Próxima pergunta
    return GERAR_RELATORIO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Adicionar filtro ao relatório
def filtro_relatorio(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para mensagem de ajuda ao usuário
    if text == 'Ajuda 🆘':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='A opção "Usuário" permite filtrar o relatório por um usuário selecionado.'
                    '\n\nA opção "Data" permite filtrar o relatório por data.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_RELATORIO

    # Para atualizar os dados apresentados
    if text == 'Atualiza 🔄':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha o tipo de filtro.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_RELATORIO

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você deseja gerar o relatório ou adicionar algum filtro?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return GERAR_RELATORIO

    # Para adicionar filtro de usuário no relatório
    if text == 'Usuário 👤':

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha um usuário para o relatório. Para voltar envie /voltar",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_USUARIO

    # Para adicionar filtro de data no relatório
    if text == 'Data 📆':

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Datas inicial e final', 'Últimos dias'], ['Ajuda 🆘', 'Voltar ⬅️'],
        ['Atualiza 🔄', 'Sair ⤴️']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você quer selecionar uma data inicial e final ou apenas informar os "
                    "últimos dias do relatório.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)

        # Próxima pergunta
        return TIPO_DATA

    # Para adicioanr filtro de ação no relatório
    if text == 'Ação 🤳':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_acao()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha a ação. Para voltar envie /voltar",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)

        # Próxima pergunta
        return FILTRO_ACAO

    # Teclado a ser exibido para o usuário
    reply_keyboard = reply_keyboard_filtro_relatorio()

    # Caso seja escolhida uma opção inválida
    context.bot.send_message(chat_id=update.message.from_user.id,
                text="Opção inválida! Tente novamente.",
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
    # Próxima pergunta
    return FILTRO_RELATORIO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Adicionar filtro de usuário ao relatório
def filtro_usuario(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para mensagem de ajuda ao usuário
    if text == 'Ajuda 🆘':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='A opção "Usuário" permite filtrar o relatório por um usuário selecionado.'
                    '\n\nA opção "Data" permite filtrar o relatório por data.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_RELATORIO

    # Para atualizar dados apresentados
    if text == 'Atualiza 🔄':

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha um usuário para o relatório.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_USUARIO

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_relatorio()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha o tipo de filtro.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_RELATORIO

    # Valida nome enviado pelo usuário
    nome_valido = False

    for i in my_sql.listarNomes():
        if i[0] == text:
            nome_valido = True

    # Caso seja um nome inválido
    if nome_valido == False:

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Usuário inválido. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_USUARIO
    # Caso o nome esteja correto
    else:
        context.user_data['usuario'] = text

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Usuário escolhido: '+text+'',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return GERAR_RELATORIO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Seleciona o tipo de filtro de data
def tipo_data(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Mensagem de ajuda ao usuário
    if text == 'Ajuda 🆘':
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Datas inicial e final', 'Últimos dias'], ['Ajuda 🆘', 'Voltar ⬅️'],
        ['Atualiza 🔄', 'Sair ⤴️']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Para escolher uma data inicial e uma data final, selecione a opção '
                    '"Datas inicial e final".'
                    '\nPara escolher um número de dias a partir de hoje, selecione a opção '
                    '"Últimos dias".',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_DATA

    # Para atualizar dados apresentados
    if text == 'Atualiza 🔄':

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Datas inicial e final', 'Últimos dias'], ['Ajuda 🆘', 'Voltar ⬅️'],
        ['Atualiza 🔄', 'Sair ⤴️']]
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você quer selecionar uma data inicial e final ou apenas informar os "
                    "últimos dias do relatório.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_DATA

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':
        
        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_relatorio()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha o tipo de filtro.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_RELATORIO

    # Filtro com intervalo de datas
    if text == 'Datas inicial e final':

        # Para a data inicial
        context.user_data['choice'] = "Controles 🙂"

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha a data inicial.",
                    reply_markup=telegramcalendar.create_calendar(),)

        # Próxima pergunta
        return GERAR_RELATORIO

    # Filtro com últimos dias
    if text == 'Últimos dias':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_dias()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha uma opção ou digite o número de dias. Para voltar envie /voltar",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)

        # Próxima pergunta
        return ULTIMOS_DIAS

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Filtro de data para ultimos dias escolhidos
def ultimos_dias(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Datas inicial e final', 'Últimos dias'], ['Ajuda 🆘', 'Voltar ⬅️'],
        ['Atualiza 🔄', 'Sair ⤴️']]
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você quer selecionar uma data inicial e final ou apenas informar os "
                    "últimos dias do relatório.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_DATA        

    # Valida opção enviada pelo usuário
    opcao_valida = False
    tipos_opcao = ['Últimos 2 dias','Últimos 3 dias','Últimos 5 dias','Últimos 7 dias',
        'Últimos 30 dias']

    for i in tipos_opcao:
        if i == text:
            opcao_valida = True

    # Verifica se foi escolhida umas das opções ou foi digitado um númermo de dias
    if opcao_valida == False and not text.isnumeric():

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_dias()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Opção inválida. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return ULTIMOS_DIAS
    else:
        # Pega só o número da string
        numero_dias = re.sub('[^0-9]', '', text)

        from datetime import date, datetime
        from datetime import timedelta

        # Seta as datas inicial e final para X dias atrás e hoje, respectivamente
        context.user_data['data_ini'] = date.strftime(date.today() - timedelta(int(numero_dias)-1),"%d/%m/%Y")
        context.user_data['data_fim'] = date.strftime(date.today(),"%d/%m/%Y")

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Opção escolhida: "Últimos '+numero_dias+' dia(s)".',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return GERAR_RELATORIO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Adicionar filtro de ação ao relatório
def filtro_acao(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha o tipo de filtro.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_RELATORIO

    # Valida ação enviada pelo usuário
    acao_valida = False
    tipos_acao = ['Abrir porta','Abrir portão','Comando para portão',
        'Ligar iluminação interna','Desligar iluminação interna','Ligar iluminação externa',
        'Desligar iluminação externa','Ligar ar-condicionado','Desligar ar-condicionado']

    for i in tipos_acao:
        if i == text:
            acao_valida = True

    # Ação enviada é inválida
    if acao_valida == False:
        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_acao()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Ação inválida. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return FILTRO_ACAO
    # Ação enviada é válida
    else:
        context.user_data['acao'] = text

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Ação escolhida: '+text+'',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return GERAR_RELATORIO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Editar título do relatório
def titulo_relatorio(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você deseja gerar o relatório ou adicionar algum filtro?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return GERAR_RELATORIO        

    # Tratamento de texto enviado pelo usuário para o título do arquivo de relatório
    value = slugify(text)

    # Caso seja escolhida a opção padrão de nome de título do arquivo de relatório
    if text == '/padrao':
        value = 'Relatorio'

    # Caso texto tratado não seja vazio
    if value !=  '':

        # Gera relatório
        relatorio = relatorio_log.relatorio_log(context.user_data['telegram_id'],context.user_data['usuario'],
            context.user_data['data_ini'],context.user_data['data_fim'],context.user_data['acao'],value)
        
        # Verifica se relatório foi gerado
        if relatorio[0] == 1:

            # Mensagem para o usuário
            mensagem = 'Relatório gerado em formato PDF. Deseja enviar o relatório para alguém?'

            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_porta()

            # Retorno da função
            retorno = ENVIAR_RELATORIO

            # Define nome do arquivo
            nome_arquivo = relatorio[1]+'_'+value+'_'+str(context.user_data['telegram_id'])+'.pdf'
            
            # Por padrão o caminho no servidor é: "C:/Users/lmrmattos/AppData/Local/Programs/Python/Python39"
            caminho_arquivo = '../../../../../../../Telegram/'+nome_arquivo

            # Tenta abrir e enviar o arquivo
            try:
                f = open(caminho_arquivo, 'rb')
                
                file_bytes = f.read()
                f.close()
                
                context.bot.sendDocument(context.user_data['telegram_id'],file_bytes,
                    relatorio[1]+'_'+value+'.pdf')
                
                context.user_data['nome_arquivo'] = nome_arquivo
                context.user_data['caminho_arquivo'] = caminho_arquivo

                print(my_sql.log_relatorio(context.user_data['telegram_id'],str(context.user_data['data_ini'])
                    +' - '+str(context.user_data['data_fim']),context.user_data['usuario'],
                    context.user_data['acao']))

            # Case não tenha conseguido abrir/enviar arquivo
            except:
                # Mensagem para o usuário
                mensagem = 'Não foi possível gerar relatório. O que você deseja controlar?'

                # Teclado a ser exibido para o usuário
                reply_keyboard = reply_keyboard_controles()

                # Retorno da função
                retorno = BASE

        # Caso relatório não tenha sido gerado
        else:
            # Mensagem para o usuário
            mensagem = 'Não foi possível gerar relatório. O que você deseja controlar?'

            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_controles()

            # Retorno da função
            retorno = BASE            

        # Seta o filtro de usuários do relatório para todos os usuários
        context.user_data['usuario'] = ''
        # Tira o filtro de datas
        context.user_data['data_ini'] = ''
        context.user_data['data_fim'] = ''
        # Tira o filtro de ações
        context.user_data['acao'] = ''

        update.message.reply_text(
            mensagem, 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        return ENVIAR_RELATORIO
    else:
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Título inválido! Tente novamente.',
                    reply_markup=ReplyKeyboardRemove(),)
        return TITULO_RELATORIO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Escolher usuário para enviar relatório
def enviar_relatorio(update: Update, context: CallbackContext) -> int:
    text = update.message.text

    # Opções fora de Sim e Atualiza fazem arquivo de relatório ser excluído
    if text != 'Sim ✅' and text != 'Atualiza 🔄':
        os.remove(context.user_data['caminho_arquivo'])

    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para enviar relatório a outro(s) usuário(s)
    if text == 'Sim ✅':

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Para qual usuário você deseja enviar o relatório?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return AGORA_ENVIA

    # Para atualizar dados apresentados
    if text == 'Atualiza 🔄':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Deseja enviar o relatório para alguém?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return ENVIAR_RELATORIO

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':
        
        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você deseja gerar o relatório ou adicionar algum filtro?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return GERAR_RELATORIO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Enviar relatório para outros usuários
def agora_envia(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        # Exclui o arquivo de relatório
        os.remove(context.user_data['caminho_arquivo'])

        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Valida nome enviado pelo usuário
    nome_valido = False

    for i in my_sql.listarNomes():
        if i[0] == text:
            nome_valido = True

    # Nome inválido
    if nome_valido == False:
        
        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Usuário inválido. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return AGORA_ENVIA        
    # Nome válido
    else:
        context.user_data['nome'] = text

        # Pega nome do remetente
        remetente = my_sql.remetente(context.user_data['telegram_id'])

        # Pega e-mail e id do telegram do usuário destinatário
        destinatario = my_sql.email(text)
        email = destinatario[0]
        id_destinatario = destinatario[1]

        # Tenta enviar relatório por e-mail
        if sendEmail.sendEmailRelatorio(remetente, text, email, 'Relatório Log Bot SARR', 
            context.user_data['nome_arquivo']):
            
            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_porta()

            # Mensagem enviada ao usuário
            mensagem = 'Relatório enviado para '+text+'. Deseja enviar para mais algum usuário?'

            # Retorno da função
            retorno = ENVIAR_RELATORIO
        else:
            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_relatorio()

            # Mensagem enviada ao usuário
            mensagem = 'Não foi possível enviar o e-mail.'

            # Retorno da função
            retorno = GERAR_RELATORIO

        # Tenta abrir e enviar o arquivo pelo Telegram
        try:
            # Abrir arquivo de relatório
            f = open(context.user_data['caminho_arquivo'], 'rb')

            file_bytes = f.read()
            f.close()

            # Arruma nome do arquivo
            nome_arquivo = context.user_data['nome_arquivo'].split('_')
            novo_nome = ''
            for i in nome_arquivo:
                if i != nome_arquivo[len(nome_arquivo)-1]:
                    if novo_nome != '':
                        novo_nome = novo_nome + '_' + i
                    else:
                        novo_nome = novo_nome + i

            novo_nome = novo_nome + '.pdf'
            print('\nnovo_nome = '+novo_nome+'\n')

            # Envia relatório via bot
            context.bot.sendDocument(id_destinatario,file_bytes,novo_nome)

            # Pega primeiro nome do usuário
            nome = text.split(' ')
            nome = nome[0]
            mensagem_destinatario = 'Olá, '+nome+'! \n'+remetente+' enviou um arquivo para você.'

            # Envia mensagem de envio de relatório via bot
            context.bot.send_message(chat_id=id_destinatario,
                        text=mensagem_destinatario,
            )

            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_porta()

            # Retorno da função
            retorno = ENVIAR_RELATORIO
        # Case não tenha conseguido abrir/enviar arquivo
        except:
            # Mensagem para o usuário
            mensagem = 'Não foi possível enviar relatório pelo bot SARR.'

            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_relatorio()

            # Retorno da função
            retorno = GERAR_RELATORIO

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return retorno

    # Exclui o arquivo de relatório
    os.remove(context.user_data['caminho_arquivo'])

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Para criar ou editar relatório periodico
def relatorio_periodico(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para a opção de relatório periódico
    if text == 'Sim ✅':

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Para qual usuário você deseja enviar o relatório?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return AGORA_ENVIA

    # Para atualizar dados apresentados
    if text == 'Atualiza 🔄':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio_periodico()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você escolheu Relatórios periódicos. O que você deseja fazer?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICO

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você deseja gerar o relatório ou adicionar algum filtro?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return GERAR_RELATORIO

    # Cria novo relatório periódico
    if text == 'Criar 🆕':

        # Vetor para guardar os remetentes do relatório periódico
        context.user_data['remetente'] = []

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_dias()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Qual a periodicidade em dias? Escolha uma opção ou digite apenas o número de dias. "
                    "Para voltar envie /voltar",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICIDADE

    # Edita um relatório criado pelo usuário
    if text == 'Editar ✏️':

        # Lista relatórios do usuário
        teclado = rp_funcoes.listaRelatorios(my_sql.remetente(context.user_data['telegram_id']))

        # Vê se o usuário tem registros
        if(teclado != False):
            # Teclado a ser exibido para o usuário
            reply_keyboard = teclado

            # Mensagem para o usuário
            mensagem = 'Escolha um relatório para editar. Para voltar envie /voltar'

            # Retorno da função
            retorno = EDITAR_RELATORIO
        else:
            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_relatorio_periodico()

            # Mensagem para o usuário
            mensagem = 'Não foi encontrado nenhum relatório. O que você quer fazer?'

            # Retorno da função
            retorno = RELATORIO_PERIODICO
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Escolha um relatório para editar.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICO

    # Para excluir um relatório criado pelo usuário
    if text == 'Excluir 🗑':

        # Lista relatórios do usuário
        teclado = rp_funcoes.listaRelatorios(my_sql.remetente(context.user_data['telegram_id']))
        
        # Vê se o usuário tem registros
        if(teclado != False):

            # Teclado a ser exibido para o usuário
            reply_keyboard = teclado

            # Mensagem para o usuário
            mensagem = 'Escolha qual relatório você quer excluir. Para voltar envie /voltar'

            # Retorno da função
            retorno = EXCLUIR_RELATORIO
        else:
            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_relatorio_periodico()

            # Mensagem para o usuário
            mensagem = 'Não foi encontrado nenhum relatório. O que você quer fazer?'

            # Retorno da função
            retorno = RELATORIO_PERIODICO
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return retorno        

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Para informar a periodicidade do relatório criado
def relatorio_periodicidade(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio_periodico()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você escolheu Relatórios periódicos. O que você deseja fazer?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICO

    # Valida opção enviada pelo usuário
    opcao_valida = False
    tipos_opcao = ['Últimos 2 dias','Últimos 3 dias','Últimos 5 dias','Últimos 7 dias',
        'Últimos 30 dias']

    for i in tipos_opcao:
        if i == text:
            opcao_valida = True

    # Opção inválida
    if opcao_valida == False and not text.isnumeric():

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_dias()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Opção inválida. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICIDADE
    # Opção válida
    else:
        # Pega só o número da string (de quanto em quanto tempo o relatório é enviado)
        numero_dias = re.sub('[^0-9]', '', text)

        # Guarda o valor de periodicidade definido para o relatório
        context.user_data['periodicidade'] = numero_dias

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['06:00'],['08:00'],['10:00'],['12:00'],['13:00'],
        ['15:00'],['17:00'],['18:00'],['20:00'],['22:00']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Você escolheu gerar o relatório de '+numero_dias+' em '+numero_dias+' dia(s).'
                    '\nQual o horário para enviar o relatório? Formato: hh:mm.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_HORARIO      

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Para informar o horário do relatório criado
def relatorio_horario(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_dias()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Qual a periodicidade em dias? Escolha uma opção ou digite apenas o número de dias. "
                    "Para voltar envie /voltar",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICIDADE

    # Validar formato de hora
    regex = "^([01]?[0-9]|2[0-3]):[0-5][0-9]$";
    # Compile the ReGex
    p = re.compile(regex);
         
    # Pattern class contains matcher() method to find matching between given time and regular expression.
    m = re.search(p, text);

    # Verifica se está vazio ou se o padrão de hora veio corretamente
    if m is None :
        # Mensagem para o usuário
        mensagem = 'Horário inválido.Tente novamente.'
        
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['06:00'],['08:00'],['10:00'],['12:00'],['13:00'],
        ['15:00'],['17:00'],['18:00'],['20:00'],['22:00']]

        retorno = RELATORIO_HORARIO
    # Padrão de horário correto
    else:
        # Mensagem para o usuário
        mensagem = 'Você escolheu gerar o relatório às '+text+'.\nDeseja enviar o relatório para quem?'
        
        # Guarda o horário escolhido para envio do relatório periódico
        context.user_data['horario'] = text

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        retorno = RELATORIO_DESTINATARIO

    # Questionamento a ser feito para o usuário
    context.bot.send_message(chat_id=update.message.from_user.id,
                text=mensagem,
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
    return retorno      

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Para informar os destinatários do relatório criado
def relatorio_destinatario(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['06:00'],['08:00'],['10:00'],['12:00'],['13:00'],
        ['15:00'],['17:00'],['18:00'],['20:00'],['22:00']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='\nQual o horário para enviar o relatório? Formato: hh:mm.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_HORARIO

    # Valida nome do destinatario
    nome_valido = False

    for i in my_sql.listarNomes():
        if i[0] == text:
            nome_valido = True

    # Nome inválido
    if nome_valido == False:

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Usuário inválido. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_DESTINATARIO
    # Nome válido
    else:
        # Guarda o usuário do remtente do relatório
        context.user_data['remetente'].append(text)
        print(str(context.user_data['remetente']))

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Outro destinatário 👤', 'Filtros 🔎'], ['Ajuda 🆘', 'Voltar ⬅️'], 
        ['Atualiza 🔄', 'Sair ⤴️']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Você escolheu enviar para '+text+'.'
                    '\nDeseja enviar para mais alguém ou deseja escolher algum filtro?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICO_FILTROS

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Define se manda para outro usuário ou adiciona filtros ao relatório criado
def relatorio_periodico_filtros(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Deseja enviar o relatório para quem?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_DESTINATARIO

    # Opção para adicioanr outro destinatário
    if text == 'Outro destinatário 👤':

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Deseja enviar o relatório para quem?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_DESTINATARIO

    # Opção para escolher os filtros
    if text == 'Filtros 🔎':

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Escolha um usuário para adicionar filtro de usuário. Para pular envie /pular',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_FILTRO_USUARIO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Para informar o filtro de usuário do relatório criado
def relatorio_filtro_usuario(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Outro destinatário 👤', 'Filtros 🔎'], ['Ajuda 🆘', 'Voltar ⬅️'], 
        ['Atualiza 🔄', 'Sair ⤴️']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Deseja enviar para mais alguém ou deseja escolher algum filtro?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICO_FILTROS

    # Pula filtro
    if text == '/pular':

        # Guarda informação de filtro de usuário vazia
        context.user_data['filtro_usuario'] = ''

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_acao()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Escolha uma ação para adicionar filtro de ação ao relatório. Para pular envie /pular',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_FILTRO_ACAO

    # Valida nome enviado pelo usuário
    nome_valido = False

    for i in my_sql.listarNomes():
        if i[0] == text:
            nome_valido = True

    # Nome inválido
    if nome_valido == False:

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Usuário inválido. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_FILTRO_USUARIO
    # Nome válido
    else:
        # Guarda informação do filtro de usuário
        context.user_data['filtro_usuario'] = text

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_acao()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Você escolheu filtro com o usuário '+text+'.'
                    '\nEscolha o tipo de ação para adicionar filtro de Operação. Para pular envie /pular',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_FILTRO_ACAO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Para informar o filtro de ação do relatório criado
def relatorio_filtro_acao(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Escolha um usuário para adicionar filtro de usuário. Para pular envie /pular',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_FILTRO_USUARIO

    # Pula filtro
    if text == '/pular':

        # Guarda informação do filtro de ação vazia
        context.user_data['filtro_acao'] = ''

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Deseja criar um relatório periódico?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return CRIAR_RELATORIO_PERIODICO

    # Valida ação escolhida
    acao_valida = False
    tipos_acao = ['Abrir porta','Abrir portão','Comando para portão',
        'Ligar iluminação interna','Desligar iluminação interna','Ligar iluminação externa',
        'Desligar iluminação externa','Ligar ar-condicionado','Desligar ar-condicionado']

    for i in tipos_acao:
        if i == text:
            acao_valida = True

    # Ação inválida
    if acao_valida == False:

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_filtro_acao()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Ação inválida. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_FILTRO_ACAO
    # Ação válida
    else:
        # Guarda informação do filtro de ação
        context.user_data['filtro_acao'] = text

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Ação escolhida: "'+text+'". Deseja criar um relatório periódico?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return CRIAR_RELATORIO_PERIODICO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Cria um relatório periódico com as informações escolhidas pelo usuário
def criar_relatorio_periodico(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Outro destinatário 👤', 'Filtros 🔎'], ['Ajuda 🆘', 'Voltar ⬅️'], 
        ['Atualiza 🔄', 'Sair ⤴️']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Escolha o tipo de ação para adicionar filtro de ação. Para pular envie /pular',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_FILTRO_ACAO

    # Sim para criar relatório periódico
    if text == 'Sim ✅':
        
        # Insere um relatório periódico no banco com as informações passadas pelo usuário
        if rp_funcoes.insereRelatorioPeriodico(my_sql.remetente(context.user_data['telegram_id']),
            context.user_data['periodicidade'],context.user_data['horario'],
            str(context.user_data['remetente']),context.user_data['filtro_usuario'],
            context.user_data['filtro_acao'],'ativo'):
            
            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_relatorio_periodico()

            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.message.from_user.id,
                        text='Relatório periódico criado. O que você deseja fazer?',
                        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            return RELATORIO_PERIODICO            
        # Se houve erro na inserção de relatório periódico
        else:
            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_porta()

            # Questionamento a ser feito para o usuário
            context.bot.send_message(chat_id=update.message.from_user.id,
                        text='Houve um problema para criar o relatório. Tente novamente.'
                        'Deseja criar um relatório periódico?',
                        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            return CRIAR_RELATORIO_PERIODICO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Para editar relatório periodico do banco
def editar_relatorio(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio_periodico()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você escolheu Relatórios periódicos. O que você deseja fazer?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICO

    # Lista relatórios do usuário
    relatorio = rp_funcoes.listaRelatorios(my_sql.remetente(context.user_data['telegram_id']))

    # Valida relatório
    relatorio_valido = False

    for i in relatorio:
        if str(i) == str("['"+text+"']"):
            id_relatorio = str(i)
            id_relatorio = id_relatorio.split(' ')
            context.user_data['id_relatorio'] = id_relatorio[1]
            print(context.user_data['id_relatorio'])
            relatorio_valido = True

    # Verifica se foi escolhido um dos relatórios da lista
    if relatorio_valido == False:

        # Teclado a ser exibido para o usuário
        reply_keyboard = rp_funcoes.listaRelatorios(my_sql.remetente(context.user_data['telegram_id']))

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Relatório inválido. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return EDITAR_RELATORIO 
    else:
        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Relatório de selecionado: '+text+
                    '\nO que você deseja editar?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return AGORA_EXCLUI_RELATORIO        

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Para excluir relatório periodico do banco
def excluir_relatorio(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio_periodico()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você escolheu Relatórios periódicos. O que você deseja fazer?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICO

    # Lista relatórios do usuário
    relatorio = rp_funcoes.listaRelatorios(my_sql.remetente(context.user_data['telegram_id']))

    # Valida relatório enviado pelo usuário
    relatorio_valido = False

    for i in relatorio:
        if str(i) == str("['"+text+"']"):
            id_relatorio = str(i)
            id_relatorio = id_relatorio.split(' ')
            context.user_data['id_relatorio'] = id_relatorio[1]
            print(context.user_data['id_relatorio'])
            relatorio_valido = True

    # Verifica se foi escolhido um dos relatórios da lista
    if relatorio_valido == False:

        # Teclado a ser exibido para o usuário
        reply_keyboard = rp_funcoes.listaRelatorios(my_sql.remetente(context.user_data['telegram_id']))

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Relatório inválido. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return EXCLUIR_RELATORIO 
    else:
        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_porta()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Tem certeza que deseja excluir o relatório selecionado?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return AGORA_EXCLUI_RELATORIO        

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Para excluir relatório periodico
def agora_exclui_relatorio(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Sim para excluir relatório
    if text == 'Sim ✅':

        # Exclui relatório periódico (muda status)
        if rp_funcoes.excluirRelatorioPeriodico(context.user_data['id_relatorio']):

            # Mensagem para o usuário
            mensagem = 'Relatório periódico excluído. O que você deseja fazer?'

            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_relatorio_periodico()

            # Retorno da função
            retorno = RELATORIO_PERIODICO
        else:
            # Mensagem para o usuário
            mensagem = 'Não foi possível exluir o relatório periódico selecionado. Tente novamente.'
            
            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_relatorio_periodico()

            # Retorno da função
            retorno = RELATORIO_PERIODICO

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return retorno

    # Para atualizar dados apresentados
    if text == 'Atualiza 🔄':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_relatorio_periodico()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Você escolheu Relatórios periódicos. O que você deseja fazer?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return RELATORIO_PERIODICO

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':
        
        # Lista relatórios do usuário
        teclado = rp_funcoes.listaRelatorios(my_sql.remetente(context.user_data['telegram_id']))

        # Vê se o usuário tem registros
        if(teclado != False):
            # Teclado a ser exibido para o usuário
            reply_keyboard = teclado

            # Mensagem para o usuário
            mensagem = 'Escolha qual relatório você quer excluir. Para voltar envie /voltar'

            # Retorno da função
            retorno = EXCLUIR_RELATORIO
        # Não foram encontrados registros
        else:
            # Teclado a ser exibido para o usuário
            reply_keyboard = reply_keyboard_relatorio_periodico()

            # Mensagem para o usuário
            mensagem = 'Não foi encontrado nenhum relatório. O que você quer fazer?'

            # Retorno da função
            retorno = RELATORIO_PERIODICO
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text=mensagem,
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return retorno   

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Mostrar lifecounter
def life_counter(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Mensagem de ajuda ao usuário
    if text == 'Ajuda 🆘':

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Ajuda 🆘', 'Voltar ⬅️'], ['Atualiza 🔄', 'Sair ⤴️']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O lifecounter permite monitorar a comunicação entre o SARR e o CLP."
                    "\n\nA tag de leitura é apenas lida pelo SARR."
                    "\nA tag de escrita é incrementada pelo SARR a cada ciclo.",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return LIFE_COUNTER

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':
        
        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_controles()
       
        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="O que você deseja controlar?",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return BASE                

    # Para atualizar dados apresentados
    if text == 'Atualiza 🔄':

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Ajuda 🆘', 'Voltar ⬅️'], ['Atualiza 🔄', 'Sair ⤴️']]

        # Leitura das tags do lifecounter
        leitura = controle_clp.life_counter()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Tag de leitura = "+str(leitura[0])+
                    "\nTag de escrita = "+str(leitura[0]),
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return LIFE_COUNTER

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Controle de acesso dos usuários
def acesso_usuarios(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_principal(context.user_data['telegram_id'])

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='O que você deseja fazer?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_REGISTRO    

    # Valida nome enviado pelo usuário
    nome_valido = False

    for i in my_sql.listarNomes():
        if i[0] == text:
            nome_valido = True

    # Nome inválido
    if nome_valido == False:

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Usuário inválido. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return ACESSO_USUARIOS        
    # Nome válido
    else:
        context.user_data['nome'] = text

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Padrão'], ['Administrador'], ['Programador']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Qual a permissão para o colaborador '+text+'?'
                    '\nPara voltar, envie /voltar',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return ACESSO_PERMISSAO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Controle de acesso dos usuários
def acesso_permissao(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = my_sql.listarNomes()

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Para qual usuário você deseja colocar uma permissão?"
                    "\nPara voltar, envie /voltar",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return ACESSO_USUARIOS

    # Tipo de usuário escolhido
    if text == 'Padrão' or text == 'Administrador' or text == 'Programador':
        context.user_data['permissao'] = text

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Sim ✅'],['Voltar ⬅️'],['Sair ⤴️']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Tem certeza que deseja dar a permissão: '+text+'?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return CONFIRMAR_PERMISSAO
    # Nenhum usuário escolhido
    else:
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Padrão'], ['Administrador'], ['Programador']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Opção inválida. Tente novamente.',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return ACESSO_PERMISSAO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Controle de acesso dos usuários
def confirmar_permissao(update: Update, context: CallbackContext) -> int:
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar' or text == 'Sair ⤴️':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Para voltar ao passo anterior
    if text == 'Voltar ⬅️' or text == '/voltar':

        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Padrão'], ['Administrador'], ['Programador']]

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text="Qual permissão você deseja dar?"
                    "\nPara voltar, envie /voltar",
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return ACESSO_PERMISSAO

    # Sim para confirmar a permissão dada ao usuário
    if text == 'Sim ✅':

        # Dá permissão de acesso ao usuário
        my_sql.permissao(context.user_data['nome'],context.user_data['permissao'])

        # Teclado a ser exibido para o usuário
        reply_keyboard = reply_keyboard_principal(context.user_data['telegram_id'])

        # Questionamento a ser feito para o usuário
        context.bot.send_message(chat_id=update.message.from_user.id,
                    text='Permissão concedida. O que mais você deseja fazer?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
        return TIPO_REGISTRO

    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        mensagem_saida , reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# Apagar um registro lançado pelo usuário
def apagar_registro(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    if not(text == 'Quilometragem' or text == 'Nota Fiscal' or text == 'Abastecimento'):
        reply_keyboard = [['Quilometragem', 'Nota Fiscal', 'Abastecimento']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Tipo de registro inválido! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return APAGAR_REGISTRO

    # Informação gravada no session do usuário
    context.user_data['tipo_apagar'] = text

    user = update.message.from_user
    telegram_id = user.id

    # Apagar deslocamento
    if context.user_data['tipo_apagar'] == 'Quilometragem':
        reply_keyboard = my_sql.listarDeslocamentosAbertos(telegram_id)
    # Apagar nota fiscal
    if context.user_data['tipo_apagar'] == 'Nota Fiscal':
        reply_keyboard = my_sql.listarDespesasAbertas(telegram_id)
    # Apagar abastecimento
    if context.user_data['tipo_apagar'] == 'Abastecimento':
        reply_keyboard = my_sql.listarAbastecimentosAbertos(telegram_id)

    # Sem registros para cancelar
    if not(reply_keyboard):
        update.message.reply_text(
        'Não há registros para cancelar! \nCaso tenha detectado um erro em algum lançamento que não está mais disponível para ser apagado, favor entrar em contato com o setor administrativo.\n'
        'Até a próxima vez !\n\n'
        'Para enviar um novo registro favor apertar /start'
        )

        return ConversationHandler.END

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Selecione qual registro você deseja apagar:',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    # Próxima pergunta
    return CONFIRMACAO_APAGAR

# Qual pergunta será feita para o usuário
def confirmacao_apagar(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Apagar deslocamento
    if context.user_data['tipo_apagar'] == 'Quilometragem':
        tipo_para_apagar = "deslocamentos"
    # Apagar nota fiscal
    if context.user_data['tipo_apagar'] == 'Nota Fiscal':
        tipo_para_apagar = "notinhas"
    # Apagar abastecimento
    if context.user_data['tipo_apagar'] == 'Abastecimento':
        tipo_para_apagar = "abastecimento"

    user = update.message.from_user
    telegram_id = user.id

    # Apagar deslocamento
    if context.user_data['tipo_apagar'] == 'Quilometragem':
        reply_keyboard = my_sql.listarDeslocamentosAbertos(telegram_id)
    # Apagar nota fiscal
    if context.user_data['tipo_apagar'] == 'Nota Fiscal':
        reply_keyboard = my_sql.listarDespesasAbertas(telegram_id)
    # Apagar abastecimento
    if context.user_data['tipo_apagar'] == 'Abastecimento':
        reply_keyboard = my_sql.listarAbastecimentosAbertos(telegram_id)

    if text.find("|") != -1:
        id_apagar = text.split("|")    
        text_id = re.sub("[^0-9]", "", id_apagar[0])
        context.user_data['registro_apagar'] = re.sub("[^0-9]", "", id_apagar[0])
    else:
        text_id = re.sub("[^0-9]", "", text)
        context.user_data['registro_apagar'] = text_id

    # Valida se é número
    if not text_id.isnumeric():
        update.message.reply_text(
            "Precisa ser um número! Tente novamente" , 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return CONFIRMACAO_APAGAR

    confere_se_pode_apagar = my_sql.validaApagarRegistro(tipo_para_apagar,telegram_id,context.user_data['registro_apagar'])

    if not(confere_se_pode_apagar):
        update.message.reply_text(
            "Você não pode apagar esse ID! Tente novamente." , 
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        return CONFIRMACAO_APAGAR

    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Sim', 'Não']]

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Tem certeza que quer apagar o registro abaixo? \n\n' + text,
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    # Próxima pergunta
    return AGORA_APAGOU

# Confirma se vai apagar o registro
def agora_apagou(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)

        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Valida resposta que precisa ser sim ou não
    if not(text == 'Sim' or text == 'Não'):
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Sim', 'Não']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Opção inválida! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return AGORA_APAGOU

    # Apagar deslocamento
    if context.user_data['tipo_apagar'] == 'Quilometragem':
        tipo_para_apagar = "deslocamentos"
    # Apagar nota fiscal
    if context.user_data['tipo_apagar'] == 'Nota Fiscal':
        tipo_para_apagar = "notinhas"
    # Apagar abastecimento
    if context.user_data['tipo_apagar'] == 'Abastecimento':
        tipo_para_apagar = "abastecimento"

    # Sim para apagar registro
    if text == 'Sim':   
        my_sql.apagarRegistro(tipo_para_apagar, context.user_data['registro_apagar'])

    # Não para não apagar registro
    if text == 'Não':
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
        'Operação cancelada !\n'
        'Até a próxima vez !\n\n'
        'Para enviar um novo registro favor apertar /start'
        )
        return ConversationHandler.END

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Registro apagado com sucesso !\n'
        'Até a próxima vez !\n\n'
        'Para enviar um novo registro favor apertar /start'
    )
    return ConversationHandler.END

# Pega o centro de custos do registro
def centro_custos(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Utiliza dados do último registro
    if text == '/repetir':
        user = update.message.from_user
        context.user_data['repetir']=1
        context.user_data['cc']= context.user_data['lastReport'][0][5]
        context.user_data['modalidade']= context.user_data['lastReport'][0][6]
        context.user_data['cidade']=context.user_data['lastReport'][0][7]
        context.user_data['solicitante']=context.user_data['lastReport'][0][16]
        context.user_data['pernoite']=context.user_data['lastReport'][0][17]
        context.user_data['data'] = context.user_data['lastReport'][0][2]
    
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o horário de entrada? (utilizar formato hh:mm)\n\nPara considerar padrão, envie:\n/manha ou\n\n/tarde',
            reply_markup=ReplyKeyboardRemove())
        # Próxima pergunta
        return ENTRADA
    else:
        context.user_data['repetir']=0

    # Pega o centro de custo
    if text.find("|") != -1:
        text = text.split("|")    
        # Informação gravada no session do usuário
        context.user_data['cc'] = re.sub("[^0-9]", "", text[0])
    else:
        context.user_data['cc'] = re.sub("[^0-9]", "", text)

    # Valida se é número
    if context.user_data['cc'].isnumeric():
        cc_existe = get_info.checkCC(context.user_data['cc'])
    else:
        reply_keyboard = get_info.ccList() #[['251-GM-Conversão CPU', '245-LM-Sistema WEB', '248-LM-SARR']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Este centro de custos não existe! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return CC

    if cc_existe:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual é a data do registro?',
            reply_markup=telegramcalendar.create_calendar(),
            #reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return DATA_REG
    else:
        # Teclado a ser exibido para o usuário
        reply_keyboard = get_info.ccList()

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Este centro de custos não existe! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return CC

# Qual pergunta será feita para o usuário
def dia_reg (update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text
    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    try:
        date = context.user_data['data']
    except KeyError:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
                'O formato da data está errado! Tente novamente.',
                reply_markup=telegramcalendar.create_calendar(),
            )
        # Próxima pergunta
        return DATA_REG

    # Opção escolhida: "Quilometragem"
    if context.user_data['choice'] == "Quilometragem 📋":
        # Teclado a ser exibido para o usuário
        reply_keyboard = get_info.carList()
        # Resposta recebida do usuário
        text = update.message.text

        # Finaliza a conversa
        if text == '/cancelar':
            user = update.message.from_user
            logger.info("Usuário %s cancelou a conversa.", user.first_name)
            update.message.reply_text(
                mensagem_saida , reply_markup=ReplyKeyboardRemove()
            )
            return ConversationHandler.END

        # Informação gravada no session do usuário
        context.user_data['distancia_percorrida'] = text

        # Valida se é número
        if text.isnumeric():
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual o veículo utilizado?',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )
            # Próxima pergunta
            return VEICULO
        else:
            update.message.reply_text(
                'Deve ser um número! Tente novamente.',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return QUILOMETRAGEM

    # Opção escolhida: "Nota Fiscal" ou "Pedágio"
    if context.user_data['choice'] == 'Nota Fiscal 📋' or context.user_data['choice'] == 'Pedágio 📋':
        # Resposta recebida do usuário
        text = update.message.text

        # Finaliza a conversa
        if text == '/cancelar':
            user = update.message.from_user
            logger.info("Usuário %s cancelou a conversa.", user.first_name)
            update.message.reply_text(
                mensagem_saida , reply_markup=ReplyKeyboardRemove()
            )
            return ConversationHandler.END

        # Pega CNPJ
        if text.find("|") != -1:
            text = text.split("|")    
            # Informação gravada no session do usuário
            context.user_data['cnpj'] = re.sub("[^0-9]", "", text[1])
        else:
            context.user_data['cnpj'] = re.sub("[^0-9]", "", text)

        if not (cnpj_verify.validate(context.user_data['cnpj'])or cpf_verify.validate(context.user_data['cnpj'])):
            # Teclado a ser exibido para o usuário
            reply_keyboard = get_info.cnpjList(context.user_data['cc']) #[['LM-Logix-04.580.206/0001-99', 'Braun-04.580.206/0001-99', 'Darolt-04.580.206/0001-99', 'Delupo-04.580.206/0001-99']]
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'CNPJ inválido! Tente novamente.',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )
            # Próxima pergunta
            return CNPJ

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o valor gasto?',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return VALOR

    # Opção escolhida: "Abastecimento"
    if context.user_data['choice'] == 'Abastecimento 📋':
        # Guarda o km abastecimento na variável global
        context.user_data['quilometro_abastecido'] = text

        # Valida valor de km
        if re.search("^[0-9]{1,}([,.][0-9]{1,3})?$", text):
            if text.find(",") != -1:
                context.user_data['abastecimento'] = text.replace(",",".") 

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Quantos litros foram abastecidos?',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return ABASTECIMENTO
        # Valor de km inválido
        else:
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'O valor inserido tem de ser um número! Tente novamente.',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return QUILOMETRO_ABASTECIDO

    # Opção escolhida: "Ponto" (lançamento de horas)
    if context.user_data['choice'] == 'Ponto 📋':
        context.user_data['modalidade'] = text
        context.user_data['veiculo'] = None
        context.user_data['motorista'] = None
        context.user_data['ini'] = None
        context.user_data['fim'] = None

        for i in context.user_data['modalidades']:
            if text in i:
                # Questionamento a ser feito para o usuário
                reply_keyboard = get_info.cidades()
                update.message.reply_text(
                    'Qual a cidade?',
                    reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
                )
                # Próxima pergunta
                return LOCAL
        
        # Teclado a ser exibido para o usuário
        reply_keyboard =  context.user_data['modalidades']

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Opção inválida! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return MODALIDADE

# Qual pergunta será feita para o usuário
def tipo_deslocamento(update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['tipo_deslocamento'] = text

    # aua
    if text == 'Inicial':
        reply_keyboard = get_info.ccList()
        update.message.reply_text(
        'Qual é o centro de custos do registro?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

        # Próxima pergunta
        return CC

    elif text == 'Final':
        # Questionamento a ser feito para o usuário
        reply_keyboard = my_sql.listaDeslocamentosIncompletos(context.user_data['telegram_id'])

        if reply_keyboard:
            update.message.reply_text(
            'Qual registro você está completando?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),)
            return SELECT_REGISTRO
        else:
            user = update.message.from_user
            logger.info("Usuário %s cancelou a conversa.", user.first_name)
            update.message.reply_text(
                'Você não possui deslocamentos em aberto!\n\nPara enviar um novo registro favor apertar /start' , reply_markup=ReplyKeyboardRemove()
            )
            return ConversationHandler.END

# Escolher registro para completar deslocamento
def select_registro(update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Pega ID do deslocamento
    if text.find("|") != -1:
        id_apagar = text.split("|")    
        text_id = re.sub("[^0-9]", "", id_apagar[0])
        context.user_data['id_deslocamento'] = re.sub("[^0-9]", "", id_apagar[0])

        # Confere se o deslocamento existe pela ID e pelo usuário
        context.user_data['distancia_inicial'] = my_sql.confereExisteDeslocamento(
            context.user_data['id_deslocamento'],context.user_data['telegram_id'])

        # Deslocamento existe: pede km do hodômetro
        if context.user_data['distancia_inicial']:
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual a quilometragem marcada no hodômetro?',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return QUILOMETRAGEM

    else:
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Inicial', 'Final']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Opção inválida! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return TIPO_QM

# Qual pergunta será feita para o usuário
def nota_fiscal(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.cnpjList(context.user_data['cc']) #[['LM-Logix-04.580.206/0001-99', 'Braun-04.580.206/0001-99', 'Darolt-04.580.206/0001-99', 'Delupo-04.580.206/0001-99']]

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['nota_fiscal'] = text

    # Valida se é número
    if text.isnumeric():
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o CNPJ da nota?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return CNPJ   
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'O valor inserido tem de ser um número! Tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return NF

# Qual pergunta será feita para o usuário
def quilometro_abastecido(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['quilometro_abastecido'] = text

    # Valida abastecimento
    if re.search("^[0-9]{1,}([,.][0-9]{1,3})?$", text):
        if text.find(",") != -1:
            context.user_data['abastecimento'] = text.replace(",",".") 

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Quantos litros foram abastecidos?',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return ABASTECIMENTO
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'O valor inserido tem de ser um número! Tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return QUILOMETRO_ABASTECIDO

# Qual pergunta será feita para o usuário
def abastecimento(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.carList() #[['AAA-2359 -> Corsa Sedan', 'AAA-2359 -> Gol', 'AAA-2359 -> Montana']]
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['abastecimento'] = text

    # Valida abastecimento
    if re.search("^[0-9]{1,}([,.][0-9]{1,3})?$", text):
        if text.find(",") != -1:
            context.user_data['abastecimento'] = text.replace(",",".") 

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o veículo utilizado?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return VEICULO
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'O valor inserido tem de ser um número! Tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return ABASTECIMENTO

# Qual pergunta será feita para o usuário
def veiculo(update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Retira hífen de veículo
    if text.find("-") != -1:
        text = text.replace("-","")  

    # Pega a placa
    placa = text.split("|")
    context.user_data['veiculo'] = placa[0].strip()

    # Valida placa
    if re.search("[a-zA-Z]{3}[0-9][0-9A-Z][0-9]{2}", text):
        # Opção escolhida "Quilometragem"
        if context.user_data['choice'] == 'Quilometragem 📋':
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual a descrição do deslocamento?',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return DESCRICAO
    # Placa não encontrada
    else:
        # Teclado a ser exibido para o usuário
        reply_keyboard = get_info.carList()

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Placa não encontrada! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return VEICULO    

    # Opção escolhida: "Abastecimento"
    if context.user_data['choice'] == 'Abastecimento 📋':
        # Teclado a ser exibido para o usuário
        reply_keyboard = get_info.cnpjPOSTOS()

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o CNPJ do posto?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return CNPJ

# Qual pergunta será feita para o usuário
def cnpj(update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Pega o CNPJ
    if text.find("|") != -1:
        text = text.split("|")    
        # Informação gravada no session do usuário
        context.user_data['cnpj'] = re.sub("[^0-9]", "", text[1])
    else:
        context.user_data['cnpj'] = re.sub("[^0-9]", "", text)

    if not (cnpj_verify.validate(context.user_data['cnpj'])or cpf_verify.validate(context.user_data['cnpj'])):
        # Teclado a ser exibido para o usuário
        reply_keyboard = get_info.cnpjList(context.user_data['cc'])

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'CNPJ inválido! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return CNPJ

    # Questionamento a ser feito para o usuário
    update.message.reply_text(
        'Qual o valor gasto?',
        reply_markup=ReplyKeyboardRemove(),
    )
    # Próxima pergunta
    return VALOR

# Qual pergunta será feita para o usuário
def valor(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = [['Cartão da empresa', 'Caixa'],['Faturado']]
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['valor_gasto'] = text

    # Valida o valor gasto
    if re.search("^[0-9]{1,}([,.][0-9]{1,3})?$", text):
        # Troca vírgula por ponto
        if text.find(",") != -1:
            context.user_data['valor_gasto'] = text.replace(",",".") 
        
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual o cartão utilizado?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return CARTAO
    else:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Deve ser um número! Tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return VALOR

# Qual pergunta será feita para o usuário
def deslocamento(update: Update, context: CallbackContext) -> int:
    # Teclado a ser exibido para o usuário
    reply_keyboard = get_info.carList() #[['AAA-2359 -> Corsa Sedan', 'AAA-2359 -> Gol', 'AAA-2359 -> Montana']]
    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['distancia_percorrida'] = text

    # Valida se é número
    if text.isnumeric():
        # Tipo de deslocamento final
        if context.user_data['tipo_deslocamento']=='Final':
            # km final menor do que km inicial
            if context.user_data['distancia_inicial']>int(context.user_data['distancia_percorrida']):
                update.message.reply_text(
                    'A quilometragem final não pode ser menor que a inicial! Informe a quilometragem final:',
                    reply_markup=ReplyKeyboardRemove(),
                )
                # Próxima pergunta
                return QUILOMETRAGEM
            # km final maior do que km inicial (está correto)
            else:
                update.message.reply_text(
                    "Caso queira adicionar alguma observação à descrição inicial, digite aqui."
                    "\n\nCaso queira mantê-la inalterada, envie /pular.",
                    reply_markup=ReplyKeyboardRemove(),
                )
                # Próxima pergunta
                return INCREMENTAR_DESCRICAO
        # Não é km final
        else:
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual o veículo utilizado?',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )
            # Próxima pergunta
            return VEICULO
    # Não é número
    else:
        update.message.reply_text(
            'Deve ser um número! Tente novamente.',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return QUILOMETRAGEM

# Para incrementar descrição de deslocamento no km final
def incrementar_descricao(update: Update, context: CallbackContext) -> int:
    # Resposta recebida do usuário
    text = update.message.text
    context.user_data['descricao'] = my_sql.getDescricaoDeslocamento(context.user_data['id_deslocamento'])

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Pular atualização da descrição do deslocamento
    if text == '/pular':
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Favor enviar uma foto do registro',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return FOTOGRAFIA
    else:
        # Atualiza a descrição do deslocamento
        context.user_data['descricao'] = context.user_data['descricao']+' | OBS final: '+text
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Favor enviar uma foto do registro',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return FOTOGRAFIA

# Qual pergunta será feita para o usuário
def cartao (update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['cartao'] = text

    # Opção escolhida: "Cartão da empresa"
    if text == 'Cartão da empresa':
        # Pega os cartões do usuário
        cartoes = get_info.cartao(context.user_data['telegram_id'])

        # Não há cartão para o usuário cadastrado no banco
        if cartoes is None:
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Quais os 4 últimos dígitos do cartão?',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return DIGITOS_CARTAO
        # Há algum cartão do usuário cadastrado no banco
        else:
            cartoes = json.loads(cartoes)

            # Corrige erro de cartões com digito "0"
            for index,i in enumerate(cartoes):
                cartoes[index][0] = str(cartoes[index][0]).replace("12345","")            
            
            # Teclado a ser exibido para o usuário
            reply_keyboard = cartoes       

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Confirme os últimos 4 dígitos do cartão',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )
            # Próxima pergunta
            return SELECT_CARTAO
    # Não foi utilizado cartão da empresa
    else:
        # Para "Caixa" ou "Faturado"
        if text == 'Caixa' or text == 'Faturado':
            # Opção escolhida: "Abastecimento"
            if context.user_data['choice'] != 'Abastecimento':
                # Questionamento a ser feito para o usuário
                update.message.reply_text(
                    'Qual a descrição?',
                    reply_markup=ReplyKeyboardRemove(),
                )
                # Próxima pergunta
                return DESCRICAO
            else:
                # Questionamento a ser feito para o usuário
                update.message.reply_text(
                    'Favor enviar foto',
                    reply_markup=ReplyKeyboardRemove(),
                )
                # Próxima pergunta
                return FOTOGRAFIA
        else:
            # Teclado a ser exibido para o usuário
            reply_keyboard = [['Cartão da empresa', 'Caixa'],['Faturado']]

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Opção inválida! Tente novamente.',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
            )
            # Próxima pergunta
            return CARTAO

# Seleciona o cartão utilizado para pagamento
def select_cartao (update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['cartao'] = text
    cartoes = json.loads(get_info.cartao(context.user_data['telegram_id']))
    cartao_select = 0

    for index,i in enumerate(cartoes):
        cartoes[index] = str(i).replace("12345","")

    logger.info("text = %s.", text)
    logger.info("cartoes[0] = %s.", cartoes)

    for i in cartoes:
        i = re.sub('[^0-9]', '', str(i))
        logger.info("i = %s.", i)   
        if text == i:
            cartao_select = i
    
    # Foi enviado o cartão correto
    if text == cartao_select:
        # Não é um abastecimento
        if context.user_data['choice'] != 'Abastecimento':
            context.user_data['cartao'] = cartao_select

            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Qual a descrição?',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return DESCRICAO
        else:
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
                'Favor enviar foto',
                reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return FOTOGRAFIA       
    else:
        cartoes = get_info.cartao(context.user_data['telegram_id'])        

        cartoes = json.loads(cartoes)
        for index,i in enumerate(cartoes):
            cartoes[index][0] = str(cartoes[index][0]).replace("12345","")            
        
        # Teclado a ser exibido para o usuário
        reply_keyboard = cartoes

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Opção inválida! Tente novamente. ',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return SELECT_CARTAO

# Qual pergunta será feita para o usuário
def digitos_cartao (update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['cartao'] = text

    text = re.sub('[^0-9]', '', text)
    logger.info("Card: %s.", text)

    if len(text) == 4:
        context.user_data['cartao'] = text

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Qual a descrição?',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return DESCRICAO
    else:
        # Teclado a ser exibido para o usuário
        reply_keyboard = [['Cartão da empresa', 'Caixa'],['Faturado']]

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Opção inválida! Tente novamente.',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )
        # Próxima pergunta
        return DIGITOS_CARTAO

# Qual pergunta será feita para o usuário
def descricao (update: Update, context: CallbackContext) -> int:

    # Resposta recebida do usuário
    text = update.message.text

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    # Informação gravada no session do usuário
    context.user_data['descricao'] = text

    # Opção escolhida: "Pedágio"
    if context.user_data['choice']=='Pedágio 📋':
        context.user_data['photoIndex']=1

        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Favor enviar foto 1/'+str(context.user_data['numPedagios']),
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return FOTOGRAFIA

    if text:
        # Questionamento a ser feito para o usuário
        update.message.reply_text(
            'Favor enviar uma foto do registro',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Próxima pergunta
        return FOTOGRAFIA
    else:
        update.message.reply_text(
            'Favor digitar a descrição',
            reply_markup=ReplyKeyboardRemove(),
        )
        # Volta para a descrição
        return DESCRIÇÃO 

# Tira a foto
def imagem(update: Update, context: CallbackContext) -> int:

    user = update.message.from_user
    now = datetime.datetime.now() # current date and time

    # Resposta recebida do usuário
    text = update.message.text
    message_id = update.message.message_id
    user_id = user.id
    date_time = now.strftime("%m-%d-%Y_%H-%M-%S-%f")

    # Finaliza a conversa
    if text == '/cancelar':
        user = update.message.from_user
        logger.info("Usuário %s cancelou a conversa.", user.first_name)
        update.message.reply_text(
            mensagem_saida , reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    file_name = "Uid_" + str(user_id) + "_time_" + date_time + ".jpg"

    # Local da imagem para deslocamento
    if  context.user_data['choice'] == "Quilometragem 📋":
        local_imagem = 'C:/Ponto/public/img/deslocamento/'
        context.user_data['caminho_imagem'] = '/img/deslocamento/' + file_name
    # Local da imagem para nota fiscal e pedágio
    if context.user_data['choice'] == 'Nota Fiscal 📋' or context.user_data['choice'] == 'Pedágio 📋':
        local_imagem = 'C:/Ponto/public/img/notas/'
        context.user_data['caminho_imagem'] = '/img/notas/' + file_name
    # Local da imagem para abastecimento
    if context.user_data['choice'] == 'Abastecimento 📋':
        local_imagem = 'C:/Ponto/public/img/abastecimento/'
        context.user_data['caminho_imagem'] = '/img/abastecimento/' + file_name

    user_name = user.first_name

    user = update.message.from_user
    
    try:
        photo_file = update.message.photo[-1].get_file()
        photo_file.download(local_imagem+file_name)
    except IndexError:
            # Questionamento a ser feito para o usuário
            update.message.reply_text(
            'Você precisa enviar uma foto! Tente novamente. '
            'Se estiver no computador, marque a opção "Compress images".',
            reply_markup=ReplyKeyboardRemove(),
            )
            # Próxima pergunta
            return FOTOGRAFIA

    # Opção escolhida: quilometragem
    if  context.user_data['choice'] == "Quilometragem 📋":
        # Com delocamento final
        if context.user_data['tipo_deslocamento']=='Final':
            # Atualiza registro adicionando km final
            my_sql.completaDeslocamento(context.user_data['id_deslocamento'],context.user_data['distancia_percorrida'],context.user_data['caminho_imagem'],context.user_data['descricao'])
            informacoes_registro = 'Modalidade: Deslocamento (final)\nID lançamento Inicial: '+str(context.user_data['id_deslocamento'])+ '\nHodômetro: ' + context.user_data['distancia_percorrida'] + '\nDeslocamento total: '+str(int(context.user_data['distancia_percorrida'])-int(context.user_data['distancia_inicial']))+'km\nDescrição: '+context.user_data['descricao']+'\n\n'
        # Sem deslocamento final
        else:
            # Insere deslocamento
            dadosDeslocamento = (context.user_data['distancia_percorrida'], '04.580.206/0001-99', context.user_data['cc'], context.user_data['data'], context.user_data['veiculo'], context.user_data['descricao'], user_name, context.user_data['caminho_imagem'], "Aberto", "0", context.user_data['tipo_deslocamento'], "reembolso",user_id, message_id)
            id = my_sql.insereDeslocamento(dadosDeslocamento)
            context.user_data['id_deslocamento'] = id
            informacoes_registro = 'Modalidade: Deslocamento\nID: '+str(id)+'\nCC: ' + str(context.user_data['cc']) + '\nData: ' + context.user_data['data'] + "\nVeículo: " + context.user_data['veiculo'] + '\nTipo: ' + context.user_data['tipo_deslocamento'] + '\nHodômetro: ' + context.user_data['distancia_percorrida'] + '\nDescrição: ' + context.user_data['descricao'] + '\n\n'

    # Opção escolhida para inserir: nota fiscal ou pedágio 
    if context.user_data['choice'] == 'Nota Fiscal 📋' or context.user_data['choice'] == 'Pedágio 📋':
        dadosDespesa = (round(float(context.user_data['valor_gasto']),2), context.user_data['cnpj'], context.user_data['cc'], context.user_data['data'], context.user_data['cartao'], context.user_data['descricao'], user_name, context.user_data['caminho_imagem'], "Aberto", "0", "tipo_gasto",user_id,message_id)
        id = my_sql.insereDespesa(dadosDespesa)
        
        informacoes_registro = 'Modalidade: Nota Fiscal\nID: '+str(id)+'\nCC: ' + str(context.user_data['cc']) + '\nData: ' + context.user_data['data'] + '\nCartão: ' + str(context.user_data['cartao']) + '\nCNPJ estabelecimento: ' + str(get_info.format(context.user_data['cnpj'])) + '\nValor gasto: R$ ' + str(context.user_data['valor_gasto']) + '\nDescrição: ' + context.user_data['descricao'] + '\n\n'

        if len(context.user_data['cnpj'])==14:
            my_sql.insereCNPJ(context.user_data['cnpj'])

    # Opção escolhida para inserir: abastecimento
    if context.user_data['choice'] == 'Abastecimento 📋':
        dadosAbastecimento = (round(float(context.user_data['valor_gasto']),2), context.user_data['cnpj'], context.user_data['cc'], context.user_data['data'], context.user_data['cartao'], round(float(context.user_data['abastecimento']),2), context.user_data['quilometro_abastecido'], user_name, context.user_data['caminho_imagem'], "Aberto", "0", context.user_data['veiculo'],user_id,message_id)  
        id = my_sql.insereAbastecimento(dadosAbastecimento)
        informacoes_registro = 'Modalidade: Abastecimento\nID: '+str(id)+'\nCC: ' + str(context.user_data['cc']) + '\nData: ' + context.user_data['data'] + '\nCartão: ' + str(context.user_data['cartao']) + '\nCNPJ estabelecimento: ' + str(get_info.format(context.user_data['cnpj'])) + '\nQuilometro abastecido: ' + str(context.user_data['quilometro_abastecido']) + '\nLitros abastecidos: ' + str(context.user_data['abastecimento']) + '\nValor gasto: R$ ' + str(context.user_data['valor_gasto']) + '\nPlaca: ' + str(context.user_data['veiculo']) + '\n\n'

    # Pega nome do usuário
    nome_cabloco = get_info.nomeCadastro(user_id)

    # Não é pedágio
    if not(context.user_data['choice'] == 'Pedágio 📋'):
        update.message.reply_text(
            'Dados obtidos: \n\n' + informacoes_registro +
            'Registro efetuado com sucesso !\n'
            'Até a próxima vez !\n\n'
            'Para enviar um novo registro favor apertar /start'
        )

        info_veiculo = ''

        # Opção escolhida: deslocamento
        if context.user_data['choice'] == "Quilometragem 📋":
            # Pega informações do veículo
            info_veiculo = get_info.verificaVeiculo(str(context.user_data['id_deslocamento']))
            print(info_veiculo)

        # Há algum aviso sobre o veículo
        if info_veiculo != '':
            update.message.reply_text(
                'AVISO:\n'+ info_veiculo
            )

        # Envia registro com foto no grupo do Telegram para os registros
        context.bot.send_photo(chat_id=-1001510539376, photo=open(local_imagem+file_name, 'rb'), 
                        caption='Usuário: ' + nome_cabloco + ' mandou um novo registro.\n\nDados obtidos: \n\n' + informacoes_registro)

        return ConversationHandler.END
    else:
        if str(context.user_data['photoIndex'])==str(context.user_data['numPedagios']):
            update.message.reply_text(
                'Dados obtidos: \n\n' + 'Registro '+str(context.user_data['photoIndex'])+'\n'+informacoes_registro +
                'Registro efetuado com sucesso !\n'
                'Até a próxima vez !\n\n'
                'Para enviar um novo registro favor apertar /start'
            )

            # Envia registro com foto no grupo do Telegram para os registros
            context.bot.send_photo(chat_id=-1001510539376, photo=open(local_imagem+file_name, 'rb'), 
                        caption='Usuário: ' + nome_cabloco + ' mandou um novo registro.\n\nDados obtidos: \n\n' + informacoes_registro)
            return ConversationHandler.END
        else:
            context.user_data['photoIndex']+=1
            update.message.reply_text(
                'Dados obtidos: \n\n' + 'Registro '+str(context.user_data['photoIndex']-1)+'\n'+informacoes_registro +
                'Registro efetuado com sucesso !\n\n'+
                'Favor enviar foto '+str(context.user_data['photoIndex'])+'/'+str(context.user_data['numPedagios']),
                reply_markup=ReplyKeyboardRemove(),
            )

            # Envia registro com foto no grupo do Telegram para os registros
            context.bot.send_photo(chat_id=-1001510539376, photo=open(local_imagem+file_name, 'rb'), 
                        caption='Usuário: ' + nome_cabloco + ' mandou um novo registro.\n\nDados obtidos: \n\n' + informacoes_registro)

            # Próxima pergunta
            return FOTOGRAFIA

# Termina a conversa
def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        'Tchau, até a próxima!\nPara iniciar um novo registro aperte: /start' , reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END

# Termina a conversa
def ajuda(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("Usuário %s cancelou a conversa.", user.first_name)
    update.message.reply_text(
        'Comando inválido !\n\nPara iniciar um novo registro aperte: /start\nPara cancelar um registro em progresso aperte: /cancelar' , reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END

def inlinequery(update: Update, context: CallbackContext) -> None:
    user_input = update.inline_query.query

    info_cnpj = get_info.searchCNPJ(str(user_input))

    try:
        results_list=[]    

        for i in info_cnpj:
            results_list.append(InlineQueryResultArticle(id=uuid4(), title=str(i[0]), input_message_content=InputTextMessageContent(str(i[0]+' | '+i[1]))))
                    
        update.inline_query.answer(results_list)

    except Exception as e:
        print(e)

def main() -> None:
    # Create the Updater and pass it your bot's token.
    # Token do bot do SARR: "1664029734:AAGLO1anpVlfAQ4_mTcesN0r6ZD6pwcWACk"
    updater = Updater("1664029734:AAGLO1anpVlfAQ4_mTcesN0r6ZD6pwcWACk", use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={ 
            TIPO_REGISTRO: [MessageHandler(Filters.all, tipo_registro), CommandHandler('cancelar', cancel)],       
            CC: [MessageHandler(Filters.all, centro_custos), CommandHandler('cancelar', cancel)],
            DATA_REG: [MessageHandler(Filters.all, dia_reg), CommandHandler('cancelar', cancel)],
            TIPO_QM: [MessageHandler(Filters.all, tipo_deslocamento), CommandHandler('cancelar', cancel)],
            QUILOMETRAGEM: [MessageHandler(Filters.all, deslocamento), CommandHandler('cancelar', cancel)],
            NF: [MessageHandler(Filters.all, nota_fiscal), CommandHandler('cancelar', cancel)],
            CNPJ: [MessageHandler(Filters.all, cnpj), CommandHandler('cancelar', cancel)],
            VALOR: [MessageHandler(Filters.all, valor), CommandHandler('cancelar', cancel)],
            CARTAO: [MessageHandler(Filters.all, cartao), CommandHandler('cancelar', cancel)],
            SELECT_CARTAO: [MessageHandler(Filters.all, select_cartao), CommandHandler('cancelar', cancel)],
            DIGITOS_CARTAO: [MessageHandler(Filters.all, digitos_cartao), CommandHandler('cancelar', cancel)],
            VEICULO: [MessageHandler(Filters.all, veiculo), CommandHandler('cancelar', cancel)],
            ABASTECIMENTO: [MessageHandler(Filters.all, abastecimento), CommandHandler('cancelar', cancel)],
            DESCRICAO: [MessageHandler(Filters.all, descricao), CommandHandler('cancelar', cancel)],
            FOTOGRAFIA: [MessageHandler(Filters.all, imagem), CommandHandler('cancelar', cancel)],
            QUILOMETRO_ABASTECIDO: [MessageHandler(Filters.all, quilometro_abastecido), CommandHandler('cancelar', cancel)],
            APAGAR_REGISTRO: [MessageHandler(Filters.all, apagar_registro), CommandHandler('cancelar', cancel)],
            CONFIRMACAO_APAGAR: [MessageHandler(Filters.all, confirmacao_apagar), CommandHandler('cancelar', cancel)],
            AGORA_APAGOU: [MessageHandler(Filters.all, agora_apagou), CommandHandler('cancelar', cancel)],
            REGISTRO_EMAIL: [MessageHandler(Filters.all, registro_email), CommandHandler('cancelar', cancel)],
            SELECT_REGISTRO: [MessageHandler(Filters.all, select_registro), CommandHandler('cancelar', cancel)],
            NUM_PEDAGIOS: [MessageHandler(Filters.all, num_pedagios), CommandHandler('cancelar', cancel)],
            INCREMENTAR_DESCRICAO: [MessageHandler(Filters.all, incrementar_descricao), CommandHandler('cancelar', cancel)],
            RELATORIO: [MessageHandler(Filters.all, relatorio), CommandHandler('cancelar', cancel)],
            SAIDA: [MessageHandler(Filters.all, saida), CommandHandler('cancelar', cancel)],
            ENTRADA: [MessageHandler(Filters.all, entrada), CommandHandler('cancelar', cancel)],
            PERNOITE: [MessageHandler(Filters.all, pernoite), CommandHandler('cancelar', cancel)],
            SOLICITANTE: [MessageHandler(Filters.all, solicitante), CommandHandler('cancelar', cancel)],
            LOCAL: [MessageHandler(Filters.all, local), CommandHandler('cancelar', cancel)],
            MODALIDADE: [MessageHandler(Filters.all, modalidade), CommandHandler('cancelar', cancel)],
            MOTORISTA: [MessageHandler(Filters.all, motorista), CommandHandler('cancelar', cancel)],
            KM_FIM_REL: [MessageHandler(Filters.all, km_fim_rel), CommandHandler('cancelar', cancel)],
            VEICULO_PLANILHA: [MessageHandler(Filters.all, veiculo_planilha), CommandHandler('cancelar', cancel)],
            KM_INI_REL: [MessageHandler(Filters.all, km_ini_rel), CommandHandler('cancelar', cancel)],
            BASE: [MessageHandler(Filters.all, base), CommandHandler('cancelar', cancel)],
            ABRIR_PORTA: [MessageHandler(Filters.all, abrir_porta), CommandHandler('cancelar', cancel)],
            ABRIR_PORTAO: [MessageHandler(Filters.all, abrir_portao), CommandHandler('cancelar', cancel)],
            FECHAR_PORTAO: [MessageHandler(Filters.all, fechar_portao), CommandHandler('cancelar', cancel)],
            LIGAR_ILUMINACAO_INTERNA: [MessageHandler(Filters.all, ligar_iluminacao_interna), CommandHandler('cancelar', cancel)],
            DESLIGAR_ILUMINACAO_INTERNA: [MessageHandler(Filters.all, desligar_iluminacao_interna), CommandHandler('cancelar', cancel)],
            LIGAR_ILUMINACAO_EXTERNA: [MessageHandler(Filters.all, ligar_iluminacao_externa), CommandHandler('cancelar', cancel)],
            DESLIGAR_ILUMINACAO_EXTERNA: [MessageHandler(Filters.all, desligar_iluminacao_externa), CommandHandler('cancelar', cancel)],
            LIGAR_AR_CONDICIONADO: [MessageHandler(Filters.all, ligar_ar_condicionado), CommandHandler('cancelar', cancel)],
            DESLIGAR_AR_CONDICIONADO: [MessageHandler(Filters.all, desligar_ar_condicionado), CommandHandler('cancelar', cancel)],
            ACESSO_USUARIOS: [MessageHandler(Filters.all, acesso_usuarios), CommandHandler('cancelar', cancel)],
            ACESSO_PERMISSAO: [MessageHandler(Filters.all, acesso_permissao), CommandHandler('cancelar', cancel)],
            CONFIRMAR_PERMISSAO: [MessageHandler(Filters.all, confirmar_permissao), CommandHandler('cancelar', cancel)],
            LIFE_COUNTER: [MessageHandler(Filters.all, life_counter), CommandHandler('cancelar', cancel)],
            GERAR_RELATORIO: [MessageHandler(Filters.all, gerar_relatorio), CommandHandler('cancelar', cancel)],
            FILTRO_RELATORIO: [MessageHandler(Filters.all,  filtro_relatorio), CommandHandler('cancelar', cancel)],
            FILTRO_USUARIO: [MessageHandler(Filters.all,  filtro_usuario), CommandHandler('cancelar', cancel)],
            TITULO_RELATORIO: [MessageHandler(Filters.all,  titulo_relatorio), CommandHandler('cancelar', cancel)],
            ENVIAR_RELATORIO: [MessageHandler(Filters.all,  enviar_relatorio), CommandHandler('cancelar', cancel)],
            AGORA_ENVIA: [MessageHandler(Filters.all,  agora_envia), CommandHandler('cancelar', cancel)],
            FILTRO_ACAO: [MessageHandler(Filters.all,  filtro_acao), CommandHandler('cancelar', cancel)],
            TIPO_DATA: [MessageHandler(Filters.all,  tipo_data), CommandHandler('cancelar', cancel)],
            ULTIMOS_DIAS: [MessageHandler(Filters.all,  ultimos_dias), CommandHandler('cancelar', cancel)],
            RELATORIO_PERIODICO: [MessageHandler(Filters.all,  relatorio_periodico), CommandHandler('cancelar', cancel)],
            EXCLUIR_RELATORIO: [MessageHandler(Filters.all,  excluir_relatorio), CommandHandler('cancelar', cancel)],
            AGORA_EXCLUI_RELATORIO: [MessageHandler(Filters.all,  agora_exclui_relatorio), CommandHandler('cancelar', cancel)],
            RELATORIO_PERIODICIDADE: [MessageHandler(Filters.all,  relatorio_periodicidade), CommandHandler('cancelar', cancel)],
            RELATORIO_HORARIO: [MessageHandler(Filters.all,  relatorio_horario), CommandHandler('cancelar', cancel)],
            RELATORIO_DESTINATARIO: [MessageHandler(Filters.all,  relatorio_destinatario), CommandHandler('cancelar', cancel)],
            RELATORIO_PERIODICO_FILTROS: [MessageHandler(Filters.all,  relatorio_periodico_filtros), CommandHandler('cancelar', cancel)],
            RELATORIO_FILTRO_USUARIO: [MessageHandler(Filters.all,  relatorio_filtro_usuario), CommandHandler('cancelar', cancel)],
            RELATORIO_FILTRO_ACAO: [MessageHandler(Filters.all,  relatorio_filtro_acao), CommandHandler('cancelar', cancel)],
            CRIAR_RELATORIO_PERIODICO: [MessageHandler(Filters.all,  criar_relatorio_periodico), CommandHandler('cancelar', cancel)],
            EDITAR_RELATORIO: [MessageHandler(Filters.all,  editar_relatorio), CommandHandler('cancelar', cancel)],
        },
        fallbacks=[CommandHandler('cancelar', cancel)],
    )

    # Teste para abrir porta pelo deep link
    dispatcher = updater.dispatcher

    # Teste para abrir porta pelo deep link
    dispatcher.add_handler(
        CommandHandler("start", abrir_porta_teste, Filters.regex(PORTA))
    )

    dispatcher.add_handler(CallbackQueryHandler(inline_handler))
    dispatcher.add_handler(conv_handler)
    dispatcher.add_handler(MessageHandler(Filters.regex('^(?!.*(start|cancelar))'), ajuda))

    dispatcher.add_handler(InlineQueryHandler(inlinequery))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
