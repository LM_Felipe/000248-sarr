'''
Arquivo para acessar o banco de dados
através de comandos "insert" para inserir
linhas de registro ao banco. Também utiliza
comandos "update" e "select"
'''


import mysql.connector
import conexao_banco


# Para testes:
#dadosDeslocamento = (km, cnpj, cc, data, placa, descricao, nome, caminho_imagem, status, responsavel_dif, tipo, reembolso,chat_id, msg_id)
#dadosDespesa = (valor, cnpj, cc, data, cartao, descricao, nome, caminho_imagem, status, responsavel_dif, tipo_gasto,chat_id,msg_id)
#dadosAbastecimento = (valor, cnpj, cc, data, cartao, litros, km, nome, caminho_imagem, status, responsavel_dif, placa,chat_id,msg_id)


# Insere um novo registro de despesa (nota fiscal)
def insereDespesa(dadosDespesa):

	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = '''INSERT INTO notinhas ( valor, cnpj, cc, data, cartao, descricao, nome, 
				caminho_imagem, status, responsavel_dif, tipo_gasto,chat_id,msg_id) 
				values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
	mycursor.execute(statement, dadosDespesa)
	mycursor.close()
	mydb.close()

# Insere um novo registro de deslocamento
def insereDeslocamento(dadosDeslocamento):

	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''INSERT INTO deslocamentos ( km, cnpj, cc, data, placa, descricao, nome, 
				caminho_imagem, status, responsavel_dif, tipo, reembolso,chat_id, msg_id) 
				values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
	mycursor.execute(statement, dadosDeslocamento)
	mycursor.close()
	mydb.close()

# Insere um novo registro de abastecimento
def insereAbastecimento(dadosAbastecimento):

	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = '''INSERT INTO abastecimento ( valor, cnpj, cc, data, cartao, litros, km, nome, 
				caminho_imagem, status, responsavel_dif, placa,chat_id,msg_id) 
				values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'''
	mycursor.execute(statement, dadosAbastecimento)
	mycursor.close()
	mydb.close()

# Apaga resgistro de despesa, deslocamento ou abastecimento
def apagarRegistro(tipo, id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "UPDATE "+tipo+" set status = 'Apagado' where id="+str(id)+" AND status != 'Lancada'"
	mycursor.execute(statement)
	mycursor.close()
	mydb.close()

# Lista os deslocamentos em aberto
def listarDeslocamentosAbertos(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''SELECT id, tipo, cc, km, placa, data, descricao from deslocamentos 
				where chat_id='''+str(telegram_id)+''' AND status != 'Lancada' order by id desc'''
	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	registros=[]
	for i in resultados:
		registros.append(['ID: '+str(i[0])+' | CC: '+str(i[2])+' | Data: '+str(i[5])+
			' | Tipo: '+str(i[1])+' | KM: '+str(i[3])+' | Descricao: '+str(i[6])])

	return registros

# Lista as despesas em aberto
def listarDespesasAbertas(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''SELECT id, valor, cc, descricao, data, cnpj,cartao from notinhas 
				where chat_id='''+str(telegram_id)+''' AND status != 'Lancada' order by id desc'''
	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	registros=[]
	for i in resultados:
		registros.append(['ID: '+str(i[0])+' | Valor:: '+str(i[1])+' | CC: '+str(i[2])+
			' | Descrição: '+str(i[3])+' | Data: '+str(i[4])+' | CNPJ: '+str(i[5])+' | Pagamento: '+str(i[6])])

	return registros

# Lista os abastecimentos em aberto
def listarAbastecimentosAbertos(telegram_id):
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''SELECT id, valor, cc, descricao, data, cnpj, cartao, placa from abastecimento 
				where chat_id='''+str(telegram_id)+''' AND status != 'Lancada' order by id desc'''
	mycursor.execute(statement)
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	registros=[]
	for i in resultados:
		registros.append(['ID: '+str(i[0])+' | Valor:: '+str(i[1])+' | CC: '+str(i[2])+
			' | Placa: '+str(i[7])+' | Descrição: '+str(i[3])+' | Data: '+str(i[4])+
			' | CNPJ: '+str(i[5])+' | Pagamento: '+str(i[6])])

	return registros

