'''
Arquivo para conexão com o banco de dados

Para realizar uma conexão com o banco em outro arquivo é necessário utilizar "import conexao_banco" e
a função conexao_banco.conexao_banco()
'''

import mysql.connector

# Variáveis
host = "45.7.171.172"
port = 1940
user = "admin"
password = "m%Le$3xA"
database = "my_schema"

# Função para se conectar ao banco de dados
def conexao_banco():
	try:
		mydb = mysql.connector.connect(
		  host=host,
		  port=port,
		  autocommit=True,
		  user=user,
		  password=password,
		  database=database
		)

		return mydb
	except:
		return False