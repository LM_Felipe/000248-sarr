'''
Arquivo para conexão com CLP 
e controle dos dispositivos da Base
'''

from pycomm3 import LogixDriver
import time
import sys

# IP do CLP
ip_clp = '129.186.0.230'

# Abrir porta
def abre_porta():
    try:
        #Conecta com PLC e altera valor da TAG
        with LogixDriver(ip_clp) as plc:

            try:
                #escreve a TAG conforme formulário na página.
                plc.write(('SARR_E_Acesso.0',1))

                return True
            except:
                #Caso haja falha na escrita
                return False
    except:
        #Caso haja falha na comunicação
        return False

# Abrir portão
def abre_portao():
    try:
        #Conecta com PLC e altera valor da TAG
        with LogixDriver(ip_clp) as plc:

            try:
                #escreve a TAG conforme formulário na página.
                plc.write(('SARR_E_Acesso.1',1))

                return True
            except:
                #Caso haja falha na escrita
                return False                
    except:
        #Caso haja falha na comunicação
        return False

# Ligar iluminação interna
def liga_iluminacao_interna():
    try:
        #Conecta com PLC e altera valor da TAG
        with LogixDriver(ip_clp) as plc:

            try:
                #escreve a TAG conforme formulário na página.
                plc.write(('SARR_E_Ilum_Circuito.1',1))

                return True
            except:
                #Caso haja falha na escrita
                return False                
    except:
        #Caso haja falha na comunicação
        return False

# Ligar iluminação externa
def liga_iluminacao_externa():
    try:
        #Conecta com PLC e altera valor da TAG
        with LogixDriver(ip_clp) as plc:

            try:
                #escreve a TAG conforme formulário na página.
                plc.write(('SARR_E_Ilum_Circuito.2',1))

                return True
            except:
                #Caso haja falha na escrita
                return False                
    except:
        #Caso haja falha na comunicação
        return False

# Ligar ar-condicionado
def liga_ar_condicionado():
    try:
        #Conecta com PLC e altera valor da TAG
        with LogixDriver(ip_clp) as plc:

            try:
                #escreve a TAG conforme formulário na página.
                plc.write(('SARR_E_Ilum_Circuito.3',1))

                return True
            except:
                #Caso haja falha na escrita
                return False                
    except:
        #Caso haja falha na comunicação
        return False

# Teste de leitura de tag do CLP
def teste_leitura_tag(tag):
    try:
        #Conecta com PLC e retorna valor da TAG
        with LogixDriver(ip_clp) as plc:
            try:
                #Lê a tag
                leitura = plc.read(tag)
                nome = str(leitura[3]).split('-')
                
                if nome[0] == "Tag doesn't exist ":
                    return ' 😐'
                else:
                    return ' 🙂'
            except:
                return ' 😐'
    except:
        return ' 😐'

# Teste de leitura das tags do CLP controladas
def teste_leitura_tags_clp():
    try:
        with LogixDriver(ip_clp) as plc:
            #Lê a tag
            leitura = plc.read('SARR_E_Acesso.0','SARR_E_Acesso.1','Local:2:O.Data.1',
                'Local:2:O.Data.2','Local:2:O.Data.3','SARR_E_Conta')

            porta = leitura[0][1]
            portao = leitura[1][1]
            iluminacao_interna = leitura[2][1]
            iluminacao_externa = leitura[3][1]
            ar_condicionado = leitura[4][1]
            lifecounter = leitura[5][1]

            # Porta está fechada
            if porta == False:
                porta = ' (F)'
            # Estado da porta é desconhecido
            else:
                porta = ' (-)'

            # Portão está fechado
            if portao == False:
                portao = ' (F)'
            # Estado do portão é desconhecido
            else:
                portao = ' (-)'

            # Iluminação interna desligada
            if iluminacao_interna == False:
                iluminacao_interna = ' (D)'
            # Iluminação intern ligada
            elif iluminacao_interna == True:
                iluminacao_interna = ' (L)'
            # Estado da iluminação interna desconhecido
            else:
                iluminacao_interna = ' (-)'

            # Iluminação externa desligada
            if iluminacao_externa == False:
                iluminacao_externa = ' (D)'
            # Iluminação externa ligada
            elif iluminacao_externa:
                iluminacao_externa = ' (L)'
            # Estado da iluminação externa é desconhecido
            else:
                iluminacao_externa = ' (-)'

            # Ar-condicionado desligado
            if ar_condicionado == False:
                ar_condicionado = ' (D)'
            # Ar-condicionado ligado
            elif ar_condicionado == True:
                ar_condicionado = ' (L)'
            # Estado do ar-condicionado é desconhecido
            else:
                ar_condicionado = ' (-)'

            # Retorna vetor com estados dos dispositivos
            return [porta,portao,iluminacao_interna,iluminacao_externa,ar_condicionado,lifecounter]
    # Falha na comunicação
    except:
        return [' 😐',' 😐',' 😐',' 😐',' 😐']

# Função do lifecounter para escrita de tag
def life_counter():
    try:
        #Conecta com PLC e altera valor da TAG
        with LogixDriver(ip_clp) as plc:

            try:
                #escreve a TAG conforme formulário na página.
                leitura = plc.read('SARR_L_Conta','SARR_E_Conta')

                return [str(leitura[0][1]),str(leitura[1][1])]
            except:
                #Caso haja falha na escrita
                return False                
    except:
        #Caso haja falha na comunicação
        return False