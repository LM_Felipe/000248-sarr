'''
Arquivo para acessar o banco de dados
através de comandos "select" e
listar diversas informações
'''

import mysql.connector
import json
from datetime import datetime, timedelta
import sendEmail
import conexao_banco

# Lista os veículos do banco
def carList():
	veiculos=[[]]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT placa, modelo from veiculos"
	mycursor.execute(statement)
	listaVeiculos = mycursor.fetchall()
	linha=0
	
	for i in listaVeiculos:
		veiculos[linha].append(i[0]+' | '+i[1])
		
		if len(veiculos[linha])==3:
			veiculos.append([])
			linha= linha+1

	mycursor.close()
	mydb.close()

	return veiculos

# Lista os centros de custo
def ccList():
	cc=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT cc, nome_obra from projetos order by trim(leading 0 from cc) asc"
	mycursor.execute(statement)
	listaCC = mycursor.fetchall()
	
	for i in listaCC:
		cc.append([i[0]+' | '+i[1]])

	mycursor.close()
	mydb.close()

	return cc

# Lista os CNPJs
def cnpjList(cc):
	cnpj=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()

	statement = "SELECT cidade from projetos where cc="+str(cc)
	mycursor.execute(statement)
	cidade = mycursor.fetchall()
	
	statement = 'SELECT razao_social, cnpj from cnpj order by FIELD(municipio, "'+cidade[0][0]+'") desc, razao_social asc limit 160'
	mycursor.execute(statement)
	listaCNPJ = mycursor.fetchall()
	
	for i in listaCNPJ:
		cnpj.append([i[0]+' | '+i[1]])

	mycursor.close()
	mydb.close()
	
	return cnpj

# Lista os CNPJs de pedágio
def cnpjListPedagio(cc):
	cnpj=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()

	statement = "SELECT cidade from projetos where cc="+str(cc)
	mycursor.execute(statement)
	cidade = mycursor.fetchall()
	
	statement = 'SELECT razao_social, cnpj from cnpj where razao_social like "%concessionaria%" order by FIELD(municipio, "'+cidade[0][0]+'") desc, razao_social asc'
	mycursor.execute(statement)
	listaCNPJ = mycursor.fetchall()
	
	for i in listaCNPJ:
		cnpj.append([i[0]+' | '+i[1]])

	mycursor.close()
	mydb.close()
	
	return cnpj

# Checa a existência do centro de custo
def checkCC(cc):

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT EXISTS(SELECT * FROM projetos WHERE cc = " + cc +" )"
	mycursor.execute(statement)
	response_cc = mycursor.fetchall()
	
	mycursor.close()
	mydb.close()

	return response_cc[0][0]

# Lista CNPJs conforme parte da razão social
def searchCNPJ(input):
	cnpj=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT razao_social, cnpj from cnpj where razao_social like '%"+input+"%' order by razao_social asc"
	mycursor.execute(statement)
	listaCNPJ = mycursor.fetchall()

	mycursor.close()
	mydb.close()

	return listaCNPJ

# Lista os CNPJs de postos de gasolina
def cnpjPOSTOS():
	cnpj=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT razao_social, cnpj from cnpj where cnae like '47.31-8-00%' or razao_social like '%posto%' order by razao_social asc"
	mycursor.execute(statement)
	listaCNPJ = mycursor.fetchall()
	
	for i in listaCNPJ:
		cnpj.append([i[0]+' | '+i[1]])

	mycursor.close()
	mydb.close()

	return cnpj

# Formata CNPJ
def format( cnpj ):
		"""
		Method to format cnpj numbers.
		Tests:
		
		>>> print Cnpj().format('53612734000198')
		53.612.734/0001-98
		"""
		if len(cnpj)==14:
			return "%s.%s.%s/%s-%s" % ( cnpj[0:2], cnpj[2:5], cnpj[5:8], cnpj[8:12], cnpj[12:14] )
		else:
			return "%s.%s.%s-%s" % ( cnpj[0:3], cnpj[3:6], cnpj[6:9], cnpj[9:11])

# Pega o nome do usuário
def nomeCadastro(telegram_id):
	cnpj=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT nome from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	nome = mycursor.fetchall()
	
	return nome[0][0]

# Pega o username do usuário
def username(telegram_id):
	cnpj=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT username from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	nome = mycursor.fetchall()
	
	return nome[0][0]

# Pega o nome do usuário
def nome(telegram_id):
	cnpj=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT nome from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	nome = mycursor.fetchall()
	
	return nome[0][0]

# Seleciona as compras pendentes da tabela "comprasteste"
def comprasPendentes():
	compras=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = '''
	select id, D.nome, cc, area, justificativa, CAST(str_to_date(prazo_entrega,'%d/%m/%Y') AS char) as prazo_entrega from comprasteste C
	left join dados_usuario D on C.solicitante = D.username where aprovacao_tec="Pendente" order by id desc limit 20
	'''
	mycursor.execute(statement)
	fetch = mycursor.fetchall()
	
	for i in fetch:
		compras.append(['id: '+str(i[0])+' | '+i[1]+' | CC'+str(i[2])+' | Tipo: '+i[3]+' | justificativa: '+i[4]])

	mycursor.close()
	mydb.close()

	return compras

# Lista cidades com país e estado de "projetos"
def cidades():
	cidade=[]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT DISTINCT cidade, pais, estado from projetos where cidade is not null"

	mycursor.execute(statement)
	listaCidades = mycursor.fetchall()
	
	for i in listaCidades:
		if(i[0] != None and i[1] != None and i[2] != None):
			cidade.append([i[1]+' - '+i[2]+' - '+i[0]])

	mycursor.close()
	mydb.close()

	return cidade

# Pega nome e cargo do usuário
def dados_relatorio(telegram_id):

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT nome, cargo from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	dados = mycursor.fetchall()
	
	return [dados[0][0],dados[0][1]]

# Pega o último lançamento de hora do usuário
def ultimoPonto(telegram_id):

	# Conexão com o banco de dados	
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT * from relatorio where id_telegram="+str(telegram_id)+" order by id desc limit 1"
	mycursor.execute(statement)
	dados = mycursor.fetchall()
	
	return dados

# Seleciona os cartões do usuário
def cartao(telegram_id):

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()
	
	mycursor = mydb.cursor()
	statement = "SELECT cartao from dados_usuario where id_telegram="+str(telegram_id)
	mycursor.execute(statement)
	nome = mycursor.fetchall()
	
	return nome[0][0]

# Veículo utilizado (lançamento de horas)
def carListPlanilha():
	veiculos=[[]]

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT placa, modelo from veiculos"
	mycursor.execute(statement)
	listaVeiculos = mycursor.fetchall()
	linha=0
	
	for i in listaVeiculos:
		veiculos[linha].append(i[1].split(' ')[0]+' | '+i[0].replace('-',''))
		
		if len(veiculos[linha])==3:
			veiculos.append([])
			linha= linha+1

	mycursor.close()
	mydb.close()

	veiculos.append(['Avião | Aéreo','Taxi | Terrestre','Uber  | Terrestre'])
	return veiculos

# Verifica irregularidades nos veículos
def verificaVeiculo(id):
	print('\n1\n')
	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''SELECT chat_id, A.id as id, inicial, final, A.placa as placa, km_oleo, 
					km_revisao, B.modelo, B.data_oleo, B.data_lic, B.data_seg, B.data_revisao
					from deslocamentos A, veiculos B where A.id = '''+id+'''
					and REPLACE(A.placa, '-', '') = REPLACE(B.placa, '-', '')'''
	mycursor.execute(statement)
	print('\n2\n')
	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()
	print('\n3\n')
	print('\nresultados = '+str(resultados[0]))
	km_inicial = resultados[0][2]
	km_final = resultados[0][3]
	placa = resultados[0][4]
	km_oleo = resultados[0][5]
	km_revisao = resultados[0][6]
	modelo = resultados[0][7]
	data_oleo = resultados[0][8]
	data_lic = resultados[0][9]
	print('\n4\n')
	data_seg = resultados[0][10]
	print('\n5\n')
	data_revisao = resultados[0][11]
	print('\n5,1\n')
	format_str = '%d/%m/%y'
	print('\n5,2\n')
	data_oleo = datetime.strptime(str(data_oleo), format_str)
	print('\n5,3\n')
	data_revisao = datetime.strptime(str(data_revisao), format_str)	
	data_seg = datetime.strptime(str(data_seg), format_str)
	data_lic = datetime.strptime(str(data_lic), format_str)
	print('\n5,4\n')
	resposta = []
	corpo_email = ''
	mensagem = ''
	km_limite = getLimite()[0]
	dias_limite = getLimite()[1]
	print('\n6\n')

	# Não tem km final
	if km_final == None:
		km_final = km_inicial
	
	# Passou do km da troca de óleo
	if km_final > km_oleo:
		mensagem = '''Troca de óleo: '''+str(km_final - km_oleo)+''' km excedido(s).\n'''
		corpo_email = '''<br>O veículo necessita de troca de óleo. Quilômetros excedidos: '''+str(km_final - km_oleo)+''' km.'''
	# Está chegando perto da troca de óleo
	elif km_final > (km_oleo - km_limite):
		mensagem = '''Troca de óleo: '''+str(km_oleo - km_final)+''' km restante(s).\n'''
		corpo_email = '''<br>O veículo necessitará em breve de troca de óleo. Quilômetros restantes: '''+str(km_oleo - km_final)+''' km.'''

	data_atual = datetime.now()

	# Passou da data de troca de óleo
	if (data_atual > data_oleo):
		dias = (str(data_atual - data_oleo)).split(' ')
		dias = dias[0]
		mensagem=mensagem+'''Troca de óleo: '''+dias+''' dia(s) excedido(s).\n'''
		corpo_email=corpo_email+'''<br>O veículo necessita de troca de óleo. Dias excedidos: '''+dias+'''.'''
	# Está chegando perto da data de troca de óleo
	elif data_atual > (data_oleo - timedelta(days=dias_limite)):
		dias = (str(data_oleo - data_atual)).split(' ')
		dias = dias[0]
		mensagem=mensagem+'''Troca de óleo: '''+dias+''' dia(s) restante(s).\n'''
		corpo_email=corpo_email+'''<br>O veículo necessitará em breve de troca de óleo. Dias restantes: '''+dias+'''.'''

	# Passou do km da revisão
	if km_final > km_revisao:
		mensagem=mensagem+'''Revisão: '''+str(km_final - km_revisao)+''' km excedido(s).\n'''
		corpo_email=corpo_email+'''<br>O veículo necessita de revisão. Quilômetros excedidos: '''+str(km_final - km_revisao)+''' km.'''
	# Está chegando no km da revisão
	elif km_final > (km_revisao - km_limite):
		mensagem=mensagem+'''Revisão: '''+str(km_revisao - km_final)+''' km restante(s).\n'''
		corpo_email=corpo_email+'''<br>O veículo necessitará em breve de revisão. Quilômetros restantes: '''+str(km_revisao - km_final)+''' km.'''	

	# Passou da data de revisão
	if (data_atual > data_revisao):
		dias = (str(data_atual - data_revisao)).split(' ')
		dias = dias[0]
		mensagem=mensagem+'''Revisão: '''+dias+''' dia(s) excedido(s).\n'''
		corpo_email=corpo_email+'''<br>O veículo necessita de revisão. Dias excedidos: '''+dias+'''.'''
	# Está chegando perto da data de revisão
	elif data_atual > (data_revisao - timedelta(days=dias_limite)):
		dias = (str(data_revisao - data_atual)).split(' ')
		dias = dias[0]
		mensagem=mensagem+'''Revisão: '''+dias+''' dia(s) restantes(s).\n'''
		corpo_email=corpo_email+'''<br>O veículo necessitará em breve de revisão. Dias restantes: '''+dias+'''.'''
	
	# Passou da data de seguro
	if (data_atual > data_seg):
		dias = (str(data_atual - data_seg)).split(' ')
		dias = dias[0]
		mensagem=mensagem+'''Seguro: '''+dias+''' dia(s) excedido(s).\n'''		
		corpo_email=corpo_email+'''<br>O veículo necessita de seguro. Dias excedidos: '''+dias+'''.'''
	# Está chegando perto da data de seguro
	elif data_atual > (data_seg - timedelta(days=dias_limite)):
		dias = (str(data_seg - data_atual)).split(' ')
		dias = dias[0]
		mensagem=mensagem+'''Seguro: '''+dias+''' dia(s) restantes(s).\n'''
		corpo_email=corpo_email+'''<br>O veículo necessitará em breve de seguro. Dias restantes: '''+dias+'''.'''
	
	# Passou da data de licenciamento
	if (data_atual > data_lic):
		dias = (str(data_atual - data_lic)).split(' ')
		dias = dias[0]
		mensagem=mensagem+'''Licenciamento: '''+dias+''' dia(s) excedido(s).\n'''		
		corpo_email=corpo_email+'''<br>O veículo necessita de licenciamento. Dias excedidos: '''+dias+'''.'''
	# Está chegando perto da data de licenciamento
	elif data_atual > (data_lic - timedelta(days=dias_limite)):
		dias = (str(data_lic - data_atual)).split(' ')
		dias = dias[0]
		if dias.includes(':'):
			dias = 0
		mensagem=mensagem+'''Licenciamento: '''+dias+''' dia(s) restantes(s).\n'''
		corpo_email=corpo_email+'''<br>O veículo necessitará em breve de licenciamento. Dias restantes: '''+dias+'''.'''
	
	# Verifica os veículos e envia e-mail para os dois responsáveis caso haja algum aviso
	if corpo_email != '' and placa != 'MFH3694' and placa != 'MFW6418':
		sendEmail.sendEmailVeiculo(modelo,placa,km_final,corpo_email,data_atual,getEmailResponsavelVeiculos('email'),nome(getEmailResponsavelVeiculos('email')))
		sendEmail.sendEmailVeiculo(modelo,placa,km_final,corpo_email,data_atual,getEmailResponsavelVeiculos('email2'),nome(getEmailResponsavelVeiculos('email2')))
	
	return mensagem

# Pega e-mail do responsável pelos veículos
def getEmailResponsavelVeiculos(email):

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''SELECT '''+email+''' from set_veiculos'''
	mycursor.execute(statement)

	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	return resultados[0][0]

# Pega nome do responsável pelos veículos
def nome(email):

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = "SELECT nome from dados_usuario where email='"+email+"'"
	mycursor.execute(statement)
	nome = mycursor.fetchall()
	
	return nome[0][0]

# Pega informações de km e dias limite para avisos de irregularidade de veículo
def getLimite():

	# Conexão com o banco de dados
	mydb = conexao_banco.conexao_banco()

	mycursor = mydb.cursor()
	statement = '''SELECT km_limite, dias_limite from set_veiculos'''
	mycursor.execute(statement)

	resultados = mycursor.fetchall()
	mycursor.close()
	mydb.close()

	return resultados[0]