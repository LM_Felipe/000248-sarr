const fs = require('fs');
const fs2 = require('fs');
const { Client } = require('whatsapp-web.js');

let date_ob = new Date();
let date = 0;
let month = 0;
let year = 0;
let hours = 0;
let minutes = 0;
let seconds = 0;

let CNPJ = require('consulta-cnpj-ws');
let cnpj = new CNPJ();

//Mysql connect
var mysql = require('mysql');
var dbconfig = require('./config/database');
connection = mysql.createConnection(dbconfig.connection);
connection.query('USE ' + dbconfig.database);

const SESSION_FILE_PATH = './session.json';
let sessionCfg;
if (fs.existsSync(SESSION_FILE_PATH)) {
    sessionCfg = require(SESSION_FILE_PATH);
}

// You can use an existing session and avoid scanning a QR code by adding a "session" object to the client options.
// This object must include WABrowserId, WASecretBundle, WAToken1 and WAToken2.
const client = new Client({ puppeteer: { headless: false }, session: sessionCfg });

var util = require('util');
get_date()
log_date = date + "_" + month + "_" + year + " " + hours + "." + minutes + "." + seconds;

var log_file = fs2.createWriteStream(__dirname + '/logs/debug_' + log_date +'.log', {flags : 'w'});
var log_stdout = process.stdout;

console.log = function(d) { //
  log_file.write(util.format(d) + '\n');
  log_stdout.write(util.format(d) + '\n');
};

client.on('qr', (qr) => {
    // Generate and scan this code with your phone
    console.log('QR RECEIVED', qr);
});

client.on('authenticated', (session) => {
    console.log('AUTHENTICATED', session);
    sessionCfg=session;
    fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
        if (err) {
            get_date()
            console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 01');
            console.error(err);
        }
    });
});

client.on('auth_failure', msg => {
    // Fired if session restore was unsuccessfull
    console.error('AUTHENTICATION FAILURE', msg);
});

client.on('ready', () => {
    console.log('Client is ready!');
    get_date()
    console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - READY');
});

client.on('message', async msg => {
    get_date()
    //console.log('MESSAGE RECEIVED', msg);
    console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - MESSAGE RECEIVED');
    console.log(msg);

    let chat = await msg.getChat();
    let info = client.info;

    if (chat.isGroup) {

        //Verifica se a mensagem está no grupo de notinhas
        if (((chat.name == 'NF e Cupom Fiscal - LM')||(chat.name == 'Teste'))&&((msg.body.toUpperCase().startsWith('DESP')))) {

            await despesa (msg);

        } else if (((chat.name == 'Veículos LMLogix')||(chat.name == 'Veiculos') || (chat.name == 'Teste'))&&((msg.body.toUpperCase().startsWith('KMI')||(msg.body.toUpperCase().startsWith('KMF'))))) {

            await deslocamento (msg);

        } else if (((chat.name == 'Veículos LMLogix') || (chat.name == 'Teste'))&&((msg.body.toUpperCase().startsWith('ABAS')))) {

            await abastecimento (msg);

        } else if (((chat.name == 'NF e Cupom Fiscal - LM'))&&((msg.body.toUpperCase().startsWith('RELEIA')))) {

             await despesa_reply (msg);

        } else if (((chat.name == 'Veículos LMLogix')||(chat.name == 'Veiculos')||(chat.name == 'Teste'))&&((msg.body.toUpperCase().startsWith('RELEIA')))) {

            const quotedMsg = await msg.getQuotedMessage();
            if (quotedMsg.body.toUpperCase().startsWith('ABAS')) {
                await abastecimento_reply (msg);
            }
            else if (quotedMsg.body.toUpperCase().startsWith('DESP')) {
                await despesa_reply (msg);
            }
            else {
                await deslocamento_reply (msg);
            }
            
        } else if ((chat.name == 'NF e Cupom Fiscal - LM')&&(msg.body.toUpperCase().startsWith('APAGAR_NOTA: '))) {

            numero = msg.author.split('@')

            numero_processado = numero[0];

            if (numero_processado.length == 12) {
                var a = numero_processado;
                var b = "9";
                var position = 4;
                numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
            }

            //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
            let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

        id = msg.body.slice(13);

        var insertQuery = "UPDATE `my_schema`.`notinhas` SET `status` = \"Apagada\" WHERE `id` = ?;";
            connection.query(insertQuery,[id], function(err, recordset){
                if (err) {
                            msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                        } else {
                            msg.reply('O registro id: ' + id + '\n\nFoi apagado com sucesso.');
                        }
            });
    }else if ((chat.name == 'Veículos LMLogix')&&(msg.body.toUpperCase().startsWith('APAGAR_DESLOCAMENTO: '))) {

            numero = msg.author.split('@')

            numero_processado = numero[0];

            if (numero_processado.length == 12) {
                var a = numero_processado;
                var b = "9";
                var position = 4;
                numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
            }

            //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
            let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

        id = msg.body.slice(21);

        var insertQuery = "UPDATE `my_schema`.`deslocamentos` SET `status` = \"Apagada\" WHERE `id` = ?;";
            connection.query(insertQuery,[id], function(err, recordset){
                if (err) {
                            msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                        } else {
                            msg.reply('O registro id: ' + id + '\n\nFoi apagado com sucesso.');
                        }
            });
    } else if ((chat.name == 'Veículos LMLogix')&&(msg.body.toUpperCase().startsWith('APAGAR_ABASTECIMENTO: '))) {

        numero = msg.author.split('@')

            numero_processado = numero[0];

            if (numero_processado.length == 12) {
                var a = numero_processado;
                var b = "9";
                var position = 4;
                numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
            }

            //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
            let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

        id = msg.body.slice(22);

        var insertQuery = "UPDATE `my_schema`.`abastecimento` SET `status` = \"Apagada\" WHERE `id` = ?;";
            connection.query(insertQuery,[id], function(err, recordset){
                if (err) {
                            msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                        } else {
                            msg.reply('O registro id: ' + id + '\n\nFoi apagado com sucesso.');
                        }
            });
    } else if (msg.body.toUpperCase().startsWith('PLACA: ')) {

        numero = msg.author.split('@')

            numero_processado = numero[0];

            if (numero_processado.length == 12) {
                var a = numero_processado;
                var b = "9";
                var position = 4;
                numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
            }

            //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
            let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

        carro = msg.body.slice(7);

        var insertQuery = 'SELECT marca, modelo, placa, km_oleo, DATE_FORMAT(\`data_oleo\`, "%d/%m/%Y") as \`data_oleo\`,DATE_FORMAT(\`data_lic\`, "%d/%m/%Y") as \`data_lic\`,DATE_FORMAT(\`data_seg\`, "%d/%m/%Y") as \`data_seg\`, km_revisao,DATE_FORMAT(\`data_revisao\`, "%d/%m/%Y") as \`data_revisao\` FROM my_schema.veiculos WHERE modelo LIKE \"%' + carro + '%\"';
            connection.query(insertQuery, function(err, info_carros){
                if (err) {
                            msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                        } else {
                            lista_carros = "";
                            var i;
                            for (i in info_carros) {
                                lista_carros = lista_carros + "\nO veículo " +  info_carros[i].marca + " - " + info_carros[i].modelo + " tem a placa: " + info_carros[i].placa + " - KM Oleo: " + info_carros[i].km_oleo + " - Data Oleo: " + info_carros[i].data_oleo + " - Data licenciamento: " + info_carros[i].data_lic + " - Data seguro: " + info_carros[i].data_seg + " - KM Revisão: " + info_carros[i].km_revisao + " - Data revisão: " + info_carros[i].data_revisao;
                            }

                            if (lista_carros == "") {lista_carros = lista_carros + "\nEste modelo não está cadastrado.";}

                            msg.reply(msg_user + lista_carros);   
                        }
            });
    } else if (msg.body.toUpperCase().startsWith('ID: ')) {

        numero = msg.author.split('@')

            numero_processado = numero[0];

            if (numero_processado.length == 12) {
                var a = numero_processado;
                var b = "9";
                var position = 4;
                numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
            }

            //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
            let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

        id_msg = msg.body.slice(4);

        var insertQuery = "SELECT id, nome FROM my_schema.dados_usuario LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username WHERE nome LIKE \"%" + id_msg + "%\"";
        if (/\d/.test(id_msg)) {insertQuery = "SELECT id, nome FROM my_schema.dados_usuario LEFT JOIN my_schema.users ON my_schema.users.username = my_schema.dados_usuario.username WHERE id LIKE \"%" + id_msg + "%\"";}
            connection.query(insertQuery, function(err, db_carros){
                if (err) {
                            msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                        } else {
                            info_pessoas = "";
                            var i;
                            for (i in db_carros) {
                                info_pessoas = info_pessoas + "\nO usuario " +  db_carros[i].nome + " tem o ID: " + db_carros[i].id;
                            }

                            if (info_pessoas == "") {info_pessoas = info_pessoas + "\nEsta pessoa não possui cadastrado";}

                            msg.reply(msg_user + info_pessoas);   
                        }
            });
    }
    }
    if (msg.body.startsWith('Check_sys')) {
        var insertQuery = "UPDATE `my_schema`.`check_conn` SET `check_conn`.`out` = `check_conn`.`in`";
            connection.query(insertQuery, function(err, recordset){
                if (err) {
                            msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                        } else {
                            msg.reply("Sistema ok");   
                        }
            });
    }
});

// Função que processa a informação da despesa
async function despesa (msg) {

    numero = msg.author.split('@')

    numero_processado = numero[0];

    if (numero_processado.length == 12) {
        var a = numero_processado;
        var b = "9";
        var position = 4;
        numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
    }

    //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
    let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

    mesagem_divida = msg.body.split("|");

    //Verifica se o texto está certo
    if (((mesagem_divida.length - 1) == 5) || ((mesagem_divida.length - 1) == 6)) {

        valor = mesagem_divida[0].replace(/[^0-9,]/g, '');
        my_cnpj = mesagem_divida[1].replace(/\D/g,'');
        cc = mesagem_divida[2].replace(/\D/g,'');
        data = mesagem_divida[3].replace(/\D/g,'');
        cartao = mesagem_divida[4];

        descricao = mesagem_divida[5];
        responsavel_dif = 0;
        if ((mesagem_divida.length - 1) == 6) {responsavel_dif = mesagem_divida[6].replace(/\D/g,'');}
        tipo_gasto = "Cartão"
        if (mesagem_divida[4].toUpperCase().trim().startsWith('CAIXA ')) {tipo_gasto = "Caixa";}

        contem_erro = 0;
        erros = "";

        data = data.slice(6, 8) + "/" + data.slice(4, 6) + "/" + data.slice(0, 4)
        my_cnpj = my_cnpj.slice(0, 2) + "." + my_cnpj.slice(2, 5) + "." + my_cnpj.slice(5, 8) + "/" +  my_cnpj.slice(8, 12) + "-" +  my_cnpj.slice(12, 15);

        if (cartao.toUpperCase().trim().startsWith('CARTÃO')) { cartao = "Cartão: " + cartao.replace(/\D/g,'');} 

        //my_msg = 'Centro de custos: ' + cc + '\nData: ' + data + '\nValor: ' + valor + '\nCNPJ: ' + cnpj + '\nCartão: ' + cartao + '\nDescrição: ' + descricao;
        my_msg = '- CC ' + cc + '\n- ' + data + '\n- R$' + valor + '\n- ' + my_cnpj + '\n- ' + cartao + '\n- ' + descricao;
        if ((mesagem_divida.length - 1) == 6) {my_msg = my_msg + '\nCusto enviado para o ID: ' + responsavel_dif;}

        var regex  = /^\d+(?:\,\d{0,2})$/;
        if (regex.test(valor)) {valor = valor.replace(",",".")} else {contem_erro = 1; erros = erros + "\nEsse valor de gasto não está no padrão correto"}
        if ((my_cnpj.length) != 18 ) {contem_erro = 1; erros = erros + "\nEsse número de CNPJ não está correto"}
        if ((cc.length) <= 1 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar um centro de custos"}
        if ((data.length) != 10) {contem_erro = 1; erros = erros + "\nVocê não digitou uma data no formato aceitável"}
        if (!(mesagem_divida[4].toUpperCase().trim().startsWith('CARTÃO') || mesagem_divida[4].toUpperCase().trim().startsWith('CAIXA'))) {contem_erro = 1; erros = erros + "\nDeve ser informado se é cartão ou caixa";}
        if ((descricao.length) <= 2 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar uma descrição do gasto"}
        if (((mesagem_divida.length - 1) == 6) && ((responsavel_dif.length) <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável."}

        if (contem_erro) {msg.reply( msg_user + erros); }
        else {
        //Verifica se uma imagem foi enviada
        if(msg.hasMedia) {
            const media = await msg.downloadMedia();
            // do something with the media data here
            if ( media.mimetype == 'image/jpeg') {
                
                get_date()
                caminho = './img/notas/' + numero_processado + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';

                fs.writeFile(caminho, media.data, 'base64' , function(err) { 
                    if (err) {
                    get_date()
                    console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 02');
                    console.log(err); }});

                var insertQuery = "INSERT INTO notinhas ( valor, cnpj, cc, data, cartao, descricao, numero_telefone, caminho_imagem, status, responsavel_dif, tipo_gasto) values (?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(insertQuery,[valor, my_cnpj, cc, data, cartao, descricao, numero_processado, caminho, 'Lancada', responsavel_dif, tipo_gasto], function(err, rows){
                if (err) {
                    msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                }
                else 
                {
                        var insertQuery = "SELECT * FROM my_schema.notinhas ORDER BY id DESC LIMIT 1;";
                        info_estabelecimento = ""

                        connection.query(insertQuery, function(err, recordset){
                            cnpj.consultaCNPJ({cnpj: my_cnpj.replace(/\D/g,'') })
                            .then(result => {
                                if (result.status.localeCompare('OK') == 0) {
                                    info_estabelecimento = '\n\nEstabelecimento:\n' + result.nome + '\n' + result.atividade_principal[0].text + '\n' + result.logradouro + ', ' + result.municipio + ' - ' + result.uf
                                    } else {
                                    info_estabelecimento = '\n\nEstabelecimento não encontrado\nErro: ' + result.message}

                                    //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                                    msg.reply(msg_user + 'Registrei com sucesso:\n' + my_msg + info_estabelecimento + '\n\nPara apagar envie *Apagar_nota: ' + recordset[0].id + '*');
                            })
                            .catch(error => {
                                info_estabelecimento = '\n\nEstabelecimento não encontrado\nErro: ' + error
                                //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                                msg.reply(msg_user + 'Registrei com sucesso:\n' + my_msg + info_estabelecimento + '\n\nPara apagar envie *Apagar_nota: ' + recordset[0].id + '*');
                                get_date()
                                console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 03');
                                console.log(error)
                            })
                    });
                }});

            }
            else {
                msg.reply(msg_user + 'Eu só aceito imagens !'); 
            }
        }
        else
        {
           msg.reply( msg_user + 'Você esqueceu de mandar a foto da notinha, mande novamente !'); 
        }
        }
    } else {
        msg.reply( msg_user + 'A mensagem não está no padrão correto !'); 
    }
}

// Função que processa a informação do deslocamento
async function deslocamento (msg) {

    numero = msg.author.split('@')

    numero_processado = numero[0];

    if (numero_processado.length == 12) {
        var a = numero_processado;
        var b = "9";
        var position = 4;
        numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
    }

    //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
    let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

    mesagem_divida = msg.body.split("|");

    //Verifica se o texto está certo
    if (((mesagem_divida.length - 1) == 5) || ((mesagem_divida.length - 1) == 6) || ((mesagem_divida.length - 1) == 7)) {

        km = mesagem_divida[0].replace(/\D/g,'');
        cnpj = mesagem_divida[1].replace(/\D/g,'');
        cc = mesagem_divida[2].replace(/\D/g,'');
        data = mesagem_divida[3].replace(/\D/g,'');
        placa = mesagem_divida[4].replace("[^a-zA-Z0-9]", "");
        placa = mesagem_divida[4].replace("-", "");
        placa = placa.replace(/\s/g, '');
        descricao = mesagem_divida[5];
        responsavel_dif = 0;
        reembolso = 0;
        if (((mesagem_divida.length - 1) == 6)||((mesagem_divida.length - 1) == 7)) {if (mesagem_divida[6].trim().toUpperCase().startsWith('REEMBOLSO')) { reembolso = mesagem_divida[6];} else if (mesagem_divida[6].trim().toUpperCase().startsWith('ID')) { responsavel_dif = mesagem_divida[6].replace(/\D/g,'');}}
        if ((mesagem_divida.length - 1) == 7) {if (mesagem_divida[7].trim().toUpperCase().startsWith('REEMBOLSO')) { reembolso = mesagem_divida[7];} else if (mesagem_divida[7].trim().toUpperCase().startsWith('ID')) { responsavel_dif = mesagem_divida[7].replace(/\D/g,'');}}
        tipo = "kmi";
        if (msg.body.toUpperCase().startsWith('KMF')) {tipo = "kmf";}


        contem_erro = 0;
        erros = "";

        data = data.slice(6, 8) + "/" + data.slice(4, 6) + "/" + data.slice(0, 4)
        cnpj = cnpj.slice(0, 2) + "." + cnpj.slice(2, 5) + "." + cnpj.slice(5, 8) + "/" +  cnpj.slice(8, 12) + "-" +  cnpj.slice(12, 15);
        placa = placa.slice(0, 3) + "-" + placa.slice(3, 7)

        //my_msg = 'Centro de custos: ' + cc + '\nData: ' + data + '\nKm: ' + km + '\nTipo: ' + tipo + '\nPlaca: ' + placa  + '\nCNPJ: ' + cnpj  + '\nDescrição: ' + descricao;
        my_msg = '- CC ' + cc + '\n- ' + data + '\n- ' + tipo + ' ' + km + '\n- ' + placa  + '\n- ' + cnpj  + '\n- ' + descricao;
        if (((mesagem_divida.length - 1) == 6) && (responsavel_dif.length > 1)) {my_msg = my_msg + '\n- Deslocamento enviado para o ID: ' + responsavel_dif;}
        if (((mesagem_divida.length - 1) == 7) && (responsavel_dif.length > 1)) {my_msg = my_msg + '\n- Deslocamento enviado para o ID: ' + responsavel_dif;}

        if (((mesagem_divida.length - 1) == 6) && (reembolso.length > 1)) {my_msg = my_msg + '\n- Despesa enviada para : ' + reembolso;}
        if (((mesagem_divida.length - 1) == 7) && (reembolso.length > 1)) {my_msg = my_msg + '\n- Despesa enviada para : ' + reembolso;}

        var regex  = /[a-zA-Z]{3}-[0-9]{4}/;
        if (regex.test(placa)) {} else {contem_erro = 1; erros = erros + "\nEsse número de placa não está no padrão correto"}
        if ((cnpj.length) != 18 ) {contem_erro = 1; erros = erros + "\nEsse número de CNPJ não está correto"}
        if ((cc.length) <= 1 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar um centro de custos"}
        if ((data.length) != 10) {contem_erro = 1; erros = erros + "\nVocê não digitou uma data no formato aceitável"}
        if ((descricao.length) <= 2 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar uma descrição do deslocamento"}
        if (((mesagem_divida.length - 1) == 6) && (responsavel_dif.length <= 1) && (reembolso.length <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável ou informar se é reembolso."}
        if (((mesagem_divida.length - 1) == 7) && (responsavel_dif.length <= 1) && (reembolso.length <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável ou informar se é reembolso."}

        if (contem_erro) {msg.reply( msg_user + erros); }
        else {
        //Verifica se uma imagem foi enviada
        if(msg.hasMedia) {
            const media = await msg.downloadMedia();
            // do something with the media data here
            if ( media.mimetype == 'image/jpeg') {

                get_date()
                caminho = './img/deslocamento/' + numero_processado + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';

                fs.writeFile(caminho, media.data, 'base64' , function(err) { 
                    if (err) {
                    get_date()
                    console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 04');
                    console.log(err); }});

                var insertQuery = "INSERT INTO deslocamentos ( km, cnpj, cc, data, placa, descricao, numero_telefone, caminho_imagem, status, responsavel_dif, tipo, reembolso) values (?,?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(insertQuery,[km, cnpj, cc, data, placa, descricao, numero_processado, caminho, 'Lancada', responsavel_dif, tipo, reembolso], function(err, rows){
                if (err) {
                    msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                }
                else 
                {
                    var insertQuery = "SELECT * FROM my_schema.deslocamentos ORDER BY id DESC LIMIT 1;";
                    connection.query(insertQuery, function(err, recordset){
                        db_id = recordset[0].id;

                        psq_tipo = "kmi";
                        if (tipo == "kmi" ) {psq_tipo = "kmf";}
                        var insertQuery = "SELECT * FROM my_schema.deslocamentos WHERE tipo = ? AND placa = ? ORDER BY id DESC LIMIT 1;";
                        connection.query(insertQuery,[psq_tipo,placa], function(err, info){ 
                            if (info.length) {
                            ultimo_km = info[0].km;

                            var insertQuery = "SELECT * FROM my_schema.veiculos WHERE placa = ?;";
                            connection.query(insertQuery,[placa], function(err, data){
                                if (data.length) {
                                    veiculo = data[0].marca + " - " + data[0].modelo 
                                    avisos = "\n\nAvisos:"
                                    tem_aviso = 0;
                                    if ((tipo == "kmi")&&(km != ultimo_km)) { tem_aviso = 1; avisos = avisos + "\nO quilometro incial não bate com o último registro: " + ultimo_km;}
                                    if ((tipo == "kmf")&&(km <= ultimo_km)) { tem_aviso = 1; avisos = avisos + "\nO quilometro final é menor que o inicial: " + ultimo_km;}
                                    if (km >= data[0].km_oleo) { tem_aviso = 1; avisos = avisos + "\nO oleo já passou da quilometragem: Km " + data[0].km_oleo }
                                    if (data >= parseInt(data[0].data_oleo)) { tem_aviso = 1; avisos = avisos + "\nO oleo já passou da validade: " + data[0].data_oleo }
                                    if (data >= parseInt(data[0].data_lic)) { tem_aviso = 1; avisos = avisos + "\nO licenciamento está vencido: " + data[0].data_lic }
                                    if (data >= parseInt(data[0].data_seg)) { tem_aviso = 1; avisos = avisos + "\nO seguro está vencido: " + data[0].data_seg}
                                    if ((data >= parseInt(data[0].data_revisao))||(km >= data[0].km_revisao)) { tem_aviso = 1; avisos = avisos + "\nO veículo precisa ser revisado: " + data[0].data_revisao + " ou Km " + data[0].km_revisao}

                                    if (tem_aviso == 0) {avisos = "";}
                                    
                                    //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + veiculo + "\n" + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + db_id + '\n\nId do registro: ' + db_id + avisos);
                                    msg.reply(msg_user + 'Registrei com sucesso:\n- ' + veiculo + "\n" + my_msg + '\n\nPara apagar envie *Apagar_deslocamento: ' + db_id + '*' + avisos);
                                } else { 
                                    avisos = "\n\nAviso:\nEste veículo não está cadastrado."
                                    //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + "\n" + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + db_id + '\n\nId do registro: ' + db_id + avisos);
                                    msg.reply(msg_user + 'Registrei com sucesso:' + "\n" + my_msg + '\n\nPara apagar envie *Apagar_deslocamento: ' + db_id + '*' + avisos);
                                    }
                            });
                        } else {
                            var insertQuery = "SELECT * FROM my_schema.deslocamentos ORDER BY id DESC LIMIT 1;";
                            connection.query(insertQuery, function(err, recordset){
                                avisos = "\n\nAviso:\nPrimeiro registro deste veículo."
                                db_id = recordset[0].id;
                                msg.reply(msg_user + 'Registrei com sucesso:' + "\n" + my_msg + '\n\nPara apagar envie *Apagar_deslocamento: ' + db_id + '*' + avisos);
                            });
                        }});
                        
                    });
                }});

            }
            else {
                msg.reply(msg_user + 'Eu só aceito imagens !'); 
            }
        }
        else
        {
           msg.reply( msg_user + 'Você esqueceu de mandar a foto da notinha, mande novamente !'); 
        }
        }
    } else {
        msg.reply( msg_user + 'A mensagem não está no padrão correto !'); 
    }  
}

// Função que processa a informação do abastecimento
async function abastecimento (msg) {

    numero = msg.author.split('@')

    numero_processado = numero[0];

    if (numero_processado.length == 12) {
        var a = numero_processado;
        var b = "9";
        var position = 4;
        numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
    }

    //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
    let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

    mesagem_divida = msg.body.split("|");

    //Verifica se o texto está certo
    if (((mesagem_divida.length - 1) == 7) || ((mesagem_divida.length - 1) == 8)) {

        valor = mesagem_divida[0].replace(/[^0-9,]/g, '');
        cnpj = mesagem_divida[1].replace(/\D/g,'');
        cc = mesagem_divida[2].replace(/\D/g,'');
        data = mesagem_divida[3].replace(/\D/g,'');
        cartao = mesagem_divida[4].replace(/\D/g,'');
        litros = mesagem_divida[5].replace(/[^0-9,]/g, '');
        km = mesagem_divida[6].replace(/\D/g,'');
        placa = mesagem_divida[7].replace("[^a-zA-Z0-9]", "");
        placa = mesagem_divida[7].replace("-", "");
        placa = placa.replace(/\s/g, '');

        responsavel_dif = 0;
        if ((mesagem_divida.length - 1) == 8) {responsavel_dif = mesagem_divida[8].replace(/\D/g,'');}

        contem_erro = 0;
        erros = "";

        data = data.slice(6, 8) + "/" + data.slice(4, 6) + "/" + data.slice(0, 4)
        cnpj = cnpj.slice(0, 2) + "." + cnpj.slice(2, 5) + "." + cnpj.slice(5, 8) + "/" +  cnpj.slice(8, 12) + "-" +  cnpj.slice(12, 15);
        placa = placa.slice(0, 3) + "-" + placa.slice(3, 7)

        //my_msg = 'Centro de custos: ' + cc + '\nData: ' + data + '\nValor: ' + valor + '\nCNPJ: ' + cnpj + '\nCartão: ' + cartao + '\nDescrição: ' + descricao;
        my_msg = '- CC ' + cc + '\n- ' + data + '\n- R$' + valor + '\n- ' + cnpj + '\n- ' + cartao + '\n- ' + litros + '\n- ' + km + '\n- ' + placa;
        if ((mesagem_divida.length - 1) == 6) {my_msg = my_msg + '\nCusto enviado para o ID: ' + responsavel_dif;}

        var regex  = /^\d+(?:\,\d{0,2})$/;
        if (regex.test(valor)) {valor = valor.replace(",",".")} else {contem_erro = 1; erros = erros + "\nEsse valor de gasto não está no padrão correto"}
        if ((cnpj.length) != 18 ) {contem_erro = 1; erros = erros + "\nEsse número de CNPJ não está correto"}
        if ((cc.length) <= 1 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar um centro de custos"}
        if ((data.length) != 10) {contem_erro = 1; erros = erros + "\nVocê não digitou uma data no formato aceitável"}
        if ((cartao.length) != 4 ) {contem_erro = 1; erros = erros + "\nO número do cartão está no formato incorreto"}
        if (regex.test(litros)) {litros = litros.replace(",",".")} else {contem_erro = 1; erros = erros + "\nEsse valor de litros não está no padrão correto"}
        if ((km.length) < 1 ) {contem_erro = 1; erros = erros + "\nPrecisa adicionar uma quilometragem"}
        if (((mesagem_divida.length - 1) == 7) && ((responsavel_dif.length) <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável."}

        if (contem_erro) {msg.reply( msg_user + erros); }
        else {
        //Verifica se uma imagem foi enviada
        if(msg.hasMedia) {
            const media = await msg.downloadMedia();
            // do something with the media data here
            if ( media.mimetype == 'image/jpeg') {

                get_date()
                caminho = './img/abastecimento/' + numero_processado + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';

                fs.writeFile(caminho, media.data, 'base64' , function(err) { 
                    if (err) {
                    get_date()
                    console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 05');
                    console.log(err); }});

                var insertQuery = "INSERT INTO abastecimento ( valor, cnpj, cc, data, cartao, litros, km, numero_telefone, caminho_imagem, status, responsavel_dif, placa) values (?,?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(insertQuery,[valor, cnpj, cc, data, cartao, litros, km, numero_processado, caminho, 'Lancada', responsavel_dif, placa], function(err, rows){
                if (err) {
                    msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                }
                else 
                {
                    var insertQuery = "SELECT * FROM my_schema.abastecimento ORDER BY id DESC LIMIT 1;";
                    connection.query(insertQuery, function(err, recordset){
                        //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                        msg.reply(msg_user + 'Registrei com sucesso:\n' + my_msg + '\n\nPara apagar envie *Apagar_abastecimento: ' + recordset[0].id + '*');
                    });
                }});

            }
            else {
                msg.reply(msg_user + 'Eu só aceito imagens !'); 
            }
        }
        else
        {
           msg.reply( msg_user + 'Você esqueceu de mandar a foto da notinha, mande novamente !'); 
        }
        }
    } else {
        msg.reply( msg_user + 'A mensagem não está no padrão correto !'); 
    }

}

// Função que processa a informação da despesa
async function despesa_reply (msg) {

    const quotedMsg = await msg.getQuotedMessage();

    numero = quotedMsg.author.split('@')

    numero_processado = numero[0];

    if (numero_processado.length == 12) {
        var a = numero_processado;
        var b = "9";
        var position = 4;
        numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
    }

    //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
    let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

    mesagem_divida = quotedMsg.body.split("|");

    //Verifica se o texto está certo
    if (((mesagem_divida.length - 1) == 5) || ((mesagem_divida.length - 1) == 6)) {

        valor = mesagem_divida[0].replace(/[^0-9,]/g, '');
        my_cnpj = mesagem_divida[1].replace(/\D/g,'');
        cc = mesagem_divida[2].replace(/\D/g,'');
        data = mesagem_divida[3].replace(/\D/g,'');
        cartao = mesagem_divida[4];

        descricao = mesagem_divida[5];
        responsavel_dif = 0;
        if ((mesagem_divida.length - 1) == 6) {responsavel_dif = mesagem_divida[6].replace(/\D/g,'');}
        tipo_gasto = "Cartão"
        if (mesagem_divida[4].toUpperCase().trim().startsWith('CAIXA ')) {tipo_gasto = "Caixa";}

        contem_erro = 0;
        erros = "";

        data = data.slice(6, 8) + "/" + data.slice(4, 6) + "/" + data.slice(0, 4)
        my_cnpj = my_cnpj.slice(0, 2) + "." + my_cnpj.slice(2, 5) + "." + my_cnpj.slice(5, 8) + "/" +  my_cnpj.slice(8, 12) + "-" +  my_cnpj.slice(12, 15);

        if (cartao.toUpperCase().trim().startsWith('CARTÃO')) { cartao = "Cartão: " + cartao.replace(/\D/g,'');} 

        //my_msg = 'Centro de custos: ' + cc + '\nData: ' + data + '\nValor: ' + valor + '\nCNPJ: ' + cnpj + '\nCartão: ' + cartao + '\nDescrição: ' + descricao;
        my_msg = '- CC ' + cc + '\n- ' + data + '\n- R$' + valor + '\n- ' + my_cnpj + '\n- ' + cartao + '\n- ' + descricao;
        if ((mesagem_divida.length - 1) == 6) {my_msg = my_msg + '\nCusto enviado para o ID: ' + responsavel_dif;}

        var regex  = /^\d+(?:\,\d{0,2})$/;
        if (regex.test(valor)) {valor = valor.replace(",",".")} else {contem_erro = 1; erros = erros + "\nEsse valor de gasto não está no padrão correto"}
        if ((my_cnpj.length) != 18 ) {contem_erro = 1; erros = erros + "\nEsse número de CNPJ não está correto"}
        if ((cc.length) <= 1 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar um centro de custos"}
        if ((data.length) != 10) {contem_erro = 1; erros = erros + "\nVocê não digitou uma data no formato aceitável"}
        if (!(mesagem_divida[4].toUpperCase().trim().startsWith('CARTÃO ')||mesagem_divida[4].toUpperCase().trim().startsWith('CAIXA '))) {contem_erro = 1; erros = erros + "\nDeve ser informado se é cartão ou caixa";}
        if ((descricao.length) <= 2 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar uma descrição do gasto"}
        if (((mesagem_divida.length - 1) == 6) && ((responsavel_dif.length) <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável."}

        if (contem_erro) {msg.reply( msg_user + erros); }
        else {
        //Verifica se uma imagem foi enviada
        if(quotedMsg.hasMedia) {
            const media = await quotedMsg.downloadMedia();
            // do something with the media data here
            if ( media.mimetype == 'image/jpeg') {

                get_date()
                caminho = './img/notas/' + numero_processado + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';

                fs.writeFile(caminho, media.data, 'base64' , function(err) { 
                    if (err) {
                    get_date()
                    console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 06');
                    console.log(err); }});

                var insertQuery = "INSERT INTO notinhas ( valor, cnpj, cc, data, cartao, descricao, numero_telefone, caminho_imagem, status, responsavel_dif, tipo_gasto) values (?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(insertQuery,[valor, my_cnpj, cc, data, cartao, descricao, numero_processado, caminho, 'Lancada', responsavel_dif, tipo_gasto], function(err, rows){
                if (err) {
                    msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                }
                else 
                {
                        var insertQuery = "SELECT * FROM my_schema.notinhas ORDER BY id DESC LIMIT 1;";
                        info_estabelecimento = ""

                        connection.query(insertQuery, function(err, recordset){
                            cnpj.consultaCNPJ({cnpj: my_cnpj.replace(/\D/g,'') })
                            .then(result => {
                                if (result.status.localeCompare('OK') == 0) {
                                    info_estabelecimento = '\n\nEstabelecimento:\n' + result.nome + '\n' + result.atividade_principal[0].text + '\n' + result.logradouro + ', ' + result.municipio + ' - ' + result.uf
                                    } else {
                                    info_estabelecimento = '\n\nEstabelecimento não encontrado\nErro: ' + result.message}
                                    
                                    //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                                    msg.reply(msg_user + 'Registrei com sucesso:\n' + my_msg + info_estabelecimento + '\n\nPara apagar envie *Apagar_nota: ' + recordset[0].id + '*');
                            })
                            .catch(error => {
                                info_estabelecimento = '\n\nEstabelecimento não encontrado\nErro: ' + error
                                //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                                msg.reply(msg_user + 'Registrei com sucesso:\n' + my_msg + info_estabelecimento + '\n\nPara apagar envie *Apagar_nota: ' + recordset[0].id + '*');
                                get_date()
                                console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 07');
                                console.log(error)
                            })
                    });
                }});

            }
            else {
                msg.reply(msg_user + 'Eu só aceito imagens !'); 
            }
        }
        else
        {
           msg.reply( msg_user + 'Você esqueceu de mandar a foto da notinha, mande novamente !'); 
        }
        }
    } else {
        msg.reply( msg_user + 'A mensagem não está no padrão correto !'); 
    }
}

// Função que processa a informação do deslocamento
async function deslocamento_reply (msg) {

    const quotedMsg = await msg.getQuotedMessage();

    numero = quotedMsg.author.split('@')

    numero_processado = numero[0];

    if (numero_processado.length == 12) {
        var a = numero_processado;
        var b = "9";
        var position = 4;
        numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
    }

    //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
    let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

    mesagem_divida = quotedMsg.body.split("|");

    //Verifica se o texto está certo
    if (((mesagem_divida.length - 1) == 5) || ((mesagem_divida.length - 1) == 6) || ((mesagem_divida.length - 1) == 7)) {

        km = mesagem_divida[0].replace(/\D/g,'');
        cnpj = mesagem_divida[1].replace(/\D/g,'');
        cc = mesagem_divida[2].replace(/\D/g,'');
        data = mesagem_divida[3].replace(/\D/g,'');
        placa = mesagem_divida[4].replace("[^a-zA-Z0-9]", "");
        placa = mesagem_divida[4].replace("-", "");
        placa = placa.replace(/\s/g, '');
        descricao = mesagem_divida[5];
        responsavel_dif = 0;
        reembolso = 0;
        if (((mesagem_divida.length - 1) == 6)||((mesagem_divida.length - 1) == 7)) {if (mesagem_divida[6].trim().toUpperCase().startsWith('REEMBOLSO')) { reembolso = mesagem_divida[6];} else if (mesagem_divida[6].trim().toUpperCase().startsWith('ID')) { responsavel_dif = mesagem_divida[6].replace(/\D/g,'');}}
        if ((mesagem_divida.length - 1) == 7) {if (mesagem_divida[7].trim().toUpperCase().startsWith('REEMBOLSO')) { reembolso = mesagem_divida[7];} else if (mesagem_divida[7].trim().toUpperCase().startsWith('ID')) { responsavel_dif = mesagem_divida[7].replace(/\D/g,'');}}
        tipo = "kmi";
        if (quotedMsg.body.toUpperCase().startsWith('KMF')) {tipo = "kmf";}


        contem_erro = 0;
        erros = "";

        data = data.slice(6, 8) + "/" + data.slice(4, 6) + "/" + data.slice(0, 4)
        cnpj = cnpj.slice(0, 2) + "." + cnpj.slice(2, 5) + "." + cnpj.slice(5, 8) + "/" +  cnpj.slice(8, 12) + "-" +  cnpj.slice(12, 15);
        placa = placa.slice(0, 3) + "-" + placa.slice(3, 7)

        //my_msg = 'Centro de custos: ' + cc + '\nData: ' + data + '\nKm: ' + km + '\nTipo: ' + tipo + '\nPlaca: ' + placa  + '\nCNPJ: ' + cnpj  + '\nDescrição: ' + descricao;
        my_msg = '- CC ' + cc + '\n- ' + data + '\n- ' + tipo + ' ' + km + '\n- ' + placa  + '\n- ' + cnpj  + '\n- ' + descricao;
        if (((mesagem_divida.length - 1) == 6) && (responsavel_dif.length > 1)) {my_msg = my_msg + '\nDeslocamento enviado para o ID: ' + responsavel_dif;}
        if (((mesagem_divida.length - 1) == 7) && (responsavel_dif.length > 1)) {my_msg = my_msg + '\nDeslocamento enviado para o ID: ' + responsavel_dif;}

        if (((mesagem_divida.length - 1) == 6) && (reembolso.length > 1)) {my_msg = my_msg + '\nDespesa enviada para : ' + reembolso;}
        if (((mesagem_divida.length - 1) == 7) && (reembolso.length > 1)) {my_msg = my_msg + '\nDespesa enviada para : ' + reembolso;}

        var regex  = /[a-zA-Z]{3}-[0-9]{4}/;
        if (regex.test(placa)) {} else {contem_erro = 1; erros = erros + "\nEsse número de placa não está no padrão correto"}
        if ((cnpj.length) != 18 ) {contem_erro = 1; erros = erros + "\nEsse número de CNPJ não está correto"}
        if ((cc.length) <= 1 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar um centro de custos"}
        if ((data.length) != 10) {contem_erro = 1; erros = erros + "\nVocê não digitou uma data no formato aceitável"}
        if ((descricao.length) <= 2 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar uma descrição do deslocamento"}
        if (((mesagem_divida.length - 1) == 6) && (responsavel_dif.length <= 1) && (reembolso.length <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável ou informar se é reembolso."}
        if (((mesagem_divida.length - 1) == 7) && (responsavel_dif.length <= 1) && (reembolso.length <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável ou informar se é reembolso."}

        if (contem_erro) {msg.reply( msg_user + erros); }
        else {
        //Verifica se uma imagem foi enviada
        if(quotedMsg.hasMedia) {
            const media = await quotedMsg.downloadMedia();
            // do something with the media data here
            if ( media.mimetype == 'image/jpeg') {

                get_date()
                caminho = './img/deslocamento/' + numero_processado + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';

                fs.writeFile(caminho, media.data, 'base64' , function(err) { 
                    if (err) {
                    get_date()
                    console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 08');
                    console.log(err); }});

                var insertQuery = "INSERT INTO deslocamentos ( km, cnpj, cc, data, placa, descricao, numero_telefone, caminho_imagem, status, responsavel_dif, tipo, reembolso) values (?,?,?,?,?,?,?,?,?,?,?, ?)";
                connection.query(insertQuery,[km, cnpj, cc, data, placa, descricao, numero_processado, caminho, 'Lancada', responsavel_dif, tipo, reembolso], function(err, rows){
                if (err) {
                    msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                }
                else 
                {
                    var insertQuery = "SELECT * FROM my_schema.deslocamentos ORDER BY id DESC LIMIT 1;";
                    connection.query(insertQuery, function(err, recordset){
                        db_id = recordset[0].id;

                        psq_tipo = "kmi";
                        if (tipo == "kmi" ) {psq_tipo = "kmf";}
                        var insertQuery = "SELECT * FROM my_schema.deslocamentos WHERE tipo = ? AND placa = ? ORDER BY id DESC LIMIT 1;";
                        connection.query(insertQuery,[psq_tipo,placa], function(err, info){ 
                            if (info.length) {
                            ultimo_km = info[0].km;

                            var insertQuery = "SELECT * FROM my_schema.veiculos WHERE placa = ?;";
                            connection.query(insertQuery,[placa], function(err, data){
                                if (data.length) {
                                    veiculo = data[0].marca + " - " + data[0].modelo 
                                    avisos = "\n\nAvisos:"
                                    tem_aviso = 0;
                                    if ((tipo == "kmi")&&(km != ultimo_km)) { tem_aviso = 1; avisos = avisos + "\nO quilometro incial não bate com o último registro: " + ultimo_km;}
                                    if ((tipo == "kmf")&&(km <= ultimo_km)) { tem_aviso = 1; avisos = avisos + "\nO quilometro final é menor que o inicial: " + ultimo_km;}
                                    if (km >= data[0].km_oleo) { tem_aviso = 1; avisos = avisos + "\nO oleo já passou da quilometragem: Km " + data[0].km_oleo }
                                    if (data >= parseInt(data[0].data_oleo)) { tem_aviso = 1; avisos = avisos + "\nO oleo já passou da validade: " + data[0].data_oleo }
                                    if (data >= parseInt(data[0].data_lic)) { tem_aviso = 1; avisos = avisos + "\nO licenciamento está vencido: " + data[0].data_lic }
                                    if (data >= parseInt(data[0].data_seg)) { tem_aviso = 1; avisos = avisos + "\nO seguro está vencido: " + data[0].data_seg}
                                    if ((data >= parseInt(data[0].data_revisao))||(km >= data[0].km_revisao)) { tem_aviso = 1; avisos = avisos + "\nO veículo precisa ser revisado: " + data[0].data_revisao + " ou Km " + data[0].km_revisao}

                                    if (tem_aviso == 0) {avisos = "";}
                                    
                                    //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + veiculo + "\n" + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + db_id + '\n\nId do registro: ' + db_id + avisos);
                                    msg.reply(msg_user + 'Registrei com sucesso:\n- ' + veiculo + "\n" + my_msg + '\n\nPara apagar envie *Apagar: ' + db_id + '*' + avisos);
                                } else { 
                                    avisos = "\n\nAviso:\nEste veículo não está cadastrado."
                                    //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + "\n" + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + db_id + '\n\nId do registro: ' + db_id + avisos);
                                    msg.reply(msg_user + 'Registrei com sucesso:' + "\n" + my_msg + '\n\nPara apagar envie *Apagar_deslocamento: ' + db_id + '*' + avisos);
                                    }
                            });
                        } else {
                            var insertQuery = "SELECT * FROM my_schema.deslocamentos ORDER BY id DESC LIMIT 1;";
                            connection.query(insertQuery, function(err, recordset){
                                avisos = "\n\nAviso:\nPrimeiro registro deste veículo."
                                db_id = recordset[0].id;
                                msg.reply(msg_user + 'Registrei com sucesso:' + "\n" + my_msg + '\n\nPara apagar envie *Apagar_deslocamento: ' + db_id + '*' + avisos);
                            });
                        }});
                        
                    });
                }});

            }
            else {
                msg.reply(msg_user + 'Eu só aceito imagens !'); 
            }
        }
        else
        {
           msg.reply( msg_user + 'Você esqueceu de mandar a foto da notinha, mande novamente !'); 
        }
        }
    } else {
        msg.reply( msg_user + 'A mensagem não está no padrão correto !'); 
    }  
}

// Função que processa a informação do abastecimento
async function abastecimento_reply (msg) {

    const quotedMsg = await msg.getQuotedMessage();

    numero = quotedMsg.author.split('@')

    numero_processado = numero[0];

    if (numero_processado.length == 12) {
        var a = numero_processado;
        var b = "9";
        var position = 4;
        numero_processado = [a.slice(0, position), b, a.slice(position)].join('');
    }

    //let msg_user = 'Olá número: ' + numero_processado +'\n\n'
    let msg_user = 'Olá número: ' + numero_processado +', Sou SARR.\n'

    mesagem_divida = quotedMsg.body.split("|");

    //Verifica se o texto está certo
    if (((mesagem_divida.length - 1) == 7) || ((mesagem_divida.length - 1) == 8)) {

        valor = mesagem_divida[0].replace(/[^0-9,]/g, '');
        cnpj = mesagem_divida[1].replace(/\D/g,'');
        cc = mesagem_divida[2].replace(/\D/g,'');
        data = mesagem_divida[3].replace(/\D/g,'');
        cartao = mesagem_divida[4].replace(/\D/g,'');
        litros = mesagem_divida[5].replace(/[^0-9,]/g, '');
        km = mesagem_divida[6].replace(/\D/g,'');
        placa = mesagem_divida[7].replace("[^a-zA-Z0-9]", "");
        placa = mesagem_divida[7].replace("-", "");
        placa = placa.replace(/\s/g, '');

        responsavel_dif = 0;
        if ((mesagem_divida.length - 1) == 8) {responsavel_dif = mesagem_divida[8].replace(/\D/g,'');}

        contem_erro = 0;
        erros = "";

        data = data.slice(6, 8) + "/" + data.slice(4, 6) + "/" + data.slice(0, 4)
        cnpj = cnpj.slice(0, 2) + "." + cnpj.slice(2, 5) + "." + cnpj.slice(5, 8) + "/" +  cnpj.slice(8, 12) + "-" +  cnpj.slice(12, 15);
        placa = placa.slice(0, 3) + "-" + placa.slice(3, 7)

        //my_msg = 'Centro de custos: ' + cc + '\nData: ' + data + '\nValor: ' + valor + '\nCNPJ: ' + cnpj + '\nCartão: ' + cartao + '\nDescrição: ' + descricao;
        my_msg = '- CC ' + cc + '\n- ' + data + '\n- R$' + valor + '\n- ' + cnpj + '\n- ' + cartao + '\n- ' + litros + '\n- ' + km + '\n- ' + placa;
        if ((mesagem_divida.length - 1) == 6) {my_msg = my_msg + '\nCusto enviado para o ID: ' + responsavel_dif;}

        var regex  = /^\d+(?:\,\d{0,2})$/;
        if (regex.test(valor)) {valor = valor.replace(",",".")} else {contem_erro = 1; erros = erros + "\nEsse valor de gasto não está no padrão correto"}
        if ((cnpj.length) != 18 ) {contem_erro = 1; erros = erros + "\nEsse número de CNPJ não está correto"}
        if ((cc.length) <= 1 ) {contem_erro = 1; erros = erros + "\nÉ necessário adicionar um centro de custos"}
        if ((data.length) != 10) {contem_erro = 1; erros = erros + "\nVocê não digitou uma data no formato aceitável"}
        if ((cartao.length) != 4 ) {contem_erro = 1; erros = erros + "\nO número do cartão está no formato incorreto"}
        if (regex.test(litros)) {litros = litros.replace(",",".")} else {contem_erro = 1; erros = erros + "\nEsse valor de litros não está no padrão correto"}
        if ((km.length) < 1 ) {contem_erro = 1; erros = erros + "\nPrecisa adicionar uma quilometragem"}
        if (((mesagem_divida.length - 1) == 7) && ((responsavel_dif.length) <= 1)) {contem_erro = 1; erros = erros + "\nNesse formato você precisa adicionar um id de responsável."}

        if (contem_erro) {msg.reply( msg_user + erros); }
        else {
        //Verifica se uma imagem foi enviada
        if(quotedMsg.hasMedia) {
            const media = await quotedMsg.downloadMedia();
            // do something with the media data here
            if ( media.mimetype == 'image/jpeg') {

                get_date()
                caminho = './img/abastecimento/' + numero_processado + '_' +  + data.replace(/\D/g,'') + '_' + String(new Date().getTime()/1000)  + '.png';

                fs.writeFile(caminho, media.data, 'base64' , function(err) { 
                    if (err) {
                    get_date()
                    console.log(date + "/" + month + "/" + year + " " + hours + ":" + minutes + ":" + seconds + ' - Error 09');
                    console.log(err); }});

                var insertQuery = "INSERT INTO abastecimento ( valor, cnpj, cc, data, cartao, litros, km, numero_telefone, caminho_imagem, status, responsavel_dif, placa) values (?,?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(insertQuery,[valor, cnpj, cc, data, cartao, litros, km, numero_processado, caminho, 'Lancada', responsavel_dif, placa], function(err, rows){
                if (err) {
                    msg.reply(msg_user + 'Ocorreu um erro interno no banco de dados !\n\nErro:\n' + err);
                }
                else 
                {
                    var insertQuery = "SELECT * FROM my_schema.abastecimento ORDER BY id DESC LIMIT 1;";
                    connection.query(insertQuery, function(err, recordset){
                        //msg.reply(msg_user + 'Eu registrei as informações com sucesso !\n\nAbaixo foi o que eu registrei:\n' + my_msg + '\n\nPara apagar o registro mande uma mensagem escrevendo:\nApagar: ' + recordset[0].id + '\n\nId do registro: ' + recordset[0].id);
                        msg.reply(msg_user + 'Registrei com sucesso:\n' + my_msg + '\n\nPara apagar envie *Apagar_abastecimento: ' + recordset[0].id + '*');
                    });
                }});

            }
            else {
                msg.reply(msg_user + 'Eu só aceito imagens !'); 
            }
        }
        else
        {
           msg.reply( msg_user + 'Você esqueceu de mandar a foto da notinha, mande novamente !'); 
        }
        }
    } else {
        msg.reply( msg_user + 'A mensagem não está no padrão correto !'); 
    }

}

function get_date() {

    // current date
    // adjust 0 before single digit date
    date = ("0" + date_ob.getDate()).slice(-2);
    // current month
    month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    // current year
    year = date_ob.getFullYear();
    // current hours
    hours = date_ob.getHours();
    // current minutes
    minutes = date_ob.getMinutes();
    // current seconds
    seconds = date_ob.getSeconds();

}

client.initialize();